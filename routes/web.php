<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

Auth::routes(['register' => false]);

// Route::get('/home', 'HomeController@index')->name('home');



    /* Catalog Category */
    // Route::group(['prefix' => 'catalog/category', 'as' => 'category.catalog.'], function () {
    //     Route::get('/', 'PostController@index')->name('index');
    //     Route::get('/create', 'PostController@create')->name('create');
    //     Route::post('/store' 'PostController@store')->name('store');
    // });

//
// Route::match(['get', 'post'], '/', function () {
//     //
// });

// Route::any('foo', function () {
//     //
// });

// Redirect
// Route::redirect('/home', '/admin');

Route::post('/test', function () {
    return response()->json(
        [
            'sf' => request()->post(),
            'data' => request()->all()
        ], 200
    );
});

Route::post('uploadProductImage','ImageController@uploadProductImage');


Route::get('/sidebar-test', function () {
    return view('themes.default.layouts.frontend.sidebar');
});


Route::get('/edit-content', function () {
    return view('admin.edit-content');
});

// Route::post('/upload-image', function () {
//     // dd(request()->image->getRealPath());
//     return request()->image->getRealPath();
// });

Route::post(
    '/upload-image',
    [
        'uses' => 'UploadController@uploadImage',
        'as' => 'api.content.image.upload'
    ]
);

Route::get('/data/test', function() {
    $directory = '/public/media/images/wysiwyg';
    $files = Storage::allFiles($directory);
    return $files;
    // return response()->json(
    //     [
    //         'image1' => 'https://i.stack.imgur.com/fdbWI.jpg',
    //         'image2' => '/storage/media/images/wysiwyg/offers/dust-mask.jpg',
    //         'test' => 'test'
    //     ]
    // );
});

Route::group(['prefix' => 'admin/laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/getBaseUrl',
    [
        'uses' => 'ConfigurationController@getBaseUrl',
        'as' => 'api.general.baseUrl'
    ]
);
// Fallback route - Admin
Route::group(['middleware' => ['auth']], function () {

    Route::fallback(function () {
        return view('layouts.admin.404');
    });

    /* Admin */
    Route::get('/admin', function () {
        return view('themes.default.layouts.admin.index');
    });
    Route::get('/admin/dashboard', function () {
        return view('themes.default.layouts.admin.index');
    })->name('dashboard');

    // Redirect with 301 status code:
    Route::permanentRedirect('/home', '/admin/dashboard');
    Route::permanentRedirect('/admin', '/admin/dashboard');

    // Catalog
    include('route/catalog.php');

    // Settings
    include('route/stores/settings.php');


    // Attributes
    include('route/stores/attributes.php');


    // Route::get('/imagetest', function()
    // {
    //     $img = Image::make('foo.jpg')->resize(300, 200);
    //     Image::make(Input::file('artist_pic')->getRealPath())->resize(120,75);

    //     return $img->response('jpg');
    // });

    /* Menu */
    include('route/menu.php');

    /* Slider and Slider items */
    include('route/slider.php');

    /* Content */
    include('route/content/index.php');


    /* System */
    include('route/system/cache.php');
    include('route/system/maintenance.php');

    /* api */
    include('route/api/get.php');

    include('route/api/post.php');
});

// Fallback route - Site
Route::fallback(function () {
    return view('themes.default.layouts.frontend.404');
});

Route::view('404', '404')->name('404');

Route::post(
    '/category/updateCaterotyOrderAndParentId',
    [
        'uses' => 'CatalogCategoryController@updateOrderAndParent',
        'as' => 'api.category.update'
    ]
);

Route::post(
    'category/updateCategoryProductOrderId',
    [
        'uses' => 'CatalogProductController@updateOrder',
        'as' => 'api.category.product.update.order'
    ]
);

Route::post(
    '/menu/items/updateOrder',
    [
        'uses' => 'MenuItemsController@updateOrder',
        'as' => 'api.category.update'
    ]
);

Route::post(
    '/category/getFilteredCategory',
    [
        'uses' => 'CatalogCategoryController@returnFilteredCategories',
        'as' => 'api.category.filteredCategory'
    ]
);

Route::post(
    '/product/updateImageOrderId',
    [
        'uses' => 'CatalogProductController@updateImageOrderId',
        'as' => 'api.product.image.update.orderId'
    ]
);
Route::post(
    '/product/deleteProductImage',
    [
        'uses' => 'CatalogProductController@deleteProductImage',
        'as' => 'api.product.image.delete'
    ]
);

include('route/blog.php');

include('route/site.php');

/* */
Route::get(
    '/css/admin_style.css',
    // function () {
    //     // fetch your CSS and assign to $contents
    //     $contents = 'test content';

    //     $response = Response::make($contents);
    //     $response->header('Content-Type', 'text/css');
    //     return $response;
    // }
    [
        'uses' => 'StyleGeneratorController@getAdminCssStyle',
        'as' => 'generator.admin.css.style'
    ]
);
