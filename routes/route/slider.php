<?php

Route::get(
    '/admin/content/slider/{slider_id}/items/',
    [
        'uses' => 'App\Http\Controllers\SliderItemsController@index',
        'as' => 'content.slider.items'
    ]
);

Route::get(
    '/admin/content/slider/{slider_id}/items/create',
    [
        'uses' => 'App\Http\Controllers\SliderItemsController@view',
        'as' => 'content.slider.items.create',
        'view' => 'create'
    ]
);


Route::post(
    '/admin/content/slider/{slider_id}/items/create',
    [
        'uses' => 'App\Http\Controllers\SliderItemsController@create',
        'as' => 'content.slider.items.create'
    ]
);

Route::get(
    'admin/content/slider/{slider_id}/items/{item_id}/delete',
    [
        'uses' => 'App\Http\Controllers\SliderItemsController@destroy',
        'as' => 'content'
    ]
);
Route::get(
    'admin/content/slider/{slider_id}/items/{item_id}/edit',
    [
        'uses' => 'App\Http\Controllers\SliderItemsController@edit',
        'as' => 'content.slider.items.edit'
    ]
);
Route::post(
    'admin/content/slider/{slider_id}/items/{item_id}/edit',
    [
        'uses' => 'App\Http\Controllers\SliderItemsController@update',
        'as' => 'content'
    ]
);

Route::post(
    'admin/content/slider/{slider_id}/edit',
    [
        'uses' => 'App\Http\Controllers\SliderController@update',
        'as' => 'content'
    ]
);

Route::get(
    '/admin/content/slider/create',
    [
        'uses' => 'App\Http\Controllers\SliderController@createView',
        'as' => 'content.slider.create'
    ]
);


Route::get(
    '/admin/content/slider/{id?}/{method?}',
    [
        'uses' => 'App\Http\Controllers\SliderController@resolveGetUrl',
        'as' => 'content'
    ]
);

Route::post(
    '/admin/content/slider/{id?}/{method?}',
    [
        'uses' => 'App\Http\Controllers\SliderController@resolvePostUrl',
        'as' => 'content.slider'
    ]
);
