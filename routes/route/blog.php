<?php

Route::get(
    '/blog',
    [
        'uses' => 'Blog\BlogController@viewPage',
        'as' => 'blog.index',
        'page' => 'blog.index',
        'block' => 'blog.index',
        'template' => '1-column',
        'type' => 'blog.index'
    ]
);

Route::get(
    '/blog/{identifier}',
    [
        'uses' => 'Blog\BlogController@viewPage',
        'as' => 'blog.view',
        'page' => 'blog.view',
        'block' => 'blog.view',
        'template' => '1-column',
        'type' => 'blog.post'
    ]
);

/* */
Route::get(
    'blog/{author}/postovi',
    [
        'uses' => 'Blog\BlogController@viewPage',
        'as' => 'blog.view.author',
        'page' => 'blog.view.author',
        'template' => '1-column',
        'type' => 'blog.author.posts'
    ]
);
