<?php

/* Edit */
Route::get(
    '/admin/content/menu/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\MenuController@viewEdit',
        'as' => 'content.menu.edit'
    ]
);

/* Update */
Route::post(
    '/admin/content/menu/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\MenuController@update',
        'as' => 'content.menu.update'
    ]
);

/* Create */
Route::get(
    '/admin/content/menu/create',
    [
        'uses' => 'App\Http\Controllers\MenuController@view',
        'as' => 'content.menu.create',
        'view' => 'create'
    ]
);
Route::post(
    '/admin/content/menu/create',
    [
        'uses' => 'App\Http\Controllers\MenuController@create',
        'as' => 'content.menu.create'
    ]
);

Route::post(
    '/admin/content/menu/create',
    [
        'uses' => 'App\Http\Controllers\MenuController@create',
        'as' => 'content.menu.create'
    ]
);

/* Delete */
Route::get(
    '/admin/content/menu/{menu_id}/delete',
    [
        'uses' => 'App\Http\Controllers\MenuController@destroy',
        'as' => 'content.menu.delete'
    ]
);

/* Items */
Route::get(
    '/admin/content/menu/{id}/items',
    [
        'uses' => 'MenuItemsController@view',
        'as' => 'content.menu.items',
        'view' => 'index'
    ]
);
/* Items */
Route::get(
    '/admin/content/menu/{id}/items/add',
    [
        'uses' => 'MenuItemsController@view',
        'as' => 'content.menu.items.add',
        'view' => 'add'
    ]
);
Route::post(
    '/admin/content/menu/{id}/items/create',
    [
        'uses' => 'MenuItemsController@create',
        'as' => 'content.menu.items.create',
        'view' => 'creat.menu.item.custom'
    ]
);
Route::post(
    '/admin/content/menu/{id}/items/createCategoryItem',
    [
        'uses' => 'MenuItemsController@createCategoryItem',
        'as' => 'content.menu.items.createCategoryItem',
        'view' => 'create.menu.item.category'
    ]
);
Route::post(
    '/admin/content/menu/{id}/items/createPageItems',
    [
        'uses' => 'MenuItemsController@createPageItems',
        'as' => 'content.menu.items.createPageItems',
        'view' => 'create.menu.item.page'
    ]
);

/* Items */
Route::get(
    '/admin/content/menu/{id}/items/export',
    [
        'uses' => 'MenuItemsController@export',
        'as' => 'content.menu.items.export',
        'view' => 'export'
    ]
);


/* Items */
Route::get(
    '/admin/content/menu/{id}/items/{item_id}/edit',
    [
        'uses' => 'MenuItemsController@edit',
        'as' => 'content.menu.items.edit',
        'view' => 'items.edit'
    ]
);

Route::post(
    '/admin/content/menu/{id}/items/{item_id}/update',
    [
        'uses' => 'MenuItemsController@update',
        'as' => 'content.menu.items.update'
    ]
);

Route::get(
    '/admin/content/menu/{id}/items/{item_id}/delete',
    [
        'uses' => 'MenuItemsController@destroy',
        'as' => 'content.menu.items.delete'
    ]
);

Route::get(
    '/admin/content/menu/{route?}/{method?}/{id?}',
    [
        'uses' => 'App\Http\Controllers\MenuController@resolveUrl',
        'as' => 'content.menu'
    ]
);
