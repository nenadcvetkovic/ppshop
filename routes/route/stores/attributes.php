<?php

Route::get(
    '/admin/store/attributes/{option?}/{route?}/{method?}/{id?}',
    [
        'uses' => 'App\Http\Controllers\AttributesController@resolveGetUrl',
        'as' => 'store.attributes'
    ]
);
