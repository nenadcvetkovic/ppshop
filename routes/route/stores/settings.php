<?php

Route::get(
    '/admin/stores/settings/configuration',
    [
        'uses' => 'App\Http\Controllers\ConfigurationController@index',
        'as' => 'store.settings.configuration.index'
    ]
);

Route::post(
    '/admin/stores/settings/configuration',
    [
        'uses' => 'App\Http\Controllers\ConfigurationController@store',
        'as' => 'store.settings.configuration'
    ]
);

/* General */
Route::get(
    '/admin/stores/settings/general',
    [
        'uses' => 'App\Http\Controllers\Store\Settings\GeneralController@index',
        'as' => 'store.settings.general.index'
    ]
);
Route::post(
    '/admin/stores/settings/general',
    [
        'uses' => 'App\Http\Controllers\Store\Settings\GeneralController@store',
        'as' => 'store.settings.general'
    ]
);

/* Resto */
Route::get(
    '/admin/stores/settings/{option?}/{route?}/{method?}/{id?}',
    [
        'uses' => 'App\Http\Controllers\SettingsController@resolveUrl',
        'as' => 'store.settings'
    ]
);
