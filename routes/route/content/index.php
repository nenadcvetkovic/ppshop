<?php

/* Content */
Route::post(
    '/admin/content/design/configuration',
    [
        'uses' => 'App\Http\Controllers\ContentDesignController@store',
        'as' => 'content.design.configuration'
    ]
);

/* Blocks */
Route::get(
    '/admin/content/blocks',
    [
        'uses' => 'App\Http\Controllers\CmsBlockController@index',
        'as' => 'content.block.index'
    ]
);

Route::get(
    '/admin/content/blocks/create',
    [
        'uses' => 'App\Http\Controllers\CmsBlockController@view',
        'as' => 'content.block.create',
        'view' => 'create'
    ]
);

Route::post(
    '/admin/content/blocks/create',
    [
        'uses' => 'App\Http\Controllers\CmsBlockController@create',
        'as' => 'content.block.create'
    ]
);

Route::get(
    '/admin/content/blocks/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\CmsBlockController@view',
        'as' => 'content.block.edit',
        'view' => 'edit'
    ]
);

Route::post(
    '/admin/content/blocks/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\CmsBlockController@update',
        'as' => 'content.block.update'
    ]
);

Route::get(
    '/admin/content/blocks/{id}/delete',
    [
        'uses' => 'App\Http\Controllers\CmsBlockController@destroy',
        'as' => 'content.block.delete'
    ]
);


/* Pages */
Route::get(
    '/admin/content/pages',
    [
        'uses' => 'App\Http\Controllers\PagesController@index',
        'as' => 'content.pages'
    ]
);

Route::get(
    '/admin/content/pages',
    [
        'uses' => 'App\Http\Controllers\CmsPagesController@index',
        'as' => 'content.pages.index'
    ]
);

Route::get(
    '/admin/content/pages/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\CmsPagesController@view',
        'as' => 'content.pages.edit',
        'view' => 'edit'
    ]
);


Route::post(
    '/admin/content/pages/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\CmsPagesController@update',
        'as' => 'content.pages.update'
    ]
);

Route::get(
    '/admin/content/pages/create',
    [
        'uses' => 'App\Http\Controllers\PagesController@view',
        'as' => 'content.pages.create',
        'view' => 'create'
    ]
);

Route::post(
    '/admin/content/pages/create',
    [
        'uses' => 'App\Http\Controllers\CmsPagesController@create',
        'as' => 'content.pages.create'
    ]
);


Route::get(
    '/admin/content/pages/{id}/delete',
    [
        'uses' => 'App\Http\Controllers\CmsPagesController@destroy',
        'as' => 'content.pages.delete'
    ]
);

/* Brands */
Route::get(
    '/admin/content/brands',
    [
        'uses' => 'App\Http\Controllers\BrandsController@index',
        'as' => 'content.brands.index'
    ]
);
Route::get(
    '/admin/content/brands/create',
    [
        'uses' => 'App\Http\Controllers\BrandsController@view',
        'as' => 'content.brands.create',
        'view' => 'create'
    ]
);
Route::post(
    '/admin/content/brands/create',
    [
        'uses' => 'App\Http\Controllers\BrandsController@create',
        'as' => 'content.brands.create'
    ]
);

Route::get(
    '/admin/content/brands/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\BrandsController@view',
        'as' => 'content.brands.edit',
        'view' => 'edit'
    ]
);

Route::post(
    '/admin/content/brands/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\BrandsController@update',
        'as' => 'content.brands.update'
    ]
);

Route::get(
    '/admin/content/brands/{id}/delete',
    [
        'uses' => 'App\Http\Controllers\BrandsController@destroy',
        'as' => 'content.brands.delete'
    ]
);

/* Blog */

Route::get(
    'admin/content/blog',
    [
        'uses' => 'App\Http\Controllers\Blog\BlogController@index',
        'as' => 'themes.default.admin.content.blog.index'
    ]
);

Route::get(
    'admin/content/blog/create',
    [
        'uses' => 'App\Http\Controllers\Blog\BlogController@view',
        'as' => 'themes.default.admin.content.blog.create',
        'page' => 'create'
    ]
);

Route::post(
    'admin/content/blog/create',
    [
        'uses' => 'App\Http\Controllers\Blog\BlogController@create',
        'as' => 'themes.default.admin.content.blog.create'
    ]
);

Route::get(
    'admin/content/blog/{id}/edit',
    [
        'uses' => 'App\Http\Controllers\Blog\BlogController@view',
        'as' => 'admin.content.blog.edit',
        'page' => 'edit'
    ]
);

/* updatge post */
Route::post(
    'admin/content/blog/{id}/update',
    [
        'uses' => 'App\Http\Controllers\Blog\BlogController@update',
        'as' => 'admin.content.blog.update'
    ]
);

Route::get(
    'admin/content/blog/{id}/delete',
    [
        'uses' => 'App\Http\Controllers\Blog\BlogController@destroy',
        'as' => 'admin.content.blog.delete',
        'page' => 'delete'
    ]
);

/* Resto */
Route::get(
    '/admin/content/{options?}/{route?}/{method?}/{id?}',
    [
        'uses' => 'App\Http\Controllers\ContentDesignController@resolveUrl',
        'as' => 'content'
    ]
);
