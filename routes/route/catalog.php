<?php
// Catalog

use App\ProductImages;

Route::get('/admin/catalog/category/delete/{id}',
    [
        'uses' => 'App\Http\Controllers\CatalogCategoryController@destroy',
        'as' => 'catalog.category.delete'
    ]
);

Route::post('admin/catalog/category/{id}/images', function($id) {
        $categoryImages = \App\CategoryImages::where('category_id', $id)->first();
        return $categoryImages;
    }
);

Route::get('/admin/catalog/product/{id}/delete',
    [
        'uses' => 'CatalogProductController@destroy',
        'as' => 'catalog.product.delete'
    ]
);

Route::get('/admin/catalog/category/{id}/products',
    [
        'uses' => 'App\Http\Controllers\CatalogCategoryController@showProducts',
        'as' => 'catalog.category.products'
    ]
);

Route::get(
    '/admin/catalog/{options?}/{route?}/{method?}/{id?}',
    [
        'uses' => 'App\Http\Controllers\CatalogController@resolveUrl',
        'as' => 'catalog'
    ]
);

Route::post('/admin/catalog/category/create',
    [
        'uses' => 'App\Http\Controllers\CatalogCategoryController@create',
        'as' => 'catalog.category.create'
    ]
);
Route::get('/admin/catalog/category/edit/{id}',
    [
        'uses' => 'App\Http\Controllers\CatalogCategoryController@edit',
        'as' => 'catalog.category.edit'
    ]
);
// Route::get('/admin/catalog/category/{id}/edit',
//     [
//         'uses' => 'App\Http\Controllers\CatalogCategoryController@edit',
//         'as' => 'catalog.category.edit'
//     ]
// );
Route::post('/admin/catalog/category/edit/{id}',
    [
        'uses' => 'App\Http\Controllers\CatalogCategoryController@update',
        'as' => 'catalog.category.update'
    ]
);


Route::post('/admin/catalog/product/create',
    [
        'uses' => 'CatalogProductController@create',
        'as' => 'catalog.product.create'
    ]
);

Route::post('/admin/catalog/product/{id}/update',
    [
        'uses' => 'CatalogProductController@update',
        'as' => 'catalog.product.update'
    ]
);

Route::post(
    'admin/catalog/product/{product_id}/images', function($product_id) {
        return ProductImages::where('product_id', $product_id)->get();
    }
);

Route::post(
    'admin/catalog/product/{product_id}/deleteImage/{image_id}',
    [
        'uses' => 'CatalogProductController@deleteImage',
        'as' => 'catalog.product.delete.image'
    ]
);
