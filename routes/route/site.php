<?php

Route::view('/welcome', 'welcome');

Route::view('/welcome', 'welcome', ['name' => 'Taylor']);


Route::get('/',
    [
        'uses' => 'App\Http\Controllers\SiteController@index',
        'as' => 'home',
        'type' => 'homepage'
    ]
);


Route::get(
    '/catalog/product/search',
    [
        'uses' => 'App\Http\Controllers\SearchController@view',
        'as' => 'product.search'
    ]
);
Route::get(
    '/katalog/proizvod/pretraga',
    [
        'uses' => 'App\Http\Controllers\SearchController@view',
        'as' => 'proizvod.pretraga'
    ]
);

Route::fallback(function () {
    return view('themes.default.layouts.frontend.404');
});

Route::get(
    '/catalog/brands/{brand?}',
    [
        'uses' => 'App\Http\Controllers\SiteController@viewBrands',
        'as' => 'site.brands',
        'view' => 'brands',
        'template' => '2-column-left',
        'type' => 'page'
    ]
);
Route::get(
    '/katalog/brend/{brand?}',
    [
        'uses' => 'App\Http\Controllers\SiteController@viewBrands',
        'as' => 'site.brands',
        'view' => 'brands',
        'template' => '2-column-left',
        'type' => 'page'
    ]
);

Route::get(
    '/catalog/products/{product?}',
    [
        'uses' => 'App\Http\Controllers\SiteController@view',
        'as' => 'site.category',
        'template' => '2-column-left',
        'type' => 'catalog.products'
    ]
);
Route::get(
    '/katalog/proizvodi/{product?}',
    [
        'uses' => 'App\Http\Controllers\SiteController@view',
        'as' => 'site.category',
        'template' => '2-column-left',
        'type' => 'catalog.products'
    ]
);

Route::get(
    '/catalog/{identifier?}',
    [
        'uses' => 'App\Http\Controllers\SiteController@view',
        'as' => 'site.category',
        'template' => '2-column-left',
        'type' => 'catalog.category'
    ]
);
Route::get(
    '/katalog/{identifier?}',
    [
        'uses' => 'App\Http\Controllers\SiteController@view',
        'as' => 'site.category',
        'template' => '2-column-left',
        'type' => 'catalog.category'
    ]
);

Route::get(
    '/proizvodi/{identifier?}',
    [
        'uses' => 'App\Http\Controllers\SiteController@view',
        'as' => 'site.category',
        'template' => '2-column-left',
        'type' => 'catalog.category'
    ]
);

/* Subscribe */

Route::post(
    '/subscribe/confirm',
    [
        'uses' => 'SubscribeController@confirm',
        'as' => 'subscribe.comfirm'
    ]
);


Route::get(
    '/subscribe/success',
    [
        'uses' => 'SubscribeController@success',
        'as' => 'subscribe.success',
        'type' => 'page'
    ]
);


/* Pages */
Route::get(
    '/{identifier}',
    [
        'uses' => 'App\Http\Controllers\SiteController@viewPage',
        'template' => '1-column',
        'type' => 'page'
    ]
);

Route::get(
    '/kontakt',
    [
        'uses' => 'App\Http\Controllers\SiteController@viewPage',
        'as' => 'contact',
        'page' => 'contact',
        'template' => '1-column'
    ]
);

Route::post(
    '/kontakt',
    [
        'uses' => 'App\Http\Controllers\SiteController@contact',
        'as' => 'contact.form.sent',
        'type' => 'page'
        ]
    );

Route::get(
    '/kontakt/hvala',
    [
        'uses' => 'App\Http\Controllers\SiteController@viewPage',
        'as' => 'contact.success',
        'type' => 'page',
        'page' => 'contact.success',
        'block' => 'contact.success',
        'template' => '1-column'
    ]
);
