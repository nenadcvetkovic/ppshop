<?php

Route::post(
    '/updateBrandsStatus',
    [
        'uses' => 'App\Http\Controllers\BrandsController@updateStatus',
        'as' => 'api.brands.status.update'
    ]
);


Route::post(
    '/updateCmsPagesStatus',
    [
        'uses' => 'App\Http\Controllers\CmsPagesController@updateStatus',
        'as' => 'api.pages.status.update'
    ]
);

Route::post(
    '/updateCmsBlockStatus',
    [
        'uses' => 'App\Http\Controllers\CmsBlockController@updateStatus',
        'as' => 'api.block.status.update'
    ]
);


/* Menu */
Route::post(
    '/updateMenuStatus',
    [
        'uses' => 'App\Http\Controllers\MenuController@updateStatus',
        'as' => 'api.menu.status.update'
    ]
);

/* Product */
Route::post(
    '/updataProductData',
    [
        'uses' => 'App\Http\Controllers\CatalogProductController@updataProductData',
        'as' => 'api.product.data.update'
    ]
);
