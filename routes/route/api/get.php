<?php

Route::get(
    '/api/get/{method}',
    [
        'uses' => 'App\Http\Controllers\Api\GetController@get'
    ]
);

