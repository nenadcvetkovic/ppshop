<?php

Route::get('/admin/system/tools/cache', function() {
    // Artisan::call('cache:clear');
    // return "Cache is cleared";
});


Route::get('/admin/system/tools/cache',
    [
        'uses' => 'App\Http\Controllers\System\Tools\CacheController@index',
        'as' => 'system.tools.cache'
    ]
);

Route::get('/admin/system/tools/cache/{action}',
    [
        'uses' => 'App\Http\Controllers\System\Tools\CacheController@action',
        'as' => 'system.tools.action'
    ]
);
