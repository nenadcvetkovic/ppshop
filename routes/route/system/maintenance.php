<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\MessageController;

// Route::get('/admin/system/site/down', function(){
//     // return Artisan::call('down');
//     Artisan::call('down', [
//         '--allow' => explode(',', env('APP_MAINTENANCE_IPS'))
//     ]);
//     MessageController::errorMsg("Maintenace is enabled");
//     return response()->back();
// });

// Route::get('/admin/system/site/live', function(){
//     return Artisan::call('up');
//     MessageController::successMsg("Maintenace is disabled");
//     return response()->back();
// });

Route::get('/admin/system/site/maintenance/{action}',
    [
        'uses' => 'System\Site\MaintenanceController@action',
        'as' => 'system.site.maintenance'
    ]
);
