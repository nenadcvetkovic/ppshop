<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsPages extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function images()
    {
        return $this->hasMany('App\PageImage', 'page_id', 'id');
    }

    public function getImage()
    {
        return $this->images()->first() ? $this->images()->first()->image_path : asset('storage/media/images/catalog/category/category-hover.jpg');
    }

    public function showImage()
    {
        return $this->show_page_image;
    }

    public function showPageTitle()
    {
        return $this->show_page_title;
    }

    public function getDesc()
    {
        return $this->desc;
    }

    public function getSeoKeywords()
    {
        return $this->seo_keywords ?? env('APP_KEYWORDS');
    }

    public function getSeoDescription()
    {
        return $this->seo_desc ?? env('APP_DESCRIPTION');
    }

    public function getSeoTitle()
    {
        return $this->seo_title ?? env('APP_TITLE');
    }
}
