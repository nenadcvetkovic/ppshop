<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function product()
    {
        return $this->hasMany('App\Product');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function brands()
    {
        return $this->hasMany('App\BrandProducts');
    }
}
