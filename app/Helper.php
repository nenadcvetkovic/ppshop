<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Helper extends Model
{
    const DEBUG_LOG_PATH = 'public/var/logs/debug.log';
    const SYSTEM_LOG_PATH = 'public/var/logs/system.log';
    const ERROR_LOG_PATH = 'public/var/logs/error.log';
    const CRON_LOG_PATH = 'public/var/logs/cron.log';
    const LOGIN_LOG_PATH = 'public/var/logs/login.log';

    public static function err_log($msg)
    {
            Storage::append(Helper::ERROR_LOG_PATH, $msg);
    }

    public static function debug_log($msg)
    {
            Storage::append(Helper::DEBUG_LOG_PATH, $msg);
    }

    public static function system_log($msg)
    {
            Storage::append(Helper::SYSTEM_LOG_PATH, $msg);
    }

    public static function cron_log($msg)
    {
            Storage::append(Helper::CRON_LOG_PATH, $msg);
    }

    public static function login_log($msg)
    {
            Storage::append(Helper::LOGIN_LOG_PATH, $msg);
    }
}
