<?php

namespace App;

use App\Configuration;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $config;

    public function __construct()
    {

        $this->config = new Configuration();
    }
    /**
     * Get the phone record associated with the user.
     */
    public function productImages()
    {
        return $this->product_images()->orderBy('order');
    }

    public function product_images() {
        return $this->hasMany('App\ProductImages');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function category()
    {
        return $this->hasMany('App\Category');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function brand()
    {
        return $this->hasMany('App\ProductBrands');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function productCategory()
    {

        return $this->hasManyThrough(
            'App\Category',
            'App\ProductCategory',
            'product_id', // Foreign key on  product_categories table
            'product_category_id', // Foreign key on categories table
            'id', // Local key on product table
            'product_category_id' // Local key on product_categories table
        );

        // Category::class,
        // ProductCategory::class,
        // 'product_id', // Foreign key on  product_categories table
        // 'product_category_id', // Foreign key on categories table
        // 'id', // Local key on product table
        // 'product_category_id' // Local key on product_categories table
    }

    public function getHoverImage()
    {
        $path = '/storage/media/images/no-image.png';

        $images = $this->productImages()->first();
        $path = $images ? $images->image_path : $path;

        return $path;
    }

    public function getSalePercent()
    {
        $price = $this->price;
        $special_price = $this->special_price;
        $sale = floor((($price - $special_price) * 100) / $special_price);
        return '-'.$sale;
    }

    public function isNew()
    {
        return $this->is_new;
    }

    public function getMostViewProducts()
    {
        return Product::where(['views', '>', '1'])->orderBy('views', 'desc')->get(10);
    }

    public function getSeoKeywords()
    {
        return $this->seo_keywords ?? env('APP_KEYWORDS');
    }

    public function getSeoDescription()
    {
        return $this->seo_desc ?? env('APP_DESCRIPTION');
    }

    public function getSeoTitle()
    {
        return $this->seo_title ?? env('APP_TITLE');
    }

    public function prodPerRow()
    {
        return $this->config->prodPerRow();
    }

    public function showCatalogProdSku()
    {
        return $this->config->showCatalogProdSku();
    }

    public function showCatalogProdCat()
    {
        return $this->config->showCatalogProdCat();
    }

    public function showCatalogProdDesc()
    {
        return $this->config->showCatalogProdDesc();
    }

    public function showCatalogProdPrice()
    {
        return $this->config->showCatalogProdPrice();
    }

    public function showCatalogProdStockStatus()
    {
        return $this->config->showCatalogProdStockStatus();
    }

    public function showProdSku()
    {
        return $this->config->showProdSku();
    }

    public function showProdCat()
    {
        return $this->config->showProdCat();
    }

    public function showProdDesc()
    {
        return $this->config->showProdDesc();
    }

    public function showProdPrice()
    {
        return $this->config->showProdPrice();
    }

    public function showProdStockStatus()
    {
        return $this->config->showProdStockStatus();
    }

    public function nextId()
    {
        $last_row = DB::table('items')->latest('id')->first();
        return  $last_row->id;
    }
}
