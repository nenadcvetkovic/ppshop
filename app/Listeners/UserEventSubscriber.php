<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;

class UserEventSubscriber
{
    protected $helper;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \App\Helper $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
    }

    /**
     * Handle user login events.
     */
    public function handleUserLogin($event)
    {
        // dd($event, 'login', request()->ip());
        $msg = self::class .'::'. Carbon::now() .'| IP:'.request()->ip(). "| User {$event->user->id} successfully logged";
        $this->helper->login_log($msg);
    }

    /**
     * Handle user logout events.
     */
    public function handleUserLogout($event)
    {
        // dd($event, 'logout');
        $msg = self::class .'::'. Carbon::now() .'| IP:'. request()->ip(). "| User {$event->user->id} successfully logout";
        $this->helper->login_log($msg);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@handleUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@handleUserLogout'
        );
    }
}
