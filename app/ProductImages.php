<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function product()
    {
        return $this->hasMany('App\Product');
    }
}
