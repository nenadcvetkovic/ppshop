<?php

namespace App\Providers;

use App\Blog;
use App\Helper\Block;
use App\Configuration;
use App\Translate;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $config = new Configuration();
        $translate = new Translate();

        view()->share('config', $config);
        view()->share('t', $translate);

        View::creator('*', 'App\Http\View\Composers\SettingsComposer');
        View::composer('*', function ($view) {
            $view = Block::replaceIdentifierWithBlockContent($view);
            $view = Block::replaceIdentifierWithSectionContent($view);
        });
        View::composer('*', function ($view) {
            $view->with('latestPosts', Blog::where('status', 1)->orderBy('updated_at', 'desc')->take(3)->get());
        });
    }
}
