<?php

namespace App\Providers;

use App\Helper\Block;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Blade::directive('block', function ($block) {
            // $block = preg_replace('/[\"\']+/', '', $block);
            return Block::getBlockData($block);
        });
    }
}
