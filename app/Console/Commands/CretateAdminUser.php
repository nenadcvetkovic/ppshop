<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CretateAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (User::where('name', 'admin')->first()) {
            print <<<EOL
            Admin user already has been created

            EOL;
            return 0;
        }
        //
        $name = 'admin';
        $email = '';
        $pass = Str::random(15);
        DB::table('users')->insert([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($pass),
        ]);
        DB::table('users')->insert([
            'name' => 'Nenad Cvetkovic',
            'email' => 'nenadcv@gmail.com',
            'password' => Hash::make('@Nenad05031984'),
        ]);
        print <<<EOL
            Admin user has been created
            Username: admin
            Password: $pass

            EOL;
        return 0;
    }
}
