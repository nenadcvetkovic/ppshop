<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    public function category()
    {
        return $this->hasMany('App\Category');
    }

    public function product()
    {

        return $this->hasOne('App\Product', 'id', 'product_id');
    }
}
