<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryImages extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function category()
    {
        return $this->hasMany('App\Category');
    }
}
