<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function getAuthor($id)
    {
        $user = User::find($id);
        if ($user) {
            return $user;
        }
        return 'admnistrator';
    }

    public function getAuthorName($id)
    {
        return $this->getAuthor($id);
    }

    public function getPostCreated($format = 'd.m.Y')
    {
        return $this->created_at->format($format);;
    }

    public function getPostUpdated($format = 'd.m.Y')
    {
        return $this->updated_at->format($format);;
    }

    public function showTopPostImage()
    {
        return $this->show_top_post_image;
    }

    public function getPostImage()
    {
        return $this->image_path;
    }

    public function getPostTopImage()
    {
        return $this->top_image_path;
    }

    public function getPostTitle()
    {
        return $this->name;
    }

    public function getTopPostImage()
    {
        return $this->top_image_path;
    }

    public function showPostTitle()
    {
        return $this->show_post_title;
    }

    public function getAuthorData()
    {
        $authorId = $this->author_id;

        return User::find($authorId);
    }

    public function getSeoKeywords()
    {
        return $this->seo_keywords ?? env('APP_KEYWORDS');
    }

    public function getSeoDescription()
    {
        return $this->seo_desc ?? env('APP_DESCRIPTION');
    }

    public function getSeoTitle()
    {
        return $this->seo_title ?? env('APP_TITLE');
    }
}
