<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * Get the phone record associated with the user.
     */
    public function product()
    {
        return $this->hasMany('App\Product');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function productCategory()
    {
        return $this->hasMany('App\ProductCategory');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function categoryImages()
    {
        return $this->hasMany('App\CategoryImages');
    }

    public function getCatImage()
    {
        return $this->categoryImages()->first()->image_path ?? 'media/images/catalog/category/category-hover.jpg';
    }

    public function showCatImage()
    {
        return $this->show_cat_image;
    }

    public function showPageTitle()
    {
        return $this->show_page_title;
    }

    public function getDesc()
    {
        return $this->desc;
    }

    public function getShortDesc()
    {
        return $this->short_desc;
    }

    public function childrens()
    {
        return $this->hasMany('App\Category','parent_id','id');
    }

    public function getSeoKeywords()
    {
        return $this->seo_keywords ?? env('APP_KEYWORDS');
    }

    public function getSeoDescription()
    {
        return $this->seo_desc ?? env('APP_DESCRIPTION');
    }

    public function getSeoTitle()
    {
        return $this->seo_title ?? env('APP_TITLE');
    }
}
