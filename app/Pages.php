<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function pageImages()
    {
        return $this->hasMany('App\PageImages');
    }

    public function getImage()
    {
        return $this->pageImages()->first() ? $this->pageImages()->first()->image_path : asset('storage/media/images/catalog/category/category-hover.jpg');
    }

    public function showImage()
    {
        return $this->show_page_image;
    }
}
