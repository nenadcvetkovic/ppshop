<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Http\Requests\SliderItemRequest;
use App\Http\Requests\SliderItemUpdateRequest;
use App\Slider;
use App\SliderItems;
use Exception;
use Illuminate\Http\Request;

class SliderItemsController extends Controller
{
    public function resolveGetUrl($url = 'index', $options = null, $route = null, $method = null, $id = null)
    {
        $slider = @Slider::find($options) ? @Slider::find($options) : $options;
        dd($url, $options, $route, $method, $id, $slider);
        // if (!method_exists($this, $url)) {
        //     return redirect('admin/404');
        //     // return route('fallback', 'admin/catalog/'. $url);
        // }
        // return $this->options($url, $options, $route, $method, $id);
        return $url == 'index' ? $this->index() : view('themes.default.admin.content.slider.'.$url, ['slider' => $slider]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slider_id = null)
    {
        $slider = @Slider::find($slider_id) ? @Slider::find($slider_id) : $slider_id;
        $sliderItems = SliderItems::where('slider_id', $slider_id)->get();
        // dd($slider,$sliderItems);
        return view('themes.default.admin.content.slider.items', ['slider' => $slider, 'sliderItems' => $sliderItems]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(SliderItemRequest $request)
    {
        // dd($request->all());

        $slider_id = $request->slider_id;
        $name = $request->name;
        $status = $request->status;

        $content_from_url = $request->content_from_url ? $request->content_from_url : '';
        $content_from_file = $request->content_from_file ? $request->content_from_file : '';

        $item = new SliderItems();
        $item->slider_id = $slider_id;
        $item->name = $name;
        $item->status = $status;

        $item->caption_1 = $request->caption_1;
        $item->caption_2 = $request->caption_2;
        try {
            $item->save();

            // $this->saveImageFile($item, $content_from_url, $content_from_file, $slider_id);
            $storage = new UploadController();
            $pathData = $storage->uploadImage($request, 'slider/'.$slider_id.'/items', $content_from_file ? 'file' : 'url');

            // dd($pathData, $pathData->getStatusCode(), $content_from_file, $content_from_url);

            if ($content_from_url) {
                $item->image_path = $content_from_url;
                $item->image_type = 'url';
                $item->save();
            }

            if ($content_from_file) {
                $this->saveImageFile($item, $content_from_url, $content_from_file, $slider_id);
                $item->image_path = $pathData['path'];
                $item->image_type = 'file';
                $item->save();
            }


            MessageController::successMsg("Slider Item for slider id:{$slider_id} has been created.");

        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving slider item. '.$e->getMessage().' - '.$e->getLine());
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slider_id, $item_id)
    {
        $sliderItem = SliderItems::where(['slider_id' => $slider_id, 'id' => $item_id])->first();
        // dd($slider,$sliderItems);
        return view('themes.default.admin.content.slider.items.edit', ['sliderItem' => $sliderItem]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderItemUpdateRequest $request, $slider_id, $item_id)
    {
        $item = SliderItems::where(
            [
                'slider_id' => $slider_id,
                'id' => $item_id
            ]
        )->first();
        // dd($request->all(), $slider_id, $item_id, $item);
        if ($item->count() == 0) {
            Helper::err_log('Slider item not found');
            MessageController::errorMsg('Error saving slider item. '.'Slider item not found');
            return redirect()->back();
        }

        $name = $request->name;
        $status = $request->status;

        $content_from_url = $request->content_from_url ? $request->content_from_url : '';
        $content_from_file = $request->content_from_file ? $request->content_from_file : '';


        $item->slider_id = $slider_id;
        $item->name = $name;
        $item->status = $status;

        $item->caption_1 = $request->caption_1;
        $item->caption_2 = $request->caption_2;
        try {
            $item->save();

            // $this->saveImageFile($item, $content_from_url, $content_from_file, $slider_id);
            $storage = new UploadController();
            $pathData = $storage->uploadImage($request, 'slider/'.$slider_id.'/items', $content_from_file ? 'file' : 'url');

            // dd($pathData, $pathData->getStatusCode(), $content_from_file, $content_from_url);

            if ($content_from_url) {
                $item->image_path = $content_from_url;
                $item->image_type = 'url';
                $item->save();
            }

            if ($content_from_file) {
                $this->saveImageFile($item, $content_from_url, $content_from_file, $slider_id);
                $item->image_path = $pathData['path'];
                $item->image_type = 'file';
                $item->save();
            }

            MessageController::successMsg("Slider Item for slider id:{$slider_id} has been updated.");
            return redirect()->back();

        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving slider item. '.$e->getMessage().' - '.$e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slider_id, $item_id)
    {
        // dd($slider_id, $item_id);
        $sliderItem = SliderItems::where(['slider_id' => $slider_id, 'id' => $item_id]);
        try {
            $sliderItem->delete();
            MessageController::successMsg("Slider Item id:{$item_id} for slider id:{$slider_id} has been deleted.");
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error deleting slider item. '.$e->getMessage().' - '.$e->getLine());
            return redirect()->back();
        }
    }

    public function saveImageFile(
        $item = null,
        $content_from_url = null,
        $content_from_file = null,
        $slider_id = null
    ) {
        if ($content_from_url) {
            $item->image_path = $content_from_url;
            $item->save();
        }

        if ($content_from_file) {
            $storage = new StorageController();
            $path = $storage->createSliderItemImageFolder($slider_id);

            $org_name = $content_from_file->getClientOriginalName();
            $type = $content_from_file->getMimeType();
            $realPath = $content_from_file->getRealPath();
            $ext = $content_from_file->extension();
            $imageFile = $realPath . '/' . $org_name;

            if (!file_exists($path.'/'.$org_name)) {
                $content_from_file->storeAs(
                    $path, $org_name
                );
            }
            $content_from_file->move(public_path().'/storage/' . $path, $org_name);

            $item->image_path = $path. $org_name;
            $item->save();
        }
    }

    public function view()
    {
        if (request()->route()->action['view']) {
            $view = request()->route()->action['view'];

            return view('themes.default.admin.content.slider.items.'.$view);
        }
    }
}
