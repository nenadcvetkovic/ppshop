<?php

namespace App\Http\Controllers;

use Exception;
use App\Helper;
use App\Configuration;
use Illuminate\Http\Request;

class ContentDesignController extends Controller
{
    public function resolveUrl($url = 'index', $options = null, $route = null, $method = null, $id = null)
    {
        // dd($url, $options, $route, $method, $id);
        if (!method_exists($this, $url)) {
            return redirect('admin/404');
            // return route('fallback', 'admin/catalog/'. $url);
        }
        return $this->options($url, $options, $route, $id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function options($url = null, $options = null, $route = null, $method = null, $data = null)
    {
        if ($url && $options == null) {
            return $this->{$url}();
        }
        if ($options != null) {
            return $this->{$url}($url, $options, $route, $data);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function design($url = null, $options = null, $route = null, $method = null, $id = null, $data = null)
    {
        // dd($url, $options, $route, $method, $id, $data);
        return view('themes.default.admin.content.design.index',
            [
                'settings_configuration' => new ConfigurationController
            ]
        );
    }

    public function store(Request $request)
    {
        // dd($request->all());

        foreach ($request->all() as $key => $value) {
            if ($key == "_token" || $value == '') {
                continue;
            }
            try {
                $content_design_configuration = Configuration::where(['path' => $key])->first();

                if (!$content_design_configuration) {
                    $content_design_configuration = new Configuration();
                }

                $content_design_configuration->path = $key;
                $content_design_configuration->value = $value;
                $content_design_configuration->save();
            } catch (Exception $e) {
                Helper::err_log($e->getMessage());
                MessageController::errorMsg('Error saving configuration. ' . $e->getMessage() . ' - ' . $e->getFile() . ' - ' . $e->getLine());
                return redirect()->back();
            }
        }
        MessageController::successMsg("Configuration has been saved");
        return redirect()->back();
    }
}
