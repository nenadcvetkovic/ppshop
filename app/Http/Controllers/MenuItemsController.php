<?php

namespace App\Http\Controllers;

use App\CmsPages;
use Exception;
use App\Helper;
use App\MenuItems;
use Illuminate\Http\Request;
use App\Http\Requests\MenuItemsRequest;
use App\Http\Requests\MenuPageItemsRequest;
use App\Http\Requests\MenuCategoryItemsRequest;

class MenuItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(MenuItemsRequest $request)
    {

        $type = $request->type;
        $url = $type == 'custom' ? $request->url : '';
        $menu_id = $request->menu_id;
        $parent_id = $request->parent_id;
        $name = $request->name;
        $status = $request->status;
        $desc = $request->desc;

        try {
            $menuItem = new MenuItems();

            $menuItem->type = $type;
            $menuItem->menu_id = $menu_id;
            if ($type == 'custom') {
                $menuItem->url = $url;
            }
            $menuItem->parent_id = $parent_id;
            $menuItem->name = $name;
            $menuItem->status = $status;
            $menuItem->desc = $desc;

            $menuItem->save();
            MessageController::successMsg("Menu item with id {$menuItem->id} has been created.");
            return response()->json(
                [
                    'data' => $request->all(),
                    'menuItem' => $menuItem
                ], 200
            );
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. '.$e->getMessage().' - '.$e->getLine());
            return response()->json(
                [
                    'data' => $request->all(),
                    'error' => $e->getMessage().' - '.$e->getLine()
                ], 200
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createItems(MenuCategoryItemsRequest $request)
    {
        // return response()->json(
        //     [
        //         'all' => $request->all()
        //     ], 201
        // );

        $type = $request->type;
        $design_type = $request->design_type;
        $menu_id = $request->menu_id;
        $parent_id = $request->parent_id;
        $name = $request->name;
        $status = $request->status;
        $desc = $request->desc;

        try {
            $menuItem = new MenuItems();

            $menuItem->type = $type;
            $menuItem->design_type = $design_type;
            $menuItem->menu_id = $menu_id;
            $menuItem->parent_id = $parent_id;
            $menuItem->name = $name;
            $menuItem->status = $status;
            $menuItem->desc = $desc;

            $menuItem->save();
            MessageController::successMsg("Menu item with id {$menuItem->id} has been created.");
            return response()->json(
                [
                    'data' => $request->all(),
                    'menuItem' => $menuItem
                ], 200
            );
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. '.$e->getMessage().' - '.$e->getLine());
            return response()->json(
                [
                    'data' => $request->all(),
                    'error' => $e->getMessage().' - '.$e->getLine()
                ], 200
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPageItems(MenuPageItemsRequest $request)
    {
        $type = $request->type;
        $menu_id = $request->menu_id;
        $parent_id = $request->parent_id;
        $identifier = $request->identifier;
        $status = $request->status;

        try {
            $menuItem = new MenuItems();

            $page = CmsPages::where(['identifier' => $identifier])->first();

            if (!$page) {
                MessageController::errorMsg('No page found.');
                return response()->json(
                    [
                        'data' => $request->all(),
                        'error' => 'No page found'
                    ], 200
                );
            }

            $menuItem->type = $type;
            $menuItem->menu_id = $menu_id;
            $menuItem->parent_id = $parent_id;
            $menuItem->identifier = $identifier;
            $menuItem->url = '/'.$identifier;
            $menuItem->name = $page->name;
            $menuItem->status = $status;
            $menuItem->desc = $page->desc;

            $menuItem->save();
            MessageController::successMsg("Menu item with id {$menuItem->id} has been created.");
            return response()->json(
                [
                    'data' => $request->all(),
                    'menuItem' => $menuItem
                ], 200
            );
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. '.$e->getMessage().' - '.$e->getLine());
            return response()->json(
                [
                    'data' => $request->all(),
                    'error' => $e->getMessage().' - '.$e->getLine()
                ], 200
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $item_id)
    {
        // items.edit
        // dd(
        //     [
        //         'Menu id: ' => $id,
        //         'Item id: ' => $item_id,
        //         'view' => request()->route()->action['view']
        //     ]
        // );
        return view('themes.default.admin.content.menu.items.edit', [
            'menuItem' => MenuItems::find($item_id),
            'id' => $id,
            'item_id' => $item_id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request  $request, $id, $item_id)
    {
        $name = $request->name;
        $type = $request->type;
        $design_type = $request->design_type;
        $status = $request->status;
        $desc = $request->desc;
        if ($type == 'custom') {
            $url = $request->url;
        }

        $item = MenuItems::where(['menu_id' => $id, 'id' => $item_id])->first();

        // dd(
        //     $request->all(),
        //     $id, $item_id,
        //     MenuItems::where(['menu_id' => $id, 'id' => $item_id])->get(),
        //     MenuItems::where(['menu_id' => $id, 'id' => $item_id])->toSql(),
        //     $item->count()
        // );

        if ($item->count() == 0) {
            Helper::err_log('Item has not been updated');
            MessageController::errorMsg('Item has not been updated');
            return response()->json(
                [
                    'data' => request()->all()
                ], 200
            );
        }

        $item->design_type = $design_type;
        try {
            $item->name = $name;
            $item->status = $status;
            $item->desc = $desc;
            if ($type == 'custom') {
                $item->url = $url;
            }

            $item->save();

            MessageController::successMsg("Menu item with id {$item->id} has been updated.");
            return back();
            return response()->json(
                [
                    'data' => request()->all(),
                    'item' => $item
                ], 200
            );
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. '.$e->getMessage().' - '.$e->getLine());
            return response()->json(
                [
                    'data' => request()->all(),
                    'error' => $e->getMessage().' - '.$e->getLine()
                ], 200
            );
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $item_id)
    {
        // dd($id, $item_id);
        $item = MenuItems::where(['menu_id' => $id, 'id' => $item_id])->first();

        try {
            $item->delete();

            MessageController::successMsg("Menu item with id {$item->id} has been deleted.");
            return redirect()->route('content.menu.items', ['id' => $id]);
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. '.$e->getMessage().' - '.$e->getLine());
            return back();
        }
    }

    public function view($id)
    {
        // dd($id);
        if (request()->route()->action && isset(request()->route()->action['view'])) {
            $view = request()->route()->action['view'];
            $menuItems = MenuItems::where('menu_id', $id)->orderBy('order')->get();
            return view('themes.default.admin.content.menu.items.'.$view, ['id' => $id, 'menuItems' => $menuItems]);
        }
    }

    public function updateOrder(MenuItems $menuitems)
    {
        $items = request()->data;

        $message['error'] = [];
        $message['success'] = [];

        foreach ($items as $key => $item) {
            // dd($item['id']);
            $item = $menuitems::find($item['id']);
            // dd($category);
            $item->order = $key;
            $item->parent_id = 0;
            try {
                $item->update();
                $message['success'][] = ' Saved '. $item->id . ' | order: '.  $item->order . ' | paernt id:' . $item->parent_id;

                foreach ($item['children'] as $cKey => $cvalue) {
                    $childItem = $menuitems::find($cvalue['id']);
                    $childItem->order = $cKey;
                    $childItem->parent_id = $item['id'];
                    try {
                        $childItem->update();
                        $message['success'][] = ' Saved children '. $childItem->id . ' | order: '.  $childItem->order . ' | parent id:' . $childItem->parent_id;
                    } catch (Exception $e) {
                        $message['error'][] = ' ' . $e->getMessage();
                    }
                }

            } catch (Exception $e) {
                $message['error'][] = ' ' . $e->getMessage();
            }
        }

        return response()->json(
            [
                'items' => $menuitems,
                'data' => $items,
                'new' => $this->getOrdered($menuitems::orderBy('order')->get()),
                'message' => $message
            ], 200
        );
    }

    public function getOrdered($items)
    {
        $collection = [];
        $childrens = [];

        foreach ($items as $item) {
            if ($item->parent_id == 0) {
                $collection[] = $item;
            }
            if ($item->parent_id != 0) {
                $childrens[$item->parent_id][] = $item;
            }
        }
        // foreach ($childrens as $key => $value) {
        //     $collection[$key]->children = collect($value);
        // }
        foreach ($collection as $ckey => $cvalue) {
            foreach ($childrens as $key => $value) {
                if ($collection[$ckey]->id == $key) {
                    $collection[$ckey]->children = $value;
                }
            }
            // echo '<br/>';
        }

        return $collection;
    }
}
