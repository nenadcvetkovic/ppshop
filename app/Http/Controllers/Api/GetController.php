<?php

namespace App\Http\Controllers\Api;

use App\CmsPages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GetController extends Controller
{
    public function get($method)
    {
        if (method_exists(GetController::class, $method)) {
            return $this->{$method}();
        }
    }

    public function allPages()
    {
        return CmsPages::all();
    }

    public function test()
    {
        $content = view('helper.admin.edit-content')->render();
        return response()->json(
            [
                'data' => $content
            ], 200
        );
    }

}
