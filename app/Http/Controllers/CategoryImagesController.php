<?php

namespace App\Http\Controllers;

use App\CategoryImages;
use Illuminate\Http\Request;

class CategoryImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoryImages  $categoryImages
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryImages $categoryImages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoryImages  $categoryImages
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryImages $categoryImages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoryImages  $categoryImages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryImages $categoryImages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoryImages  $categoryImages
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryImages $categoryImages)
    {
        //
    }
}
