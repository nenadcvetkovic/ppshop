<?php

namespace App\Http\Controllers;

use Exception;
use App\Helper;
use App\Options;
use App\Product;
use App\Category;
use App\CategoryImages;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\ProductCategory;

class CatalogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function resolve($options, $route)
    {
        $url = $this->$options();
        return $url;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCategories = Category::orderBy('order')->get();
        $collection = $this->getOrderedCategory($allCategories);

        $catList = collect($collection);

        // dd($collection, $catList);


        return view(
            'themes.default.admin.catalog.category.index',
            [
                'allCategories'=>$allCategories,
                'catList' => $catList
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateCategoryRequest $request ,$url = null, $options = null, $route = null, $data = null)
    {
        // dd($url, $options, $route, request()->all());
        $cat = new Category();

        $name = request('name');
        $identifier = str_replace(' ', '-', $name);

        $cat->name = $name;
        $cat->identifier = $identifier;
        $cat->product_category_id = 0;
        $cat->parent_id = request('parent_id');
        $cat->status = request('status');
        $cat->desc = request('desc');

        $cat->seo_title = request('seo_title') ?? '';
        $cat->seo_desc = request('seo_desc') ?? '';

        $cat->save();
        $cat->product_category_id = $cat->id;
        $cat->order = $cat->id;
        $cat->save();

        return redirect('admin/catalog/category');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($url = null, $options = null, $route = null, $id = null)
    {
        $view = 'themes.default.admin.catalog';
        $view .= $url ? '.'.$url: '';
        $view .= $options ? '.'.$options : '';

        $allCategories = Category::all();
        $selectCategories[0] = 'Root';
        foreach ($allCategories as $cat) {
            $selectCategories[$cat->id] = $cat->name;
        }

        return view($view,
            [
                'url' => $url,
                'options' => $options,
                'route' => $route,
                'category' => $route ? Category::find($route) : '',
                'allCategories' => $allCategories,
                'selectCategories' => $selectCategories
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->saveOrUpdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Options  $options
     * @return \Illuminate\Http\Response
     */
    public function show(Options $options)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Options  $options
     * @return \Illuminate\Http\Response
     */
    public function edit(Options $options)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Options  $options
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Options $options, $id)
    {
        // dd($request->all());

        return $this->saveOrUpdate($request, $id);
    }

    public function saveOrUpdate($request, $id = null)
    {

        // dd($request->all());
        $cat = $id ? Category::find($id) : new Category();

        $cat->name = $request->name;
        $cat->show_page_title = $request->show_page_title;

            $cat->identifier = str_replace(" ", "-", strtolower($request->name));
            $identifier = $request->identifier ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($request->identifier)) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower($request->name));
            $cat->identifier =  $identifier;

        $cat->status = $request->status;
        $cat->parent_id = $request->parent_id;
        $cat->desc = $request->desc;
        $cat->show_desc = $request->show_desc;
        $cat->show_cat_image = $request->show_cat_image;

        $cat->seo_title = $request->seo_title ?? $request->name;
        $cat->seo_desc = $request->seo_desc ?? $request->desc;
        $cat->seo_keywords = $request->seo_keywords;

        try {
            $cat->save();

            $catId = $cat->id;

            if ($request->images) {
                $images = $request->images;
                $storage = new StorageController();
                $path = $storage->createCatalogImageFolder($catId);

                foreach ($images as $image) {
                    $org_name = $image->getClientOriginalName();
                    $type = $image->getMimeType();
                    $realPath = $image->getRealPath();
                    $ext = $image->extension();
                    $imageFile = $realPath . '/' . $org_name;

                    $file = $path . '/' . $org_name;

                    if (!$storage->isPathExist('public/' . $file)) {
                        $image->storeAs(
                            'public/' . $path,
                            $org_name
                        );
                    }

                    $categoryImage = CategoryImages::where(
                        [
                            'category_id' => $catId,
                        ]
                    )->first();
                    if (!$categoryImage) {
                        $categoryImage = new CategoryImages();
                    }
                    $categoryImage->category_id = $catId;
                    $categoryImage->image_path = $file;

                    $categoryImage->save();

                }
            }
            MessageController::successMsg("Categotu with id {$catId} has been saved.");
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. ' . $e->getMessage() . ' - ' . $e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Options  $options
     * @return \Illuminate\Http\Response
     */
    public function destroy(Options $options, $id)
    {
        $cat = Category::find($id);
        if ($cat->delete()) {
            $this->changeParentIdToRoot($id);
            $this->changeProductCategoryToUntracked($id);

            return redirect('admin/catalog/category');
        } else {
            return redirect('admin/catalog/category/false');
        }
    }

    public function changeParentIdToRoot($id)
    {
        $all_cat = Category::where('parent_id', $id)->get();
        foreach ($all_cat as $cat) {
            $cat->parent_id = 0;
            $cat->save();
        }
    }

    public function changeProductCategoryToUntracked($id)
    {
        $all_prod = ProductCategory::where('product_category_id', $id)->get();

        foreach ($all_prod as $prod) {
            $prod->product_category_id = 1;
            $prod->save();
        }
    }

    public function reorder(Category $cat)
    {
        return view('themes.default.admin.catalog.category.reorder', ['cat' => $cat]);
    }

    public function updateOrderAndParent(Category $cat)
    {
        $categories = request()->data;

        $message['error'] = [];
        $message['success'] = [];

        foreach ($categories as $key => $item) {
            // dd($item['id']);
            $category = $cat::find($item['id']);
            // dd($category);
            $category->order = $key;
            $category->parent_id = 0;
            try {
                $category->update();
                $message['success'][] = ' Saved '. $category->id . ' | order: '.  $category->order . ' | paernt id:' . $category->parent_id;

                foreach($item['children'] as $cKey => $cvalue) {
                    $childItem = $cat::find($cvalue['id']);
                    $childItem->order = $cKey;
                    $childItem->parent_id = $item['id'];
                    try {
                        $childItem->update();
                        $message['success'][] = ' Saved children '. $childItem->id . ' | order: '.  $childItem->order . ' | parent id:' . $childItem->parent_id;
                    } catch (Exception $e) {
                        $message['error'][] = ' ' . $e->getMessage();
                    }
                }

            } catch (Exception $e) {
                $message['error'][] = ' ' . $e->getMessage();
            }
        }

        return response()->json(
            [
                'cat' => $cat,
                'categories' => $categories,
                'new' => $this->getOrderedCategory($cat::orderBy('order')->get()),
                'message' => $message
            ], 200
        );
    }

    public function getOrderedCategory($allCategories)
    {
        $collection = [];
        $childrens = [];

        foreach ($allCategories as $cat) {
            if ($cat->parent_id == 0) {
                $collection[] = $cat;
            }
            if ($cat->parent_id != 0) {
                $childrens[$cat->parent_id][] = $cat;
            }
        }
        // foreach ($childrens as $key => $value) {
        //     $collection[$key]->children = collect($value);
        // }
        foreach ($collection as $ckey => $cvalue) {
            foreach ($childrens as $key => $value) {
                if ($collection[$ckey]->id == $key) {
                    $collection[$ckey]->children = $value;
                }
            }
            // echo '<br/>';
        }

        return $collection;
    }

    public function returnFilteredCategories()
    {
        $cat = Category::orderBy('order')->get();
        $filteredCategries = [];

        foreach ($cat as $item) {
            if ($item->parent_id == 0) {
                $filteredCategries['key'.$item->id]['data'] = $item;
                $filteredCategries['key'.$item->id]['show'] = false;
            } else {
                $filteredCategries['key'.$item->parent_id]['children'][] = $item;
            }
        }

        // dd($filteredCategries);

        return response()->json(
            [
                'data' => $cat,
                'filteredCategories' => $filteredCategries
            ], 200
        );
    }

    public function showProducts($id)
    {
        $cat = Category::where('id', $id)->first();
        $cat_prod = ProductCategory::where('product_category_id', $id)->with('product')->orderBy('order')->get();

        // dd($cat, $cat_prod);

        return view(
            'themes.default.admin.catalog.category.products.reorder',
            [
                'cat' => $cat,
                'cat_prod' => $cat_prod
            ]
        );
    }
}
