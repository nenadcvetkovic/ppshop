<?php

namespace App\Http\Controllers;

use Exception;
use App\Helper;
use App\CmsBlock;
use App\CmsPages;
use App\PageImage;
use App\Helper\Block;
use App\CategoryImages;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Http\Requests\PageRequest;
use PhpParser\Node\Expr\Cast\Array_;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdatePageRequest;

class CmsPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CmsPages $pages)
    {
        return view('themes.default.admin.content.pages.index', ['allPages' => $pages::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PageRequest $request, CmsPages $cmsPages)
    {
        // dd($request->all());

        $page = new $cmsPages;
        $page->name = $request->name;
        $page->identifier = $request->identifier ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($request->identifier)) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower(request('name')));
        $page->status = $request->status;
        $page->layout = $request->layout;
        $page->content = request('content');
        $page->show_page_image = $request->show_page_image;
        $page->show_page_title = $request->show_page_title;
        $page->show_desc = $request->show_desc;
        $page->seo_title = $request->seo_title ?? $request->name;
        $page->seo_desc = $request->seo_desc ?? $request->desc;
        $page->seo_keywords = $request->seo_keywords ?? $request->keywords;

        try {
            $page->save();

            $this->saveImage($request, $page);

            MessageController::successMsg("Page with id {$page->id} has been created.");
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving page. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
        return redirect()->route('content.pages.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CmsPages  $cmsPages
     * @return \Illuminate\Http\Response
     */
    public function show(CmsPages $cmsPages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CmsPages  $cmsPages
     * @return \Illuminate\Http\Response
     */
    public function edit(CmsPages $cmsPages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CmsPages  $cmsPages
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePageRequest $request, CmsPages $cmsPages, $id)
    {
        // dd($request->all(), $id);
        $name = $request->name;
        $status = $request->status;
        $layout = $request->layout;
        $content = request('content');
        $show_page_image = $request->show_page_image;
        $show_page_title = $request->show_page_title;
        $show_desc = $request->show_desc;
        $seo_title = $request->seo_title ?? $request->name;
        $seo_desc = $request->seo_desc ?? $request->desc;
        $seo_keywords = $request->seo_keywords ?? $request->desc;

        $page = $cmsPages::where(['id' => $id])->first();

        if ($page == '') {
            MessageController::errorMsg('Page not found.');
            return redirect()->back();
        }
        $page->name = $name;
        $page->status = $status;
        $page->layout = $layout;
        $page->content = $content;
        $page->show_page_image = $show_page_image;
        $page->show_page_title = $show_page_title;
        $page->show_desc = $show_desc;

        $page->seo_title = $seo_title;
        $page->seo_desc = $seo_desc;
        $page->seo_keywords = $seo_keywords;

        try {
            $page->save();

            $this->saveImage($request, $page);

            MessageController::successMsg("Page with id {$page->id} has been updated.");
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving page. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CmsPages  $cmsPages
     * @return \Illuminate\Http\Response
     */
    public function destroy(CmsPages $cmsPages, $id)
    {
        $page = $cmsPages::where(['id' => $id])->first();
        if ($page) {
            try {
                $page->delete();

                MessageController::successMsg("Page with id {$page->id} has been deleted.");
            } catch (Exception $e) {
                Helper::err_log($e->getMessage());
                MessageController::errorMsg('Error deleting page. ' . $e->getMessage() . ' - ' . $e->getLine());
            }
            return redirect()->route('content.pages.index');
        }
        MessageController::errorMsg('Error deleting page. Page is not deleted!');
        return redirect()->back();
    }

    public function view(Route $route, CmsPages $cmsPages, $id)
    {
        $view = $route->action['view'];
        $page = $cmsPages::where(['id' => $id])->with('images')->first();

        if ($page == null) {
            return view('themes.default.layouts.frontend.404');
        }
        return view('themes.default.admin.content.pages.'.$view, ['page' => $page]);
    }

    public function viewPage(CmsPages $cmsPages, $identifier)
    {
        $pageContent = $cmsPages::where(['identifier' => $identifier])->with('images')->first();

        if ($pageContent == null) {
            return view('themes.default.layouts.frontend.404');
        }

        // dd($pageContent);
        $content = $this->replaceIdentifierWithBlockContent($pageContent);
        $content = Block::replaceIdentifierWithSectionContent($content);
        $content = Block::replaceIdentifierWithWidgetContent($content);
        $content = Block::getWidgetParams($content);

        /* return content */
        return view('themes.default.front.pages.index', ['pageContent' => $pageContent, 'content' => $content]);
    }

    /**
     * Return cotnent with block indentifier replaced with content
     *
     * @param Object $pageContent
     * @return Object $conten
     */
    public function replaceIdentifierWithBlockContent($pageContent)
    {
        $content = $pageContent->content;

        $blocks = $this->getBlockIdentifier('{{', '}}', $pageContent->content);

        /* Import blocks into page */
        foreach ($blocks as $block) {
            $blockContent = CmsBlock::where(['identifier' => trim($block), 'status' => 1])->first();

            if ($blockContent) {
                $content = str_replace("{{".$block."}}", $blockContent->content, $content);
            } else {
                $content = str_replace("{{".$block."}}", '', $content);
            }
        }

        // dd($blocks, $pageContent->content, $content);

        return $content;
    }

    /**
     * Return block indentifier from page content
     *
     * @param String $tag_open
     * @param String $tag_close
     * @return Array $result
     */
    public function getBlockIdentifier($tag_open = '{{', $tag_close = '}}', $string = ''): Array
    {
        $result = [];

        foreach (explode($tag_open, $string) as $key => $value) {
            if (strpos($value, $tag_close) !== false) {
                 $result[] = substr($value, 0, strpos($value, $tag_close));
            }
        }
        return $result;
     }

     public function saveImage($request, $page)
     {
        if ($request->images) {
            $images = $request->images;
            $storage = new StorageController();
            $path = $storage->createPageImageFolder($page->id);

            foreach ($images as $image) {

                // dd($image);
                $org_name = preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($image->getClientOriginalName()));
                $type = $image->getMimeType();
                $realPath = $image->getRealPath();
                $ext = $image->extension();
                $imageFile = $realPath . '/' . $org_name;

                $file = $path . '/' . $org_name;

                if (!Storage::exists($file)) {
                    $image->storeAs(
                        'public/'.$path,
                        $org_name
                    );
                }


                $pageImage = PageImage::where('page_id', $page->id)->first();

                if (!$pageImage) {
                    $pageImage = new PageImage();
                    $pageImage->page_id = $page->id;
                }
                // dd($pageImage);

                $pageImage->image_path = $file;
                $pageImage->save();

            }
        }
     }

    /* Api */
    public function updateStatus()
    {
        $page_id = request('id');

        $page = CmsPages::where('id', $page_id)->first();

        try {
            $page->status = !$page->status;
            $page->save();

            return response()->json(
                [
                    'status' => 'success',
                    'page' => $page
                ]
            );
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving page. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
    }
}
