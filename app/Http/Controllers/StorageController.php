<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{


    public function createProductImageFolder($id): String
    {
        $path = "media/images/catalog/products/".$id;
        return $this->createPathIfNotExist($path);
    }

    public function createProductThumbImageFolder($id): String
    {
        $path = "media/images/catalog/products/thumb/".$id;
        return $this->createPathIfNotExist($path);
    }

    public function createCatalogImageFolder($id): String
    {
        $path = "media/images/catalog/category/".$id;
        return $this->createPathIfNotExist($path);
    }

    public function createPageImageFolder($id): String
    {
        $file_path = 'media/images/pages/'.$id;
        return $this->createPathIfNotExist($file_path);
    }

    public function savePageImage($file_path)
    {
        Storage::put('public/'.$file_path);
    }

    public function createSliderItemImageFolder($id): String
    {
        $path = "media/images/slider/".$id."/items";
        return $this->createPathIfNotExist($path);
    }

    public function createWysiwyg()
    {
        $path = "media/images/wysiwyg";
        return $this->createPathIfNotExist($path);
    }


    public function createImagePath($imagePath = null)
    {
        $path = $imagePath ? 'media/images/' . $imagePath : "media/images/wysiwyg";
        return $this->createPathIfNotExist($path);
    }

    public function createPathIfNotExist($path, $shared = false)
    {

        if ($shared) {
            // $path = public_path($path);
            // if (!File::exists($path)) {
            //     // path does not exist
            // }
        } else {
            if (!Storage::exists('public/'.$path)) {
                Storage::makeDirectory('public/'.$path);
            }
        }
        return $path;
    }

    public function isPathExist($path)
    {
        if (Storage::exists($path)) {
            return true;
        }
        return false;
    }
}
