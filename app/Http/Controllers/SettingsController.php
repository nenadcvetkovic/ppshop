<?php

namespace App\Http\Controllers;

use App\Configuration;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    public function resolveUrl($url = 'index', $options = null, $route = null, $method = null, $id = null)
    {
        // dd($url, $options, $route, $method, $id);
        if (!method_exists($this, $url)) {
            return redirect('admin/404');
            // return route('fallback', 'admin/catalog/'. $url);
        }
        return $this->options($url, $options, $route, $method, $id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function configuration($url = null, $options = null, $route = null, $method = null, $id = null, $data = null)
    {
        $conf = new ConfigurationController();
        if ($options) {
            return $conf->view($url, $options, $route, $method, $id, $data);
        }
        return $conf->index();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function options($url = null, $options = null, $route = null, $method = null, $id = null, $data = null)
    {
        if ($url && $options == null) {
            return $this->{$url}();
        }
        if ($options != null) {
            return $this->{$url}($url, $options, $route, $method, $id, $data);
        }
    }
}
