<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helper;
use App\Http\Requests\CreateMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use App\Menu;
use App\MenuItems;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class MenuController extends Controller
{

    public function resolveUrl($url = 'index', $options = null, $route = null, $method = null, $id = null)
    {
        // dd($url, $options, $route, $method, $id);
        if (!method_exists($this, $url)) {
            return redirect('admin/404');
            // return route('fallback', 'admin/catalog/'. $url);
        }
        return $this->view($url, $options, $route, $method, $id);
    }

    public function view( $url = null, $options = null, $route = null, $method = null, $id = null)
    {
        // dd($url, $options, $route, $method, $id);
        if (request()->route()->action && isset(request()->route()->action['view'])) {
            $view = request()->route()->action['view'];
            return view('themes.default.admin.content.menu.'.$view);
        }
        $allMenus = Menu::all();
        $categories = Category::all();
        return view('themes.default.admin.content.menu.'.$url, ['categories' => $categories, 'allMenus' => $allMenus]);

    }
    public function handle($request, Closure $next)
    {
        dd($request->route('parameter_name'));
        return $next($request);
    }

    public function viewEdit($id = null)
    {
        // dd($id);
        $menu = Menu::find($id);
        return view('themes.default.admin.content.menu.edit', ['menu' => $menu]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function options($url = null, $options = null, $route = null, $method = null, $id = null, $data = null)
    {
        if ($url && $options == null) {
            return $this->{$url}();
        }
        if ($options != null) {
            return $this->{$url}($url, $options, $route, $method, $id, $data);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateMenuRequest $request)
    {
        $name = $request->name;
        $identifier = $request->identifier;
        $status = $request->status;
        $desc = $request->desc;

        // dd($request->all());

        $menu = new Menu();
        $menu->name = $name;
        $menu->identifier = $identifier;
        $menu->status = $status;
        $menu->desc = $desc;

        try {
            $menu->save();

            MessageController::successMsg("Menu with id {$menu->id} has been created.");
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving menu. '.$e->getMessage().' - '.$e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenuRequest $request, $id)
    {
        // dd('gshffdxgf', $request->all());
        try {
            $menu = Menu::find($id);

            $name = $request->name;
            $status = $request->status;
            $desc = $request->desc;

            $menu->name = $name;
            $menu->status = $status;
            $menu->desc = $desc;
            $menu->save();

            MessageController::successMsg("Menu with id {$menu->id} has been updated");
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Menu was not updated. Please check this log >>> '.$e->getMessage().' - '.$e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($menu_id)
    {
        // dd($menu_id);

        try {
            $menu = Menu::find($menu_id);
            // $menu->delete();

            MessageController::successMsg("Menu with id {$menu->id} has been deleted. [ Delete options is not available ]");
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Menu was not delteted. Please check this log >>> '.$e->getMessage().' - '.$e->getLine());
            return redirect()->back();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function returnMenuView()
    {
        $menuItems = MenuItems::where(['status' => 1])->orderBy('order')->get();

        // dd($menuItems);

        $menuFilteredItems = [];
        foreach ($menuItems as $item) {
            if ($item->parent_id == 0) {
                $menuFilteredItems['item'.$item->id]['root'] = $item;
                $menuFilteredItems['item'.$item->id]['type'] = $item->type;
            } else {
                $menuFilteredItems['item'.$item->parent_id]['children'] = $item;
            }
        }

        $allCcategories = Category::all();
        $catController = new CatalogCategoryController();
        $filteredCategries = $catController->returnFilteredCategories();

        // dd($menuFilteredItems);

        return view('themes.default.front.parts.main-menu',
            [
                'menuItems' => $menuItems,
                'menuFilteredItems' => $menuFilteredItems,
                'allCategories' => $allCcategories,
                'filteredCategries' => $filteredCategries
            ]
        );
    }

    public static function getMenuView()
    {
        $menuItems = MenuItems::where(['status' => 1])->get();

        $menuFilteredItems = [];
        foreach ($menuItems as $item) {
            if ($item->parent_id == 0) {
                $menuFilteredItems[$item->id]['root'] = $item;
                $menuFilteredItems[$item->id]['type'] = $item->type;
            } else {
                $menuFilteredItems[$item->parent_id]['children'] = $item;
            }
        }

        $allCcategories = Category::orderBy('order')->get();
        $filteredCategries = [];

        foreach ($allCcategories as $item) {
            if ($item->parent_id == 0) {
                $filteredCategries[$item->id]['data'] = $item;
            } else {
                $filteredCategries[$item->parent_id]['children'][] = $item;
            }
        }

        return $filteredCategries;
    }


    /* Api */
    public function updateStatus()
    {
        $menu_id = request('id');

        $menu = Menu::where('id', $menu_id)->first();

        try {
            $menu->status = !$menu->status;
            $menu->save();

            return response()->json(
                [
                    'status' => 'success',
                    'menu' => $menu
                ]
            );
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving menu. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
    }
}
