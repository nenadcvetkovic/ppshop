<?php

namespace App\Http\Controllers;

use Exception;
use App\Helper;
use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function resolveGetUrl($url = 'index', $options = null, $route = null, $method = null, $id = null, $test = null)
    {
        $slider = @Slider::find($url) ?? @Slider::find($url);
        // dd($test, $url, $options, $route, $method, $id, $slider);
        // if (!method_exists($this, $url)) {
        //     return redirect('admin/404');
        //     // return route('fallback', 'admin/catalog/'. $url);
        // }
        // return $this->options($url, $options, $route, $method, $id);
        if ($url == 'index') {
            return $this->index();
        }

        return view('themes.default.admin.content.slider.'.$options, ['slider' => $slider]);
    }

    public function resolvePostUrl($url = 'index', $options = null, $route = null, $method = null, $id = null, $data = null)
    {
        // dd($url, $options, $route, $method, $id);
        if (!method_exists($this, $url)) {
            return redirect('admin/404');
            // return route('fallback', 'admin/catalog/'. $url);
        }
        return $this->{$url}($url, $options, $route, $method, $id, $data);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function options($url = null, $options = null, $route = null, $method = null, $id = null, $data = null)
    {
        if ($url && $options == null) {
            return $this->{$url}();
        }
        if ($options != null) {
            // return $this->{$url}($url, $options, $route, $method, $id, $data);
            return view('themes.default.admin.content.slider.'.$url);
        }
    }

    public function createView()
    {
        return view('admin.content.slider.create', ['page' => 'create']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allSliders = Slider::all();
        // dd($allSliders);
        return view('themes.default.admin.content.slider.index', ['allSliders' => $allSliders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(request()->all());
        $name = request('name');
        $idenitifier = request('idenitifier') ? strtolower(str_replace(' ', '-', request('idenitifier'))) : strtolower(str_replace(' ', '-', $name));
        $status = request('status');
        $desc = request('desc');
        $slider_type = request('slider_type');

        $slider = new Slider();

        $slider->name = $name;
        $slider->idenitifier = $idenitifier;
        $slider->slider_type = $slider_type;
        $slider->status = $status;
        $slider->desc = $desc;

        try {
            $slider->save();
            MessageController::successMsg("Slider with id {$slider->id} has been created.");
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving slajder. '.$e->getMessage().' - '.$e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConfigurationController $config, $slider_id)
    {
        // dd($request->all(), $slider_id);

        $name = $request->name;
        $status = $request->status;
        $desc = $request->desc;

        /* Desing */
        $slider_type = $request->slider_type;
        $slider_max_heigh = $request->slider_max_heigh;

        $slider = Slider::where(['id' => $slider_id])->first();
        if (!$slider) {
            MessageController::errorMsg('Error saving slajder. Slder not found');
            return redirect()->back();
        }

        $slider->name = $name;
        $slider->status = $status;
        $slider->desc = $desc;
        $slider->slider_type = $slider_type;

        try {
            $slider->save();

            $config->setSliderHeight($slider_id, $slider_max_heigh);

            MessageController::successMsg("Slider with id {$slider->id} has been saved.");
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving slajder. '.$e->getMessage().' - '.$e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
