<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\StorageController;

class UploadController extends Controller
{
    public function uploadImage(Request $request, $imagePath = null, $type = 'file')
    {
        // dd($type, $request->all(), $imagePath);
        if ($request->image || $request->top_image || $request->content_from_file) {
            $alt = $request->alt;

            if ($type == 'file') {
                // dd('dfgdfgh');
                $image = '';
                if ($request->image) {
                    $image = $request->image;
                }
                if ($request->top_image) {
                    $image = $request->top_image;
                }
                if ($request->content_from_file) {
                    $image = $request->content_from_file;
                }

                if ($image == '') {
                    return response()->json(['success' => false]);
                }

                $storage = new StorageController();
                $path = $storage->createImagePath($imagePath);

                $org_name = $image->getClientOriginalName();
                $type = $image->getMimeType();
                $realPath = $image->getRealPath();
                $ext = $image->extension();
                $imageFile = $realPath . '/' . $org_name;

                $file = $path . '/' . $org_name;

                try {
                    if (!file_exists('public/' . $file)) {
                        // $image->store($path, ['disk' => 'public_storage'], 'test');
                        // $image->move(public_path().$path, $org_name);
                        // $image->storeAs(
                        //     // $path, 'test'.'.'.$ext
                        //     'public/' . $path,
                        //     $org_name
                        // );
                        // Storage::disk('public_storage')->put($path.'/'.$org_name, $image);
                    }
                    $extension = $image->getClientOriginalExtension();
                    Storage::disk('public_storage')->put($path . '/' .$org_name,  File::get($image));


                    // if (!Storage::disk('public_storage')->exists($path.'/'.$org_name)) {

                    // }

                    return [
                        'success' => 200,
                        'path' => $file,
                        // Upload plugin
                        'data' => [
                            'link' => '/storage/' . $file
                        ]
                    ];
                } catch (Exception $e) {
                    return response()->json(
                        [
                            'success' => false, // Must be false if upload fails
                            'message' => $e->getMessage() . ' ' . $e->getLine()
                        ]
                    );
                }
                dd('test');
            }
            if ($type == 'url') {
                $file = '';
                return [
                    'success' => 200,
                    'path' => $file,
                    // Upload plugin
                    'data' => [
                        'link' => $file
                    ]
                ];
            }
        }
        return response()->json(
            [
                'success' => false
            ]
        );
    }
}
