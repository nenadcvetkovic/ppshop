<?php

namespace App\Http\Controllers;

use App\System\Tools\Cache;
use Illuminate\Http\Request;

class CacheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\System\Tools\Cache  $cache
     * @return \Illuminate\Http\Response
     */
    public function show(Cache $cache)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\System\Tools\Cache  $cache
     * @return \Illuminate\Http\Response
     */
    public function edit(Cache $cache)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\System\Tools\Cache  $cache
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cache $cache)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\System\Tools\Cache  $cache
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cache $cache)
    {
        //
    }
}
