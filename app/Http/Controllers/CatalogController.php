<?php

namespace App\Http\Controllers;

use App\Options;
use Illuminate\Http\Request;
use App\Http\Controllers\CatalogCategoryController;

class CatalogController extends Controller
{
    public function resolveUrl($url = 'index', $options = null, $route = null, $method = null, $id = null)
    {
        // dd($url, $options, $route, $method, $id);
        if (!method_exists($this, $url)) {
            return redirect('admin/404');
            // return route('fallback', 'admin/catalog/'. $url);
        }
        return $this->options($url, $options, $route, $id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function category($url = null, $options = null, $route = null, $id = null, $data = null)
    {
        $cat = new CatalogCategoryController();
        if ($options) {
            return $cat->view($url, $options, $route, $data);
        }
        return $cat->index();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function product($url = null, $options = null, $route = null, $id = null, $data = null)
    {
        $prod = new CatalogProductController();
        if ($options) {
            return $prod->view($url, $options, $route, $id, $data);
        }
        return $prod->index();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function options($url = null, $options = null, $route = null, $method = null, $data = null)
    {
        if ($url && $options == null) {
            return $this->{$url}();
        }
        if ($options != null) {
            return $this->{$url}($url, $options, $route, $data);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'Hello catalog index';
    }
}
