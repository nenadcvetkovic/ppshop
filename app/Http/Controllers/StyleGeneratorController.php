<?php

namespace App\Http\Controllers;

use App\Configuration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class StyleGeneratorController extends Controller
{
    //
    public function getAdminCssStyle()
    {
        $configuration = new Configuration();
        $contents = "";
        // Storage::put('file.txt', 'Your name');

        // or

        $contents = "#test content\n";
        $contents .= "*:not([class*='fa']):not([class*='icon']) { font-family: 'Rajdhani', sans-serif !important; }\n";

        /* Body desing */
        if ($configuration->getBodyColor()) {
            $contents .= "body { background-color: ".$configuration->getBodyColor()." !important;} \n";
            $contents .= ".ps-section__title:not(.category-page) span { background: ".$configuration->getBodyColor()." !important;} \n";
        }

        /* Header desing */
        $contents .= ".navbar { background-color: ".$configuration->getHeaderColor()." !important;} \n";

        if ($configuration->getLinkColor()) {
            $contents .= "p a, a:links, a:visited, a:active { color: ".$configuration->getLinkColor()." !important;} \n";
        }
        if ($configuration->getLinkHoverColor()) {
            $contents .= "a:hover { color: ".$configuration->getLinkHoverColor()." !important;} \n";
        }

        /* Menu links */
        if ($configuration->getMenuLinkColor()) {
            $contents .= ".menu > li > a:links, .menu > li > a:visited, .menu > li > a:active, .sub-menu > li > a:link, .sub-menu > li > a:visited, .sub-menu > li > a:active { color: ".$configuration->getMenuLinkColor()." !important;} \n";
        }
        if ($configuration->getMenuLinkHoverColor()) {
            $contents .= ".menu > li > a:hover, .sub-menu > li > a:hover { color: ".$configuration->getMenuLinkHoverColor()." !important;} \n";
        }

        if ($configuration->getMenuLinkBGColor()) {
            $contents .= ".menu > li > a:before { background-color: ".$configuration->getMenuLinkBGColor()." !important;} \n";
        }
        if ($configuration->getMenuLinkBGHoverColor()) {
            $contents .= ".menu > li > a:hover:before, .sub-menu > li > a:hover { background-color: ".$configuration->getMenuLinkBGHoverColor()." !important;} \n";
        }

        // dd($configuration->getMenuLinkHoverColor());

        $response = Response::make($contents);
        $response->header('Content-Type', 'text/css');
        return $response;
    }
}
