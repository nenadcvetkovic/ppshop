<?php

namespace App\Http\Controllers;

use App\Helper\StringValidation;
use Illuminate\Http\Request;

class StringValidationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Helper\StringValidation  $stringValidation
     * @return \Illuminate\Http\Response
     */
    public function show(StringValidation $stringValidation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Helper\StringValidation  $stringValidation
     * @return \Illuminate\Http\Response
     */
    public function edit(StringValidation $stringValidation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Helper\StringValidation  $stringValidation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StringValidation $stringValidation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Helper\StringValidation  $stringValidation
     * @return \Illuminate\Http\Response
     */
    public function destroy(StringValidation $stringValidation)
    {
        //
    }
}
