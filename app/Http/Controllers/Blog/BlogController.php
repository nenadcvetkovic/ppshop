<?php

namespace App\Http\Controllers\Blog;

use App\Blog;
use Exception;
use App\Helper;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\MessageController;
use App\Http\Requests\Blog\CreatePostRequest;
use App\Http\Requests\Blog\UpdatePostRequest;

class BlogController extends Controller
{
    public function __construct(Request $request, Route $route)
    {
        $head = $this->getHeadData($request, $route);
        view()->share('head', $head);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allBlogPosts = Blog::paginate(10);

        return view('themes.default.admin.content.blog.index', ['allBlogPosts' => $allBlogPosts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreatePostRequest $request)
    {
        // dd($request->all(), auth()->user());

        $post = new Blog();

        try {
            $name = $request->name;
            $content = $request->content;
            $short_content = $request->short_content;
            $identifier = $request->identifier ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($request->identifier)) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower($request->name));
            $status = $request->status;
            $show_post_title = $request->show_post_title;
            $show_top_post_image = $request->show_top_post_image;

            $seo_title = $request->seo_title;
            $seo_desc = $request->seo_desc;
            $seo_keywords = $request->seo_keywords;

            if (Blog::where('identifier', $identifier)->first()) {
                MessageController::errorMsg('Error saving post. Post with identifier: '. $identifier .' exist!');
                return redirect()->back();
            }

            $post->name = $name;
            $post->content = $content;
            $post->short_content = $short_content;
            $post->identifier = $identifier;
            $post->status = $status;
            $post->show_post_title = $show_post_title;
            $post->show_top_post_image = $show_top_post_image;
            $post->image_path = '';

            $post->author_id = auth()->user()->id;

            $post->seo_title = $seo_title;
            $post->seo_desc = $seo_desc;
            $post->seo_keywords = $seo_keywords;

            $post->save();
            $postId = $post->id;

            if ($request->image) {
                $storage = new UploadController();
                $pathData = $storage->uploadImage($request, 'blog/post');

                // dd($pathData);

                if ($pathData['success'] == 200) {
                    $post->image_path = $pathData['path'];
                    $post->save();
                }

            }
            MessageController::successMsg("Post {$post->name} with id {$postId} has been created.");
            return redirect()->route('themes.default.admin.content.blog.index');
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving brand. ' . $e->getMessage() . ' - ' . $e->getLine());
            return redirect()->back();
        }

        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Blog $blog, $id)
    {
        // dd($request->all(), $id);

        $post = Blog::find($id);

        try {
            $name = $request->name;
            $content = $request->content;
            $short_content = $request->short_content;
            $identifier = $request->identifier ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($request->identifier)) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower($request->name));
            $status = $request->status;
            $show_post_title = $request->show_post_title;
            $show_top_post_image = $request->show_top_post_image;

            $seo_title = $request->seo_title;
            $seo_desc = $request->seo_desc;
            $seo_keywords = $request->seo_keywords;

            $post->name = $name;
            $post->content = $content;
            $post->short_content = $short_content;
            $post->identifier = $identifier;
            $post->status = $status;
            $post->show_post_title = $show_post_title;
            $post->show_top_post_image = $show_top_post_image;

            $post->seo_title = $seo_title;
            $post->seo_desc = $seo_desc;
            $post->seo_keywords = $seo_keywords;

            $post->save();
            $postId = $post->id;

            if ($request->image) {
                $storage = new UploadController();
                $pathData = $storage->uploadImage($request, 'blog/post/'.$postId);

                // dd($pathData);

                if ($pathData['success'] == 200) {
                    $post->image_path = $pathData['path'];
                    $post->save();
                }

            }
            if ($request->top_image) {
                $storage = new UploadController();
                $pathData = $storage->uploadImage($request, 'blog/post/'.$postId);

                // dd($pathData);

                if ($pathData['success'] == 200) {
                    $post->top_image_path = $pathData['path'];
                    $post->save();
                }

            }
            MessageController::successMsg("Post {$post->name} with id {$postId} has been updated.");
            // return redirect()->route('themes.default.admin.content.blog.index');
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving brand. ' . $e->getMessage() . ' - ' . $e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog, $id)
    {
        try {
            $blogItem = $blog->where('id', $id)->first();
            $blogItem->delete();

            MessageController::successMsg("Blog {$id} has been deleted.");
            // return redirect()->route('themes.default.admin.content.blog.index');
            return redirect()->back();
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error deleting blog. ' . $e->getMessage() . ' - ' . $e->getLine());
            return redirect()->route('themes.default.admin.content.blog.index');
        }
    }

    public function view()
    {
        // dd(request()->route()->parameters);
        $page = 'index';
        $data = [];

        if (request()->route()->action['page']) {
            $page = request()->route()->action['page'];
            if ($page == 'edit') {
                $id = request()->route()->parameters['id'];
                $post = Blog::find($id);
                $data = [
                    'post' =>  $post
                ];
            }
        }

        return view('themes.default.admin.content.blog.'. $page, $data);
    }

    public function viewPage($identifier = null)
    {
        $view = 'blog.index';
        $data = [
            'allBlogPosts' => Blog::where('status', 1)->paginate(9)
        ];

        if (request()->route()->action['page']) {
            $page = request()->route()->action['page'];
        }
        // dd($view, $data, $identifier, 'themes.default.front.'. $view);
        if ($identifier) {
            // dd('test');
            $post = Blog::where(
                [
                    'status' => 1,
                    'identifier' => $identifier
                ]
            )->first();

            $data = [
                'identifier' => $identifier,
                'post' => $post
            ];
            $view = 'blog.single';

            return view('themes.default.front.'. $view, $data);
        }

        return view('themes.default.front.'. $view, $data);
    }

    public function getHeadData($request, $route)
    {

        // dd($route);
        switch($route->uri) {
        case 'blog':
                $head = [
                    'meta' => [
                        'author' => 'Nenad Cvetkovic',
                        'description' => 'Short descritpion for blog'
                    ]
                ];
                return $head;
            break;
        case 'blog/{identifier}':
            $postIdentifier = $route->parameters['identifier'];
            $post = Blog::where('identifier', $postIdentifier)->first();
            // dd($post);

            $head = [
                'meta' => [
                    'title' => $post->name,
                    'author' => $post->meta_author ?? env('APP_NAME'),
                    'description' => $post->meta_author ?? env('APP_DESC')
                ]
            ];
            return $head;
            break;
        }

        return null;
    }

}
