<?php

namespace App\Http\Controllers;

use App\Blog;
use Exception;
use App\Helper;
use App\Slider;
use App\Product;
use App\Category;
use App\CmsPages;
use App\Configuration;
use Illuminate\Http\Request;
use App\Helper\StringValidation;
use Illuminate\Support\Facades\DB;

class ConfigurationController extends Controller
{
    const SHOP_STATUS = 'shop/status';

    /* Catalog */
    const CATALOG_ROOT = 'catalog';
    const CATALOG_LAYOUT = self::CATALOG_ROOT . '/layout';

    /* Catalog product */
    const PRODUCT_ROOT = 'product/';
    const PRODUCT_CATALOG_ROOT = 'catalog/' . self::PRODUCT_ROOT;
    const PRODUCT_RATING_STATUS = self::PRODUCT_ROOT . 'rating/status';
    const PRODUCT_WISHLIST_STATUS = self::PRODUCT_ROOT . 'wishlist/status';
    const PRODUCT_SHARE_STATUS = self::PRODUCT_ROOT . 'sharing/status';
    const PRODUCT_SHOW_RELATED_PRODUCTS = self::PRODUCT_ROOT . 'show/related-products/status';
    const PRODUCT_LIST_MODE = self::PRODUCT_ROOT . 'list/mode';
    const PRODUCT_ADDITIONAL_INFO = self::PRODUCT_ROOT . 'additional-info/status';

    const PRODUCT_MOBILE_ROOT = 'product/mobile/';
    const PRODUCT_MOBILE_LIST_MODE = self::PRODUCT_MOBILE_ROOT . 'list/mode';

    const PRODUCT_PER_ROW = self::PRODUCT_ROOT . 'prod_per_row';
    const PRODUCT_PER_PAGE = self::PRODUCT_ROOT . 'prod_per_page';

    const PRODUCT_CATALOG_SHOW_SKU = self::PRODUCT_CATALOG_ROOT . 'show_prod_sku';
    const PRODUCT_CATALOG_SHOW_DESC = self::PRODUCT_CATALOG_ROOT . 'show_prod_desc';
    const PRODUCT_CATALOG_SHOW_PRICE = self::PRODUCT_CATALOG_ROOT . 'show_prod_price';
    const PRODUCT_CATALOG_SHOW_PROD_CAT = self::PRODUCT_CATALOG_ROOT . 'show_prod_cat';
    const PRODUCT_CATALOG_SHOW_STOCK_STATUS = self::PRODUCT_CATALOG_ROOT . 'show_prod_stock_status';

    const PRODUCT_SHOW_SKU = self::PRODUCT_ROOT . 'show_prod_sku';
    const PRODUCT_SHOW_DESC = self::PRODUCT_ROOT . 'show_prod_desc';
    const PRODUCT_SHOW_PRICE = self::PRODUCT_ROOT . 'show_prod_price';
    const PRODUCT_SHOW_PROD_CAT = self::PRODUCT_ROOT . 'show_prod_cat';
    const PRODUCT_SHOW_STOCK_STATUS = self::PRODUCT_ROOT . 'show_prod_stock_status';

    /* Slider */
    const SLIDER_STATUS = 'slider/status';
    const SLIDER_ACTIVE_ID = 'slider/active/id';
    const SLIDER_MAX_HEIGHT = 'slider/style/max-height';
    const SLIDER_MARGIN = 'slider/style/margin';
    const SLIDER_PADDING = 'slider/style/padding';

    /* account */
    const ACCOUNT_SHOW_AUTH = 'account/auth/status';
    const ACCOUNT_SHOW_CURENCY = 'account/currency/status';
    const ACCOUNT_SHOW_LANGUAGE = 'account/language/status';

    /* Content desing */
    const CONTENT_DESIGN_CONFIGURATION_ROOT = 'content/design/configuration/';

    const CONTENT_DESIGN_CONFIGURATION_BODY_ROOT = self::CONTENT_DESIGN_CONFIGURATION_ROOT . 'body/';
    const CONTENT_DESIGN_CONFIGURATION_BODY_COLOR = self::CONTENT_DESIGN_CONFIGURATION_BODY_ROOT . 'color';

    const CONTENT_DESIGN_CONFIGURATION_HEADER = self::CONTENT_DESIGN_CONFIGURATION_ROOT . 'header/';
    const CONTENT_DESIGN_CONFIGURATION_HEADER_COLOR = self::CONTENT_DESIGN_CONFIGURATION_HEADER . 'color';

    const CONTENT_DESIGN_CONFIGURATION_LINKS_COLOR = self::CONTENT_DESIGN_CONFIGURATION_ROOT . 'links/active';
    const CONTENT_DESIGN_CONFIGURATION_LINKS_HOVER_COLOR = self::CONTENT_DESIGN_CONFIGURATION_ROOT . 'links/hover';

    const CONTENT_DESIGN_CONFIGURATION_MENU_LINKS_COLOR = self::CONTENT_DESIGN_CONFIGURATION_ROOT . 'menu/links/active';
    const CONTENT_DESIGN_CONFIGURATION_MENU_LINKS_HOVER_COLOR = self::CONTENT_DESIGN_CONFIGURATION_ROOT . 'menu/links/hover';

    const CONTENT_DESIGN_CONFIGURATION_MENU_LINKS_BG_COLOR = self::CONTENT_DESIGN_CONFIGURATION_ROOT . 'menu/links/bg';
    const CONTENT_DESIGN_CONFIGURATION_MENU_LINKS_BG_HOVER_COLOR = self::CONTENT_DESIGN_CONFIGURATION_ROOT . 'menu/links/bghover';


    /* Store settings general */
    const STORE_SETTINGS_GENERAL_ROOT = 'store/settings/general/';
    const STORE_SETTINGS_GENERAL_LOADER = self::STORE_SETTINGS_GENERAL_ROOT.'show-loader';
    const STORE_SETTINGS_GENERAL_HEAD_SCRIPTS = self::STORE_SETTINGS_GENERAL_ROOT.'head-script';
    const STORE_SETTINGS_GENERAL_TOP_BODY_SCRIPTS = self::STORE_SETTINGS_GENERAL_ROOT.'top-body-script';
    const STORE_SETTINGS_GENERAL_BOTTOM_BODY_SCRIPTS = self::STORE_SETTINGS_GENERAL_ROOT.'bottom-body-script';

    /* Seo */
    const STORE_SETTINGS_GENERAL_DEFAULT = self::STORE_SETTINGS_GENERAL_ROOT.'default/';
    const STORE_SETTINGS_GENERAL_ROBOTS = self::STORE_SETTINGS_GENERAL_DEFAULT . 'robots';
    const STORE_SETTINGS_GENERAL_SEO_TITLE = self::STORE_SETTINGS_GENERAL_DEFAULT.'seo-title';

    /* General */
    const STORE_SETTINGS_GENERAL_BASE_URL = self::STORE_SETTINGS_GENERAL_ROOT.'base-url';

    /* */
    const STORE_SETTINGS_CONFIURATION_ROOT = 'stores/settings/configuration/';
    const STORE_SETTINGS_CONFGURATION_DESIGN = self::STORE_SETTINGS_CONFIURATION_ROOT.'design/';
    const STORE_SETTINGS_GENERAL_DESIGN_LOGO_SRC = self::STORE_SETTINGS_CONFGURATION_DESIGN.'logo/src';
    const STORE_SETTINGS_GENERAL_DESIGN_LOGO_WIDTH = self::STORE_SETTINGS_CONFGURATION_DESIGN.'logo/max-width';
    const STORE_SETTINGS_GENERAL_DESIGN_LOGO_HEIGHT = self::STORE_SETTINGS_CONFGURATION_DESIGN.'logo/max-height';

    const STORE_SETTINGS_MAINTENANCE_ROOT = self::STORE_SETTINGS_CONFIURATION_ROOT.'maintenance/';
    const STORE_SETTINGS_MAINTENANCE_IPS = self::STORE_SETTINGS_MAINTENANCE_ROOT.'ips';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        foreach ($sliders as $slider) {
            $allSliders[$slider->id] = $slider->name;
        }

        $settings_configuration = new ConfigurationController();
        return view('themes.default.admin.stores.settings.configuration.index', [
            'settings_configuration' => $settings_configuration,
            'allSliders' => $allSliders
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        foreach ($request->all() as $key => $value) {
            if ($key == "_token" || $value == '') {
                continue;
            }
            try {
                if ($key == 'images') {
                    $images = new ImageController();
                    $logoPath =  $images->upoadImageToWysiwyg($request, true);
                    $settings_configuration = new Configuration();

                    $settings_configuration = Configuration::where(['path' => self::STORE_SETTINGS_GENERAL_DESIGN_LOGO_SRC])->first();

                    if (!$settings_configuration) {
                        $settings_configuration = new Configuration();
                    }
                    $settings_configuration->path = self::STORE_SETTINGS_GENERAL_DESIGN_LOGO_SRC;
                    $settings_configuration->value = $logoPath;
                    $settings_configuration->save();
                } else {
                    $settings_configuration = Configuration::where(['path' => $key])->first();

                    if (!$settings_configuration) {
                        $settings_configuration = new Configuration();
                    }

                    $settings_configuration->path = $key;
                    $settings_configuration->value = $value;
                    $settings_configuration->save();
                }
            } catch (Exception $e) {
                Helper::err_log($e->getMessage());
                MessageController::errorMsg('Error saving product. ' . $e->getMessage() . ' - ' . $e->getLine());
                return redirect()->back();
            }
        }
        MessageController::successMsg("Configuration has been saved");
        return redirect()->route('store.settings.configuration.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Configuration  $configuration
     * @return \Illuminate\Http\Response
     */
    public function show(Configuration $configuration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Configuration  $configuration
     * @return \Illuminate\Http\Response
     */
    public function edit(Configuration $configuration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $configuration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Configuration $configuration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Configuration  $configuration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Configuration $configuration)
    {
        //
    }

    /* Body */
    public function getBodyColor()
    {
        return $this->getConfigValue(self::CONTENT_DESIGN_CONFIGURATION_BODY_COLOR);
    }

    /* Header */
    public function getHeaderColor()
    {
        return $this->getConfigValue(self::CONTENT_DESIGN_CONFIGURATION_HEADER_COLOR);
    }

    /* Links */
    public function getLinkColor()
    {
        return $this->getConfigValue(self::CONTENT_DESIGN_CONFIGURATION_LINKS_COLOR);
    }

    public function getLinkHoverColor()
    {
        return $this->getConfigValue(self::CONTENT_DESIGN_CONFIGURATION_LINKS_HOVER_COLOR);
    }

    public function getMenuLinkColor()
    {
        return $this->getConfigValue(self::CONTENT_DESIGN_CONFIGURATION_MENU_LINKS_COLOR);
    }

    public function getMenuLinkHoverColor()
    {
        return $this->getConfigValue(self::CONTENT_DESIGN_CONFIGURATION_MENU_LINKS_HOVER_COLOR);
    }

    public function getMenuLinkBGColor()
    {
        return $this->getConfigValue(self::CONTENT_DESIGN_CONFIGURATION_MENU_LINKS_BG_COLOR);
    }

    public function getMenuLinkBGHoverColor()
    {
        return $this->getConfigValue(self::CONTENT_DESIGN_CONFIGURATION_MENU_LINKS_BG_HOVER_COLOR);
    }

    /** */
    public function getShopStatus()
    {
        return $this->getConfigValue(self::SHOP_STATUS);
    }

    public function getProductRatingStatus()
    {
        return $this->getConfigValue(self::PRODUCT_RATING_STATUS);
    }

    public function getWishlistStatus()
    {
        return $this->getConfigValue(self::PRODUCT_WISHLIST_STATUS);
    }

    public function getShareStatus()
    {
        return $this->getConfigValue(self::PRODUCT_SHARE_STATUS);
    }

    public function showRelatedProducts()
    {
        return $this->getConfigValue(self::PRODUCT_SHOW_RELATED_PRODUCTS);
    }

    public function showProductAdditionalInfo()
    {
        return $this->getConfigValue(self::PRODUCT_ADDITIONAL_INFO);
    }

    public function getCatalogLayout()
    {
        return $this->getConfigValue(self::CATALOG_LAYOUT) ? $this->getConfigValue(self::CATALOG_LAYOUT) : '2-column-left';
    }

    public function getCatalogListMod()
    {
        return $this->getConfigValue(self::PRODUCT_LIST_MODE);
    }

    public function getMobileCatalogListMod()
    {
        return $this->getConfigValue(self::PRODUCT_MOBILE_LIST_MODE);
    }

    /* Slider */

    public function getSliderStatus()
    {
        return $this->getConfigValue(self::SLIDER_STATUS);
    }

    public function getActiveSlider()
    {
        return $this->getConfigValue(self::SLIDER_ACTIVE_ID);
    }

    public function getSliderHeight($id)
    {
        return $this->getConfigValue(self::SLIDER_MAX_HEIGHT.'/'.$id);
    }

    public function setSliderHeight($id, $value)
    {
        $this->setConfigValue(self::SLIDER_MAX_HEIGHT.'/'.$id, $value);
    }

    /* account */

    public function showSiteLanguage()
    {
        return $this->getConfigValue(self::ACCOUNT_SHOW_LANGUAGE);
    }

    public function showSiteAuth()
    {
        return $this->getConfigValue(self::ACCOUNT_SHOW_AUTH);
    }

    public function showSiteCurency()
    {
        return $this->getConfigValue(self::ACCOUNT_SHOW_CURENCY);
    }

    /* Store settings general  */

    public function showLoader() {
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_LOADER);
    }

    public function getHeadScripts()
    {
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_HEAD_SCRIPTS);
    }

    public function getTopBodyScripts()
    {
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_TOP_BODY_SCRIPTS);
    }

    public function getBottomBodyScripts()
    {
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_BOTTOM_BODY_SCRIPTS);
    }

    /* Seo */

    public function getRobots()
    {
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_ROBOTS);
    }

    /* */

    public function getConfigValue($path)
    {
        if (!$path) {
            return null;
        }
        if ($conf = Configuration::where(['path' => $path])->first()) {
            // dd($conf->value);
            return $conf->value;
        }
        return false;
    }

    public function setConfigValue($path, $value)
    {
        if (!$path) {
            return null;
        }
        if ($conf = Configuration::where(['path' => $path])->first()) {
            // dd($conf->value, $value);
            $conf->value = $value;
            $conf->save();
            return true;
        } else {
            $conf = New Configuration();
            $conf->path = $path;
            $conf->value = $value;
            // dd($path, $value);
            $conf->save();
            return true;
        }
        return false;
    }

    public function getMetadata()
    {
        return null;
    }

    public function getTitle($request)
    {
        $settings_configuration = new ConfigurationController();

        $action = $request->route()->action['type'];
        $param = $request->route()->parameters;

        $title = $this->getSeoTitle() ?? env('APP_TITLE'); // Deafult title

        switch ($action) {
        case 'catalog.category':
            $identifier = $request->route()->parameters['identifier'];
            $identifier = StringValidation::validateIdentifier($identifier);
            $cat = Category::where('identifier', $identifier)->first();
            if ($cat) {
                $title = $cat->getSeoTitle();
            }
            break;
        case 'catalog.products':
            $identifier = $request->route()->parameters['product'];
            $identifier = StringValidation::validateIdentifier($identifier);
            $prod = Product::where('identifier', $identifier)->first();
            if ($prod) {
                $title = $prod->getSeoTitle();
            }
            break;
        case 'page':
            $identifier = $request->route()->parameters['identifier'];
            $identifier = StringValidation::validateIdentifier($identifier);
            $page = CmsPages::where('identifier', $identifier)->first();
            if ($page) {
                $title = $page->getSeoTitle();
            }
            break;
        case 'homepage':
            $title = $this->getSeoTitle();
            $home = CmsPages::where('identifier', 'home')->first();
            if ($home) {
                $title = $home->getSeoTitle();
            }
            break;
        case 'blog.index':
            break;
        case 'blog.post':
            $identifier = $request->route()->parameters['identifier'];
            $identifier = StringValidation::validateIdentifier($identifier);
            $post = Blog::where('identifier', $identifier)->first();
            if ($post) {
                $title = $post->getSeoTitle();
            }
            break;
        case 'blog.author.posts':
            break;
        }
        return $title;
    }

    public function getSeoTitle()
    {
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_SEO_TITLE);
    }

    public function getHeadData($request)
    {
        if (isset($request->route()->action) && isset($request->route()->action['type'])) {
            $action = $request->route()->action['type'];
            $param = $request->route()->parameters;

            // dd(
            //     $request->route()->action,
            //     $request->route()->parameters,
            //     $action,
            //     $param
            // );


            switch ($action) {
            case 'catalog.category':
                return $this->returnCatalogHeadData($request);
                break;
            case 'catalog.products':
                return $this->returnProductHeadData($request);
                break;
            case 'page':
                return $this->returnPageHeadData($request);
                break;
            case 'homepage':
                return $this->returnDefaulHeadData();
                break;
            }
        }
        return '';
    }

    public function returnCatalogHeadData($request)
    {
        // dd($request->route()->parameters);
        if (!$request->route()->parameters) {
            return null;
        }
        $identifier = StringValidation::validateIdentifier($request->route()->parameters['identifier']);
        $category = Category::where('identifier', $identifier)->first();
        // dd($category);
        if ($category) {
            return [
                'meta' => [
                    'author' => env('APP_NAME'),
                    'keywords' => $category->getSeoKeywords(),
                    'description' => $category->getSeoDescription(),
                    'robots' => $this->getRobots()
                ],
                'category' => $category
            ];
            // return response()->json(
            //     [
            //         'meta' => [
            //             'author' => env('APP_NAME'),
            //             'keywords' => $category->getSeoKeywords(),
            //             'description' => $category->getSeoDescription(),
            //             'robots' => $this->getRobots()
            //         ],
            //         'category' => $category
            //     ], 200
            // );
        }
        return $this->returnDefaulHeadData();
    }

    public function returnProductHeadData($request)
    {
        // dd($request->route()->parameters);
        if (!$request->route()->parameters['product']) {
            return null;
        }
        $identifier = StringValidation::validateIdentifier($request->route()->parameters['product']);
        $product = Product::where('identifier', $identifier)->first();
        // dd($product);
        if ($product) {
            return [
                'meta' => [
                    'author' => env('APP_NAME'),
                    'keywords' => $product->getSeoKeywords(),
                    'description' => $product->getSeoDescription(),
                    'robots' => $this->getRobots()
                ],
                'product' => $product
            ];
        }
    }

    public function returnPageHeadData($request)
    {
        //
    }

    public function returnDefaulHeadData()
    {
        return [
            'meta' => [
                'author' => 'Nesko',
                'keywords' => 'keys',
                'description' => '',
                'robots' => $this->getRobots()
            ]
        ];
    }

    public function getBaseUrl()
    {
        // dd($this->getConfigValue(self::STORE_SETTINGS_GENERAL_BASE_URL), self::STORE_SETTINGS_GENERAL_BASE_URL);
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_BASE_URL) ?? '/';
    }

    /* STORE CONFIURATION */

    /* DESIGN */

    public function getLogoSrc()
    {
        return ($this->getConfigValue(self::STORE_SETTINGS_GENERAL_DESIGN_LOGO_SRC) ? '/storage/' . $this->getConfigValue(self::STORE_SETTINGS_GENERAL_DESIGN_LOGO_SRC) : url('storage/media/images/logo.png'));
    }

    public function getLogoWidth()
    {
        // return self::STORE_SETTINGS_GENERAL_DESIGN_LOGO_WIDTH;
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_DESIGN_LOGO_WIDTH) ?? '150px';
    }

    public function getLogoHeight()
    {
        return $this->getConfigValue(self::STORE_SETTINGS_GENERAL_DESIGN_LOGO_HEIGHT) ?? '100%';
    }

    public function getWhitelistIps()
    {
        // return $this->getConfigValue(self::STORE_SETTINGS_MAINTENANCE_IPS) explode(',', $this->getConfigValue(self::STORE_SETTINGS_MAINTENANCE_IPS)) ? : explode(',', env('APP_MAINTENANCE_IPS'));
        return explode(',', env('APP_MAINTENANCE_IPS'));

    }

    /* Product */

    public function prodPerRow()
    {
        return ($this->getConfigValue(self::PRODUCT_PER_ROW) ?? 3);
    }

    public function prodPerPage()
    {
        return ($this->getConfigValue(self::PRODUCT_PER_PAGE) ?? 9);
    }

    public function showCatalogProdSku()
    {
        return $this->getConfigValue(self::PRODUCT_CATALOG_SHOW_SKU);
    }

    public function showCatalogProdCat()
    {
        return $this->getConfigValue(self::PRODUCT_CATALOG_SHOW_PROD_CAT);
    }

    public function showCatalogProdDesc()
    {
        return $this->getConfigValue(self::PRODUCT_CATALOG_SHOW_DESC);
    }

    public function showCatalogProdPrice()
    {
        return $this->getConfigValue(self::PRODUCT_CATALOG_SHOW_PRICE);
    }

    public function showCatalogProdStockStatus()
    {
        return $this->getConfigValue(self::PRODUCT_CATALOG_SHOW_STOCK_STATUS);
    }

    public function showProdSku()
    {
        return $this->getConfigValue(self::PRODUCT_SHOW_SKU);
    }

    public function showProdCat()
    {
        return $this->getConfigValue(self::PRODUCT_SHOW_PROD_CAT);
    }

    public function showProdDesc()
    {
        return $this->getConfigValue(self::PRODUCT_SHOW_DESC);
    }

    public function showProdPrice()
    {
        return $this->getConfigValue(self::PRODUCT_SHOW_PRICE);
    }

    public function showProdStockStatus()
    {
        return $this->getConfigValue(self::PRODUCT_SHOW_STOCK_STATUS);
    }

    public function nextId($table)
    {
        $last = DB::table($table)->latest()->first();
        return $last->id + 1;
    }
}
