<?php

namespace App\Http\Controllers\Route;

use App\Http\Controllers\Blog\BlogController;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{
    public function handle(Request $request, Route $route)
    {
        // dd($route);
        if (isset($route->parameters['identifier'])) {
            $identifier = $route->parameters['identifier'];
            $this->handleIdentifier($identifier);
        }

        if (isset($route->parameters['author'])) {
            dd($route->parameters['author']);
            $author = $route->parameters['author'];
            $this->handleAuthor($author);
        }

        $this->handleBlog($request, $route);

    }

    public function handleIdentifier($identifier)
    {
        dd($identifier);
    }

    public function handleAuthor($author)
    {
        dd($author);
    }

    public function handleBlog($request, $route) {
        // dd('blog');
        $blog = new BlogController($request, $route);
        $blog->index();
    }
}
