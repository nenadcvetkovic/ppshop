<?php

namespace App\Http\Controllers;

use App\Image;
use App\ProductImages;
use Exception;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadProductImage(Request $request)
    {
        try {
            if ($request->file('file')) {
                $prodId = $request->prod_id;

                $storage = new StorageController();
                $path = $storage->createProductImageFolder($prodId);
                $image = $request->file('file');
                $name = $image->getClientOriginalName();

                $file = $path . '/' . $name;

                // $image->move(public_path().'/images/'.$prodId.'/', $name);
                $image->move(public_path().'/storage/' . $path, $name);

                // $image= new Image();
                // $image->image_name = $name;
                // $image->save();

                $productImage = new ProductImages();
                $productImage->product_id = $prodId;
                $productImage->order = $prodId;
                $productImage->image_path = url('storage/'. $file);
                $productImage->save();

                return response()->json(
                    [
                        'success' => 'You have successfully uploaded an image',
                        'path' => $path,
                        'prod_id' => $prodId,
                        'data' => $request->all()
                    ], 200
                );
            }
        } catch (Exception $e) {
            var_dump($e->getMessage() . ' ' . $e->getLine());
            die();
        }

        return response()->json(
            [
                'error' => 'You have error uploading an image',
                'data' => $request->all()
            ], 200
        );
    }

    public function test(Request $request)
    {
        if ($request->images) {

            $prodId = $request->prod->prod_id;

            $images = $request->images;
            $storage = new StorageController();
            $path = $storage->createProductImageFolder($prodId);
            $thumbPath = $storage->createProductThumbImageFolder($prodId);
            foreach ($images as $image) {
                $org_name = $image->getClientOriginalName();
                $type = $image->getMimeType();
                $realPath = $image->getRealPath();
                $ext = $image->extension();
                $imageFile = $realPath . '/' . $org_name;

                $file = $path . '/' . $org_name;
                $thumbFile = $thumbPath . '/' . $org_name;

                if (!file_exists('public/' . $file)) {
                    // dd(
                    //     [
                    //         'org name' => $org_name,
                    //         'type' => $type,
                    //         'real path' => $realPath
                    //     ]
                    // );

                    // $image->move(base_path($path),$image->getClientOriginalName());
                    // Storage::disk('local')->put('file.txt', 'Contents');
                    // Storage::copy($path.'/'.$org_name, $imageFile);
                    // Storage::put('filename', $file_content);
                    // $image->store($path);
                    $image->storeAs(
                        // $path, 'test'.'.'.$ext
                        'public/' . $path,
                        $org_name
                    );
                    // $filename =  $image->getClientOriginalName() . '.jpg';
                    // $filepath = public_path($path);

                    // move_uploaded_file($_FILES['images']['tmp_name'], $filepath.$filename);

                    // // Note that here, we are copying the destination of your moved file.
                    // // If you try with the origin file, it won't work for the same reason.
                    // copy($filepath.$filename, public_path($path).$filename);

                    $productImage = ProductImages::where(
                        [
                            'product_id' => $prodId,
                            'image_path' => $file
                        ]
                    );
                    if ($productImage->count() == 0) {
                        $productImage = new ProductImages();
                        $productImage->product_id = $prodId;
                        $productImage->image_path = url('storage/'. $file);
                        $productImage->image_org_name = $org_name;

                        // $thumbImg = Image::make(url('storage/'. $file))->resize(300, 200)->save(url('storage/thumb/'. $file));
                        // $thumbImg = Image::make($image->getRealPath())->resize(300, 200);
                        // $thumbImg->save(
                        //     'public/' . $thumbFile
                        // );
                        // Image::make($realPath)->resize(300, 200)->save('public/' . $thumbFile);
                        // Image::make('storage/'. $file)->fit(340, 340)->save('public/' . $thumbFile);

                        $productImage->save();
                    }

                }
            }
            MessageController::successMsg("Product with id {$prodId} has been created.");
            return redirect()->back();
        }
    }

    public function upoadImageToWysiwyg(Request $request, $single = false)
    {
        if ($request->images) {

            $images = $request->images;
            $path = 'media/images/wysiwyg/';

            foreach ($images as $image) {
                $org_name = $image->getClientOriginalName();
                $type = $image->getMimeType();
                $realPath = $image->getRealPath();
                $ext = $image->extension();
                $imageFile = $realPath . '/' . $org_name;

                $file = $path . '/' . $org_name;


                $image->storeAs(
                    // $path, 'test'.'.'.$ext
                    'public/' . $path,
                    $org_name
                );

                if ($single) {
                    return $file;
                }

            }
            return redirect()->back();
            // MessageController::successMsg(".");
        }
    }
}
