<?php

namespace App\Http\Controllers;

use App\Brands;
use App\Helper;
use Exception;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allBrands = Brands::all();
        return view('themes.default.admin.content.brands.index', ['allBrands' => $allBrands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd(request()->all());

        $brand = new Brands();
        $brand->name = $request->name;
        $brand->identifier = $request->identifier ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($request->identifier)) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower($request->name));
        $brand->status = $request->status;
        $brand->show_in_slider = $request->show_in_slider;
        $brand->desc = $request->desc;
        $brand->image_path = '';

        try {
            $brand->save();
            $brandId = $brand->id;

            if ($request->image) {
                $storage = new UploadController();
                $pathData = $storage->uploadImage($request, 'brands');

                // dd($pathData);

                if ($pathData['success'] == 200) {
                    $brand->image_path = $pathData['path'];
                    $brand->save();
                }

                MessageController::successMsg("Brand {$brand->name} with id {$brandId} has been created.");
                return redirect()->route('content.brands.index');
            }
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving brand. ' . $e->getMessage() . ' - ' . $e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function show(Brands $brands)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function edit(Brands $brands)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brands $brands, $id)
    {
        // dd(request()->all());

        $brandsData = $brands::find($id);

        $brandsData->name = $request->name;
        $brandsData->identifier = $request->identifier ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($request->identifier)) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower($request->name));
        $brandsData->status = $request->status;
        $brandsData->show_in_slider = $request->show_in_slider;
        $brandsData->desc = $request->desc;

        try {
            $brandsData->save();
            $brandId = $brandsData->id;

            if ($request->image) {
                $storage = new UploadController();
                $pathData = $storage->uploadImage($request, 'brands');

                // dd($pathData);

                if ($pathData['success'] == 200) {
                    $brandsData->image_path = $pathData['path'];
                    $brandsData->save();
                }
            }

            MessageController::successMsg("Brand {$brandsData->name} with id {$brandId} has been updated.");
            return redirect()->route('content.brands.index');
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving brand. ' . $e->getMessage() . ' - ' . $e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brands $brands, $id)
    {

        try {
            $brand = $brands::where('id', $id)->first();
            if ($brand) {
                $brand->delete();
            }
            MessageController::successMsg("Brand with id {$id} has been deleted.");
            return redirect()->route('content.brands.index');
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error deleting brand. ' . $e->getMessage() . ' - ' . $e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        // dd(request()->route()->action['view']);

        $view = 'themes.default.admin.content.brands';
        $view .= request()->route() && request()->route()->action ? '.' . request()->route()->action['view'] : '.index';

        $id = request('id') ?? '';
        $brands = $id ? Brands::find($id) : Brands::all();

        return view(
            $view,
            [
                'brands' => $brands
            ]
        );
    }

    /* Api */
    public function updateStatus()
    {
        $brand_id = request('id');

        $brand = Brands::where('id', $brand_id)->first();

        try {
            $brand->status = !$brand->status;
            $brand->save();

            return response()->json(
                [
                    'status' => 'success',
                    'brand' => $brand
                ]
            );
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving brand. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
    }
}
