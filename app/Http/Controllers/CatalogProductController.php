<?php

namespace App\Http\Controllers;

use App\BrandProducts;
use Exception;
use App\Helper;
use App\Options;
use App\Product;
use App\Category;
use App\ProductImages;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Session\Session;
use App\Http\Requests\CreateProductRequest;

class CatalogProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (request()->has(['filter_category'])) {
        //     dd(request('filter_category'));
        // }

        $q = isset($_GET['q']) ? $_GET['q'] : '';
        // $allProducts =  $q ? Product::where('status', 1)->where('name', 'like', "%$q%")->paginate(5) : Product::paginate(10);

        $allProducts = Product::with('productCategory');

        if (request()->has(['filter_category'])) {
            $allProducts = $allProducts->join(
                'product_categories', function ($join) {
                    $join->on('product_categories.product_id', '=', 'products.id')
                        ->where('product_categories.product_category_id', '=', (int)request('filter_category'));
                }
            )->select(
                'products.id',
                'products.name',
                'products.status',
                'products.price',
                'products.special_price'
            );
        }

        if (request()->has(['q'])) {
            $allProducts = $allProducts->where('name', 'like', "%$q%");
        }

        $allProducts =  $allProducts->paginate(100);

        $all_categories = Category::all();

        // dd($all_categories);
        return view('themes.default.admin.catalog.product.index', ['all_categories' => $all_categories, 'allProducts' => $allProducts]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search($search)
    {
        $allProducts = Product::where('status', 1)->where('name', 'like', "%$search%")->paginate(5);
        return view('themes.default.admin.catalog.product.index', ['allProducts' => $allProducts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateProductRequest $request, $url = null, $options = null, $route = null)
    {
        // dd($request->category_list);
        // dd(
        //     $request->images[0],
        //     $request->images[0]->getClientOriginalName(),
        //     $request->images[0]->getRealPath()
        // );
        $prod = new Product();
        $prod->name = $request->name;
        $prod->identifier = $request->identifier ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($request->identifier)) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower($request->name));
        $prod->status = $request->status;
        $prod->stock_status = $request->stock_status;
        $prod->desc = $request->desc;
        $prod->short_desc = $request->short_desc;
        $prod->sku = $request->sku;
        $prod->price = $request->price;
        $prod->special_price = $request->special_price;
        $prod->images = '';
        $prod->video_url = $request->video_url;

        $prod->seo_title = $request->seo_title ??  $request->name;
        $prod->seo_desc = $request->seo_desc ??  $request->desc;

        try {
            $prod->save();
            // dd($prod->id);
            $prodId = $prod->id;
            $prod->order = $prod->id;
            $prod->save();

            if ($request->category_list) {
                foreach ($request->category_list as $cat => $catId) {
                    if (ProductCategory::where(['product_category_id' => $catId, 'product_id' => $prodId])->count()) {
                        continue;
                    }

                    $prodCat = new ProductCategory();
                    $prodCat->product_category_id = $catId;
                    $prodCat->product_id = $prodId;

                    $prodCat->save();

                    $cat = Category::find($catId);
                    if ($cat->parent_id != 0 && ProductCategory::where(['product_category_id' => $cat->parent_id, 'product_id' => $prodId])->count() == 0) {
                        $rootProdCat = new ProductCategory();
                        $rootProdCat->product_category_id = $cat->parent_id;
                        $rootProdCat->product_id = $prodId;

                        $rootProdCat->save();
                    }
                }
            }

            $storage = new StorageController();
            $path = $storage->createProductImageFolder($prodId);
            $thumbPath = $storage->createProductThumbImageFolder($prodId);
            if ($request->images) {
                $images = $request->images;
                foreach ($images as $image) {
                    $org_name = $image->getClientOriginalName();
                    $type = $image->getMimeType();
                    $realPath = $image->getRealPath();
                    $ext = $image->extension();
                    $imageFile = $realPath . '/' . $org_name;

                    $file = $path . '/' . $org_name;
                    $thumbFile = $thumbPath . '/' . $org_name;

                    if (!file_exists('public/' . $file)) {
                        // dd(
                        //     [
                        //         'org name' => $org_name,
                        //         'type' => $type,
                        //         'real path' => $realPath
                        //     ]
                        // );

                        // $image->move(base_path($path),$image->getClientOriginalName());
                        // Storage::disk('local')->put('file.txt', 'Contents');
                        // Storage::copy($path.'/'.$org_name, $imageFile);
                        // Storage::put('filename', $file_content);
                        // $image->store($path);
                        $image->storeAs(
                            // $path, 'test'.'.'.$ext
                            'public/' . $path,
                            $org_name
                        );
                        // $filename =  $image->getClientOriginalName() . '.jpg';
                        // $filepath = public_path($path);

                        // move_uploaded_file($_FILES['images']['tmp_name'], $filepath.$filename);

                        // // Note that here, we are copying the destination of your moved file.
                        // // If you try with the origin file, it won't work for the same reason.
                        // copy($filepath.$filename, public_path($path).$filename);

                        $productImage = ProductImages::where(
                            [
                                'product_id' => $prodId,
                                'image_path' => $file
                            ]
                        );
                        if ($productImage->count() == 0) {
                            $productImage = new ProductImages();
                            $productImage->product_id = $prodId;
                            $productImage->image_path = url('storage/'. $file);
                            $productImage->image_org_name = $org_name;

                            // $thumbImg = Image::make(url('storage/'. $file))->resize(300, 200)->save(url('storage/thumb/'. $file));
                            // $thumbImg = Image::make($image->getRealPath())->resize(300, 200);
                            // $thumbImg->save(
                            //     'public/' . $thumbFile
                            // );
                            // Image::make($realPath)->resize(300, 200)->save('public/' . $thumbFile);
                            // Image::make('storage/'. $file)->fit(340, 340)->save('public/' . $thumbFile);

                            $productImage->save();
                        }

                    }
                }
                MessageController::successMsg("Product with id {$prodId} has been created.");
            }
            return redirect('/admin/catalog/product/edit/'. $prodId);
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. ' . $e->getMessage() . ' - ' . $e->getLine());
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($url = null, $options = null, $route = null)
    {
        // dd($url, $options, $route);

        $view = 'themes.default.admin.catalog';
        $view .= $url ? '.' . $url : '';
        $view .= $options ? '.' . $options : '';
        return view(
            $view,
            [
                'url' => $url,
                'options' => $options,
                'route' => $route,
                'categories' => Category::all(),
                'prod' => Product::where(['id' => $route])->with('productImages', 'productCategory')->first()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Options  $options
     * @return \Illuminate\Http\Response
     */
    public function show(Options $options)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Options  $options
     * @return \Illuminate\Http\Response
     */
    public function edit(Options $options)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Options  $options
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Options $options, $id)
    {
        // dd($request->category_list);

        $prod = Product::find($id);
        $name = $request->name;
        $status = $request->status;
        $stock_status = $request->stock_status;
        $desc = $request->desc;
        $short_desc = $request->short_desc;
        $sku = $request->sku;
        $price = $request->price;
        $special_price = $request->special_price;
        $seo_title = $request->seo_title ?? $request->name;
        $seo_desc = $request->seo_desc ?? $request->desc;
        $seo_keywords = $request->seo_keywords;
        $video_url = $request->video_url;

        $prod->identifier = $request->identifier ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower($request->identifier)) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower($request->name));
        $prod->name  = $name;
        $prod->status =  $status;
        $prod->stock_status =  $stock_status;
        $prod->desc = $desc;
        $prod->short_desc = $short_desc;
        $prod->price  = $price;
        $prod->special_price  = $special_price;
        $prod->seo_title = $seo_title;
        $prod->seo_desc = $seo_desc;
        $prod->seo_keywords = $seo_keywords;
        $prod->video_url = $video_url;
        $prod->sku = $sku;

        try {
            $prod->update();

            MessageController::successMsg("Product with id {$prod->id} has been updated.");
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. ' . $e->getMessage() . ' - ' . $e->getLine());
        }

        if ($request->images) {
            $images = $request->images;
            $storage = new StorageController();
            $path = $storage->createProductImageFolder($prod->id);
            foreach ($images as $image) {
                $org_name = $image->getClientOriginalName();
                $type = $image->getMimeType();
                $realPath = $image->getRealPath();
                $ext = $image->extension();
                $imageFile = $realPath . '/' . $org_name;

                $file = $path . '/' . $org_name;

                if (!file_exists('public/' . $file)) {
                    $image->storeAs(
                        'public/' . $path,
                        $org_name
                    );

                    $productImage = ProductImages::where(
                        [
                            'product_id' => $prod->id,
                            'image_path' => $file
                        ]
                    );
                    if ($productImage->count() == 0) {
                        $productImage = new ProductImages();
                        $productImage->product_id = $prod->id;
                        $productImage->order = $prod->id;
                        $productImage->image_path = url('storage/'. $file);

                        $productImage->save();
                    }

                }
            }
            MessageController::successMsg("Product with id {$prod->id} has been created.");
            return redirect()->back();
        }

        $category_list = $request->category_list;

        try {
            ProductCategory::where('product_id', $id)->delete();
            if ($category_list) {
                foreach ($category_list as $key => $value) {
                    $productCategory = ProductCategory::where(
                        [
                            'product_id' => $id,
                            'product_category_id' => $value
                        ]
                    )->get();
                    if ($productCategory->count()) {
                        continue;
                    }
                    $productCategory = new ProductCategory();
                    $productCategory->product_id = $id;
                    $productCategory->product_category_id = $value;
                    $productCategory->save();

                    $cat = Category::find($value);
                    if ($cat->parent_id != 0 && ProductCategory::where(['product_category_id' => $cat->parent_id, 'product_id' => $id])->count() == 0) {
                        $rootProdCat = new ProductCategory();
                        $rootProdCat->product_category_id = $cat->parent_id;
                        $rootProdCat->product_id = $id;

                        $rootProdCat->save();
                    }
                }
            }

            MessageController::successMsg("Product Category for with id {$prod->id} has been updated.");
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. ' . $e->getMessage() . ' - ' . $e->getLine());
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Options  $options
     * @return \Illuminate\Http\Response
     */
    public function destroy(Options $options, $id)
    {
        // dd($id);

        $brandProduct = BrandProducts::where('product_id', $id)->first();

        if ($brandProduct) {
            $brandProduct->delete();
        }

        $prod = Product::where('id', $id)->first();
        if ($prod) {
            try {
                $prod->delete();

                $path = 'public/media/images/catalog/products/'.$id;
                if (Storage::exists($path)) {
                    Storage::deleteDirectory($path);
                }

                return redirect()->back();
            } catch (Exception $e) {
                dd($e->getMessage());
            }
        }
    }

    public function deleteImage(ProductImages $prodImg, $product_id, $image_id)
    {
        $img = $prodImg::where(['product_id' => $product_id, 'id' => $image_id])->first();
        // dd($img, $product_id, $image_id);
        try {
            $img->delete();
            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'Image has been deleted'
                ], 200
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Error deleting image',
                    'error' => $e->getMessage() . ' ' . $e->getLine()
                ], 200
            );
        }

        return redirect()->back();
    }

    public function deleteProductImage()
    {
        $product_id = request('product_id');
        $id = request('id');

        $productImages = new ProductImages();
        $img = $productImages::where(['product_id' => $product_id, 'id' => $id])->first();
        // dd($img, $product_id, $image_id);
        try {
            $img->delete();

            $all_images = $productImages::where('product_id', $product_id)->get();
            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'Image has been deleted',
                    'all_images' => $all_images
                ], 200
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Error deleting image',
                    'error' => $e->getMessage() . ' ' . $e->getLine()
                ], 200
            );
        }

        return redirect()->back();
    }

    public function updateImageOrderId(ProductImages $prodImages)
    {
        $images = request()->images;

        $message['error'] = [];
        $message['success'] = [];

        foreach ($images as $key => $item) {
            $productImage = $prodImages::find($item['id']);
            $productImage->order = $key;
            try {
                $productImage->update();
                $message['success'][] = ' Saved '. $productImage->id . ' | order: '.  $productImage->order;
            } catch (Exception $e) {
                $message['error'][] = ' ' . $e->getMessage();
            }
        }

        return response()->json(
            [
                'images' => $images,
                'message' => $message
            ], 200
        );
    }

    public function updateOrder(Product $prod, ProductCategory $prod_cat)
    {
        $cat_products = request()->data;

        $message['error'] = [];
        $message['success'] = [];

        foreach ($cat_products as $key => $item) {
            // dd($item['product']['id']);
            $product = $prod::find($item['product']['id']);
            $cat_prod = $prod_cat::where('product_id', $item['product']['id'])->first();

            // dd($product);
            $product->order = $key;
            try {
                $product->update();
                $message['success'][] = ' Saved '. $product->id . ' | order: '.  $product->order;

                $cat_prod->order = $key;
                $cat_prod->save();

            } catch (Exception $e) {
                $message['error'][] = ' ' . $e->getMessage();
            }
        }
    }

    public function updataProductData()
    {
        $data = request()->all();

        $prod = Product::where('id', $data['id'])->first();

        if (!$prod) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Error finding product'
                ], 200
            );
        }

        try {
            foreach ($data as $key => $value) {
                if ($key == 'id') continue;
                $prod->$key = $value;
//                dd($prod->$key, $key, $value);
            }
            $prod->save();
            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'Product has been updated',
                    'all_images' => $prod
                ], 200
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Error updating product',
                    'error' => $e->getMessage() . ' ' . $e->getLine()
                ], 200
            );
        }

    }
}
