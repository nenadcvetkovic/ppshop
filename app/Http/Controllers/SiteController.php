<?php

namespace App\Http\Controllers;

use App\BrandProducts;
use App\Pages;
use App\Brands;
use App\Slider;
use App\Product;
use App\Category;
use App\CmsBlock;
use App\CmsPages;
use Carbon\Carbon;
use App\SliderItems;
use App\Configuration;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;
use Illuminate\Routing\Route;

class SiteController extends Controller
{

    public function __construct(Request $request, Route $route)
    {
        $head = $this->getHeadData($request, $route);
        view()->share('head', $head);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conf = new ConfigurationController();
        $slider = [];

        $slider['status'] = $conf->getSliderStatus();
        $slider['slider_id'] = $conf->getActiveSlider();
        $slider['items'] = SliderItems::where('slider_id', $slider['slider_id'])->get();

        $featured_products = Product::where('created_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())->get();
        $mostView = Product::where('views', '>=', 1)->orderBy('views', 'desc')->take(4)->get();

        $topSale = Product::whereNotNull('special_price')->get();
        // Model::whereNotNull('lunch_option');
        // Model::where('lunch_option' '<>', '');
        $allBrands = Brands::where('status', 1)->get();

        $pageContent = CmsPages::where(['identifier' => 'home', 'status' => 1])->first();
        if ($pageContent) {
            return $this->viewPage('home');
        }
        $latestPosts = 'latestPosts';


        return view('themes.default.front.index', [
            'conf' => $conf,
            'slider' => $slider,
            'featured_products' => $featured_products,
            'mostView' => $mostView,
            'topSale' => $topSale,
            'allBrands' => $allBrands,
            'latestPosts' => $latestPosts
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function viewBrands()
    {
        $actions = request()->route()->action;
        $params = request()->route()->parameters;

        $brand = $params['brand'];
        $brandData = Brands::where('identifier', $brand)->first();
        $allProducts = null;

        if ($brandData) {
            $allBrandProducts = BrandProducts::where('brand_id', $brandData->id)->get();
            $list = [];
            foreach ($allBrandProducts as $prod) {
                $list[] = $prod->product_id;
            }

            $allProducts = Product::where('status', 1)->whereIn('id', $list)->paginate(10);
        }

        // dd($actions, $params, $brandData);
        // dd($allBrandProducts, $list, $allProducts);
        return view('themes.default.front.catalog.brands.index', ['allProducts' => $allProducts]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($identifier = null)
    {
        $conf = new ConfigurationController();
        $prod_per_page = $conf->prodPerPage();
        $action = request()->route()->action;
        // $template = 'index';
        // if (request()->route()->action) {
        //     $template = request()->route()->action['template'];
        // }
        // dd($identifier);
        if ($action['type'] == 'catalog.products') {
            $prod = Product::where(['status' => 1, 'identifier'=> $identifier])->first();
            if (!$prod) {
                return view('themes.default.layouts.frontend.404');
            }
            $prod->views = $prod->views + 1;
            $prod->save();

            return view('themes.default.front.catalog.product.view',
                [
                    'prod' => $prod,
                    'page' => 'product'
                ]
            );
        }
        if ($identifier != null) {
            $cat = Category::where('identifier', $identifier)->first();

            if (!$cat) {
                return view('themes.default.layouts.frontend.404');
            }
            $productsByCat = ProductCategory::where(['product_category_id' => $cat->id])->orderBy('order')->pluck('product_id');
            // dd($productsByCat);

            $allProducts = Product::where('status', 1)->whereIn('id', $productsByCat)->orderBy('order')->paginate($prod_per_page);

            return view('themes.default.front.catalog.category.index',
                [
                    'cat' => $cat,
                    'allProducts' => $allProducts,
                    'page' => 'category',
                    'layout' => $conf->getCatalogLayout()
                ]
            );
        }

        $allProducts = Product::where('status', 1)->paginate($prod_per_page);

        return view('themes.default.front.catalog.category.index',
            [
                'allProducts' => $allProducts,
                'page' => 'catalog',
                'layout' => $conf->getCatalogLayout(),
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewPage($identifier = null)
    {
        $cmsPages = new CmsPagesController();
        $conf = new ConfigurationController();
        $prod_per_page = $conf->prodPerPage();
        // dd(request()->route()->action);
        if ($identifier) {
            if ($pageContent = CmsPages::where(['identifier' => $identifier])->first()) {
                return $cmsPages->viewPage($pageContent, $identifier);
            }

            $cat = Category::where('identifier', $identifier)->first();
            if ($cat) {
                return view('themes.default.layouts.frontend.404');
            }

            $productsByCat = ProductCategory::where(['product_category_id' => $cat->id])->pluck('product_id');
            $allProducts = Product::where('status', 1)->whereIn('id', $productsByCat)->paginate($prod_per_page);

            return view('themes.default.front.catalog.category.index', ['cat' => $cat, 'allProducts' => $allProducts]);
        }

        if (request()->route()->action && request()->route()->action['page']) {
            $content = '';
            $page = request()->route()->action['page'];
            if ($block = request()->route()->action['block']) {
                $blockContent = CmsBlock::where('identifier', $block)->first();
                $content .= $blockContent->content;
            }
            return view('themes.default.front.pages.'.$page, ['content' => $content]);
        }
    }

    public function contact (ContactFormRequest $request)
    {
        dd($request->all());
        return redirect()->back();
        return redirect()->route('contact.success');
    }

    public function getHeadData($request, $route)
    {

        // dd($route);
        switch($route->uri) {
        case '/':
                $head = [
                    'meta' => [
                        'author' => 'Nenad Cvetkovic',
                        'description' => 'Short descritpion for homepage'
                    ]
                ];
                return $head;
            break;
        }

        return null;
    }

}
