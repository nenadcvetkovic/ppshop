<?php

namespace App\Http\Controllers;

use App\Helper;
use App\CmsBlock;
use App\Http\Requests\UpdateCmsBlockRequest;
use Exception;
use Illuminate\Http\Request;

class CmsBlockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCmsBlocks = CmsBlock::all();
        return view('themes.default.admin.content.blocks.index', ['allCmsBlocks' => $allCmsBlocks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(request()->all());

        $name = request('name');
        $identifier = request('identifier') ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower(request('identifier'))) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower(request('name')));
        $status = request('status');
        $desc = request('desc');
        $content = request('content');

        $block = new CmsBlock();

        $block->name = $name;
        $block->identifier = $identifier;
        $block->status = $status;
        $block->desc = $desc;
        $block->content = $content;

        try {
            $block->save();

            MessageController::successMsg("CMS Block with id {$block->id} has been saved.");
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving CMS Block. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
        return redirect()->route('content.block.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CmsBlock  $cmsBlock
     * @return \Illuminate\Http\Response
     */
    public function show(CmsBlock $cmsBlock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CmsBlock  $cmsBlock
     * @return \Illuminate\Http\Response
     */
    public function edit(CmsBlock $cmsBlock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CmsBlock  $cmsBlock
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCmsBlockRequest $request, CmsBlock $cmsBlock, $id)
    {
        $block = $cmsBlock::find($id);

        // dd(
        //     count($block)
        // );
        if ($block->id) {
            $name = $request->name;
            $identifier = request('identifier') ? preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', "-", strtolower(request('identifier'))) : preg_replace('/[\@+\#+\$+\s+\%+\-\/\,\;+]/', '-', strtolower(request('name')));
            $status = $request->status;
            $desc = $request->desc;
            $content = $request->content;

            $block->name = $name;
            $block->identifier = $identifier;
            $block->status = $status;
            $block->desc = $desc;
            $block->content = $content;

            try {
                $block->save();

                MessageController::successMsg("CMS Block with id {$block->id} has been saved.");
            } catch (Exception $e) {
                Helper::err_log($e->getMessage());
                MessageController::errorMsg('Error saving CMS Block. ' . $e->getMessage() . ' - ' . $e->getLine());
            }
        } else {
            MessageController::errorMsg('Error saving CMS Block. Block does not exist!');
        }

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CmsBlock  $cmsBlock
     * @return \Illuminate\Http\Response
     */
    public function destroy(CmsBlock $cmsBlock, $id)
    {
        $block = $cmsBlock->find($id);

        try {
            $block->delete();

            MessageController::successMsg("CMS Block with id {$id} has been deleted.");
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving CMS Block. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
        return redirect()->route('content.block.index');
    }

    public function view($id = null)
    {
        $view = request()->route()->action['view'];

        $block = CmsBlock::where(['id' => $id])->first();

        return view('themes.default.admin.content.blocks.'.$view, ['block' => $block]);
    }

    /* Api */
    public function updateStatus()
    {
        $block_id = request('id');

        $block = CmsBlock::where('id', $block_id)->first();

        try {
            $block->status = !$block->status;
            $block->save();

            return response()->json(
                [
                    'status' => 'success',
                    'block' => $block
                ]
            );
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving block. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
    }
}
