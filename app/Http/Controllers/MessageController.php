<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public static function successMsg($msg)
    {
        request()->session()->flash('success', $msg);
    }

    public static function errorMsg($msg)
    {
        request()->session()->flash('error', $msg);
    }
}
