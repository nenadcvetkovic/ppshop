<?php

namespace App\Http\Controllers\System\Site;

use Exception;
use App\Helper;
use App\Configuration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\MessageController;

class MaintenanceController extends Controller
{
    protected $config;

    public function __construct()
    {
        $this->config = new Configuration();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function action($action)
    {
        // php artisan cache:clear
        // php artisan route:clear
        // php artisan config:clear
        // php artisan view:clear

        try {
            switch ($action) {
                case 'down':
                    Artisan::call('down',
                        [
                            '--allow' => $this->config->getWhitelistIps()
                        ]
                    );
                    MessageController::errorMsg("Maintenace is enabled");
                    break;
                case 'up':
                    Artisan::call('up');
                    MessageController::successMsg("Maintenace is disabled");
                    break;
            }
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
        return redirect()->back();
    }
}
