<?php

namespace App\Http\Controllers\System\Tools;

use Exception;
use App\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\MessageController;

class CacheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('themes.default.admin.system.cache.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function action($action)
    {
        // php artisan cache:clear
        // php artisan route:clear
        // php artisan config:clear
        // php artisan view:clear

        try {
            switch ($action) {
                case 'clear-cache':
                    Artisan::call('cache:clear');
                    MessageController::successMsg("Cache clear has been finished");
                    break;
                case 'clear-route':
                    Artisan::call('route:clear');
                    MessageController::successMsg("Cache clear oute has been finished");
                    break;
                case 'clear-config':
                    Artisan::call('config:clear');
                    MessageController::successMsg("Cache clear config has been finished");
                    break;
                case 'clear-view':
                    Artisan::call('view:clear');
                    MessageController::successMsg("Cache clear view has been finished");
                    break;
            }
        } catch (Exception $e) {
            Helper::err_log($e->getMessage());
            MessageController::errorMsg('Error saving product. ' . $e->getMessage() . ' - ' . $e->getLine());
        }
        return redirect()->back();
    }
}
