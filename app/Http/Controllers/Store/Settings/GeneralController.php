<?php

namespace App\Http\Controllers\Store\Settings;

use Exception;
use App\Helper;
use App\Configuration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\ConfigurationController;

class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings_configuration = new ConfigurationController();
        return view('themes.default.admin.stores.settings.general.index', [
            'settings_configuration' => $settings_configuration
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        foreach ($request->all() as $key => $value) {
            if ($key == "_token" || $value == '') {
                continue;
            }
            try {
                $settings_configuration = Configuration::where(['path' => $key])->first();

                if (!$settings_configuration) {
                    $settings_configuration = new Configuration();
                }

                $settings_configuration->path = $key;
                $settings_configuration->value = $value;
                $settings_configuration->save();
            } catch (Exception $e) {
                Helper::err_log($e->getMessage());
                MessageController::errorMsg('Error saving configuration. ' . $e->getMessage() . ' - ' . $e->getFile() . ' - ' . $e->getLine());
                return redirect()->back();
            }
        }
        MessageController::successMsg("Configuration has been saved");
        return redirect()->route('store.settings.general.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
