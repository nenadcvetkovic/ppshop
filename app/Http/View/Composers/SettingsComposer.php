<?php

namespace App\Http\View\Composers;

use App\Category;
use App\Configuration;
use App\Http\Controllers\ConfigurationController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PriceController;
use Illuminate\View\View;
use App\Repositories\UserRepository;
use App\Settings;
use App\Translate;

class SettingsComposer
{
    protected $settings;
    protected $configuration;
    protected $price;
    protected $t;
    protected $allCategories;
    protected $filteredCategries;

    /**
     * Create a new profile composer.
     *
     * @param
     * @return void
     */
    public function __construct(
        Settings $settings,
        ConfigurationController $configuration,
        PriceController $price,
        Translate $t,
        Category $allCategories,
        MenuController $menu
    )
    {
        // Dependencies automatically resolved by service container...
        $this->settings = $settings;
        $this->configuration = $configuration;
        $this->price = $price;
        $this->t = $t;
        $this->allCategories = $allCategories;
        $this->menu = $menu;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(
            [
                'settings' => $this->settings,
                'configuration' => $this->configuration,
                'price' => $this->price,
                't' => $this->t,
                'allCategories' => $this->allCategories::all(),
                'filteredCategries' => $this->menu::getMenuView()
            ]
        );
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function create(View $view)
    {
        $view->with(
            [
                'settings' => $this->settings,
                'configuration' => $this->configuration,
                'price' => $this->price,
                't' => $this->t,
                'allCategories' => $this->allCategories::all(),
                'filteredCategries' => $this->menu::getMenuView()
            ]
        );
    }
}
