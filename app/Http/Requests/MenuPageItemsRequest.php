<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuPageItemsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'menu_id' => 'required',
            'parent_id' => 'required',
            'identifier' => 'required|unique:menu_items'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'type.required' => 'Polje ":attribute" nije popunjeno',
            'menu_id.required' => 'Vrednost ":attribute" nije poznata',
            'parent_id.required' => 'Vrednost ":attribute" nije poznata',
            'title.required'  => 'Polje ":attribute" nije popunjeno',
            'url.required'  => 'Polje ":attribute" nije popunjeno'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'type' => 'Tip',
            'menu_id' => 'Menu ID',
            'parent_id' => 'Parent ID',
            'title' => 'Ime',
            'url' => 'Url'
        ];
    }
}
