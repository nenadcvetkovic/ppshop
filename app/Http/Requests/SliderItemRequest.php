<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slider_id' => 'required',
            'content_from_url' => 'required_without:content_from_file',
            'content_from_file' => 'required_without:content_from_url'
        ];
    }
}
