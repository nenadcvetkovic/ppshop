<?php

namespace App;

use App\Http\Controllers\TranslateController;
use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{
    public function translate(String $string)
    {
        $trans = new TranslateController();
        return $trans->translate($string);
    }
}
