<?php

namespace App;

use App\Http\Controllers\ConfigurationController;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    public $config;

    public function __construct()
    {
        $this->config = new ConfigurationController();
    }
    public function getTest()
    {
        return 'test';
    }

    public function getHeadData($request)
    {
        return $this->config->getHeadData($request);
    }

    public function getTitle($request)
    {
        return $this->config->getTitle($request);
    }

    /* Product catalog configuration */

    public function prodPerRow()
    {
        return $this->config->prodPerRow();
    }

    public function showCatalogProdSku()
    {
        return $this->config->showCatalogProdSku();
    }

    public function showCatalogProdCat()
    {
        return $this->config->showCatalogProdCat();
    }

    public function showCatalogProdPrice()
    {
        return $this->config->showCatalogProdPrice();
    }

    public function showCatalogProdDesc()
    {
        return $this->config->showCatalogProdDesc();
    }

    public function showCatalogProdStockStatus()
    {
        return $this->config->showCatalogProdStockStatus();
    }

    public function showProdSku()
    {
        return $this->config->showProdSku();
    }

    public function showProdCat()
    {
        return $this->config->showProdCat();
    }

    public function showProdPrice()
    {
        return $this->config->showProdPrice();
    }

    public function showProdDesc()
    {
        return $this->config->showProdDesc();
    }

    public function showProdStockStatus()
    {
        return $this->config->showProdStockStatus();
    }

    public function getWhitelistIps()
    {
        return $this->config->getWhitelistIps();
    }

    public function getBodyColor()
    {
        return $this->config->getBodyColor();
    }

    /* Header */
    public function getHeaderColor()
    {
        return $this->config->getHeaderColor();
    }

    /* Links */
    public function getLinkColor()
    {
        return $this->config->getLinkColor();
    }

    public function getLinkHoverColor()
    {
        return $this->config->getLinkHoverColor();
    }

    /*  Menu */
    public function getMenuLinkColor()
    {
        return $this->config->getMenuLinkColor();
    }

    public function getMenuLinkHoverColor()
    {
        return $this->config->getMenuLinkHoverColor();
    }

    public function getMenuLinkBGColor()
    {
        return $this->config->getMenuLinkBGColor();
    }

    public function getMenuLinkBGHoverColor()
    {
        return $this->config->getMenuLinkBGHoverColor();
    }

    /* Logo */
    public function getLogoSrc()
    {
        return $this->config->getLogoSrc();
    }
}
