<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandProducts extends Model
{
    /**
     * Get the phone record associated with the brands.
     */
    public function brands()
    {
        return $this->hasMany('App\Brands');
    }

    /**
     * Get the phone record associated with the product.
     */
    public function product()
    {
        return $this->hasMany('App\Product', 'id');
    }

}
