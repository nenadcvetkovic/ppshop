<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageImage extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function page()
    {
        return $this->hasMany('App\CmsPages');
    }
}
