<?php

namespace App\Helper;

use App\Brands;
use App\Product;
use App\CmsBlock;
use Carbon\Carbon;
use App\SliderItems;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\ConfigurationController;
use App\Slider;

class Block extends Model
{
        /**
     * Return cotnent with block indentifier replaced with content
     *
     * @param Object $pageContent
     * @return Object $conten
     */
    public static function replaceIdentifierWithBlockContent($pageContent)
    {
        $blockStart = '[[[';
        $blockEnd = ']]]';

        $content = isset($pageContent) && isset($pageContent->content) ? $pageContent->content : '';
        if ($content) {
            $blocks = self::getIdentifier($blockStart, $blockEnd, $pageContent->content);

            /* Import blocks into page */
            foreach ($blocks as $block) {
                $blockContent = CmsBlock::where(['identifier' => trim($block)])->first();

                if ($blockContent) {
                    $content = str_replace($blockStart.$block.$blockEnd, $blockContent->content, $content);
                } else {
                    $content = str_replace($blockStart.$block.$blockEnd, $blockContent->content, $content);
                }
            }

            // dd($pageContent->content, $content);
        }


        return $content;
    }

     public static function getBlockData($block)
     {
        // return $block;

        $blockData = [];
        $content = '';

        $block = preg_replace('/[\"\']+/','', $block);
        $blockData = CmsBlock::where(['identifier' => trim($block), 'status' => 1])->first();

        $content = Block::replaceIdentifierWithBlockContent($blockData); //HERE
        // if ($block == 'footer') {
        //     dd($blockData, $content); //HERE
        // }
        $content = Block::replaceIdentifierWithSectionContent($content, $block);
        $content = Block::replaceContentWithBlockData($content);

        if ($block == 'footer') {
            // dd($block, $blockData, $content); //HERE
        }
        return $content ? $content : '';

     }

     public static function replaceIdentifierWithSectionContent($pageContent, $block = null)
     {

         $content = isset($pageContent->content) ? $pageContent->content : $pageContent;
         $sections = self::getIdentifier('\[\[sections\=\"', '\"\]\]', $content);

        //  dd($sections, $pageContent, $content);

         $conf = new ConfigurationController();
         $slider = [];

         $slider['status'] = $conf->getSliderStatus();
         $slider['slider_id'] = $conf->getActiveSlider();
        $slider['items'] = SliderItems::where(
            [
                'status' => 1,
                'slider_id' => $slider['slider_id']
            ]
        )->get();

         $featured_products = Product::where('created_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())->take(4);
         $mostView = Product::where('views', '>=', 1)->orderBy('views', 'desc')->take(4)->get();

         $topSale = Product::whereNotNull('special_price')->get();
         // Model::whereNotNull('lunch_option');
         // Model::where('lunch_option' '<>', '');
         $allBrands = Brands::where('status', 1)->get();

         /* Import sections into page */
         foreach ($sections as $section) {
            $sectionView = 'themes.default.front.sections.'.$section;

            if (strlen($sectionView) > 200) continue;
            if (!view()->exists($sectionView)) continue;


            $sectionContent = view($sectionView)->with(
                [
                    'conf' => $conf,
                    'slider' => $slider,
                    'featured_products' => $featured_products,
                    'mostView' => $mostView,
                    'topSale' => $topSale,
                    'allBrands' => $allBrands
                ]
            )->render();
            $sectionIdentifier = '[[sections="'.$section.'"]]';

            // dd($section, $sectionView, $sectionContent);

            if ($sectionContent) {
                $content = str_replace($sectionIdentifier, $sectionContent, $content);
            } else {
                $content = str_replace($sectionIdentifier, '', $content);
            }

            if ($section == 'footer-subscribe') {
                // dd(view()->exists($sectionView), $sectionView, $sectionIdentifier, '[[sections="'.$section.'"]]', $section, $sections, $sectionContent, $content); //HERE
            }
         }

        $sections2ParamData = self::getSectionsParams($content); //{sections name="slider" identifier="homepage-1"}
        // dd($sections2ParamData);
        foreach ($sections2ParamData as $key => $value) {
            $source = ucfirst($value['name']);
            $identifier = $value['identifier'];

            $className = 'App\\' . $source;
            $item = new $className;

            $itemData = $item::where(
                [
                    'identifier' => $identifier,
                    'status' => 1
                ]
            )->first();

            // dd($key, $source, $identifier, $itemData);
            if (true) {
                $content = str_replace('{sections '.$key.'}', 'test test test', $content);
            } else {
                $content = str_replace('{sections '.$key.'}', '', $content);
            }
        }

         return $content;
     }

    public static function replaceIdentifierWithWidgetContent($pageContent)
    {
        $content = isset($pageContent->content) ? $pageContent->content : $pageContent;

        $widgets = self::getIdentifier('[[widget="', '"]]', $content);

        /* Import widgets into page */
        foreach ($widgets as $widget) {
            $widgetView = 'themes.default.front.widget.'.$widget;
            if (!view()->exists($widgetView)) continue;

            $widgetContent = view($widgetView)->render();

            // dd($widgetView, $widgetContent);

            if ($widgetContent) {
                $content = str_replace('[[widget="'.$widget.'"]]', $widgetContent, $content);
            } else {
                $content = str_replace('[[widget="'.$widget.'"]]', '', $content);
            }
        }

        return $content;
    }

    public static function replaceContentWithBlockData($content)
    {
        $tag_open = "\[block\ id\=\'";
        $tag_close = "\'\]";

        // $matches = '';
        // $patterns = "/$tag_open(.*?)$tag_close/";

        // preg_match_all($patterns, $content, $matches, PREG_PATTERN_ORDER);
        // dd($patterns, $matches[1]);

        $matches = self::getIdentifier($tag_open, $tag_close, $content);

        if (isset($matches[0])) {
            $blockContent = CmsBlock::where(['identifier' => $matches[0], 'status' => 1])->first();
            if ($blockContent) {
                $content = str_replace("[block id='".$matches[0]."']", $blockContent->content, $content);
            } else {
                $content = str_replace("[block id='".$matches[0]."']", '', $content);
            }
            // dd('trs', $matches[0], $content);
        }

        return $content;
    }

    public static function getSectionsParams($content) {

        $sections2ParamData = [];
        $sections = self::getIdentifier('\{sections ', '\}', $content);
        // dd($sections);

        if ($sections) {
            $sections2ParamData['full'] = $sections[0];
            $sections2ListRaw = [];
            foreach ($sections as $item) {
                $sections2ListRaw[$item] = explode(' ', $item);
            }
            // dd($sections2ListRaw);

            $sections2ListRaw2 = [];
            foreach ($sections2ListRaw as $key => $value) {
                foreach ($value as $item) {
                    $x = explode('=', $item);
                    $sections2ListRaw2[$key][$x[0]] = preg_replace('/[\"\']+/', '', $x[1]);
                }
            }
            // dd($sections2ListRaw2);
            return $sections2ListRaw2;

            // $sections2ListRaw3 = [];
            // foreach ($sections2ListRaw2 as $item) {
            //     $sections2ListRaw3[] = explode('=', $item[0]);
            // }

            // foreach ($sections2ListRaw3 as $item) {
            //     $sections2ParamData['sections'][$sections2ParamData['full']][$item[0]] = preg_replace('/[\"\']+/', '', $item[1]);
            // }

            // return $sections2ParamData;
        }
        return $sections2ParamData;

    }

    public static function getWidgetParams($pageContent)
    {
        /*
        * Widgets param example {widget name='' ids='' type=''}
        * $name
        * $ids
        * $type
        */
        $content = isset($pageContent->content) ? $pageContent->content : $pageContent;

        $widgets = self::getIdentifier('{widget ', '}', $content);
        // dd($widgets);
        foreach ($widgets as $widget) {
            $widgetData = explode(' ', $widget);
            $param = [];
            foreach ($widgetData as $item) {
                $paramData = explode('=', $item);
                if (count($paramData) > 1) {
                    $param[$paramData[0]] = ${$paramData[0]} = preg_replace('/\"/', '', $paramData[1]);
                    $widgetContent = '<p>Widgrt Part must be created</p>'; //TODO
                    $content = str_replace('{widget '.$widget.'}', $widgetContent, $content);
                } else {
                    $content = str_replace('{widget '.$widget.'}', '', $content);
                }
            }
            // dd('test', $widgetData, $param, explode(",", preg_replace('/\"/', '', $param['ids'])));

        }
        return $content;
    }

    public static function getIdentifier($tag_open = '[[', $tag_close = ']]', $string = ''): Array
    {
        $result = [];

        // foreach (explode($tag_open, $string) as $key => $value) {
        //     if (strpos($value, $tag_close) !== false) {
        //          $result[] = substr($value, 0, strpos($value, $tag_close));
        //     }
        // }
        // if ($result) {
        //     return $result;
        // }
        $matches = '';
        $patterns = "/$tag_open(.*?)$tag_close/";
        // $patterns = '/\[\[sections=\"(.*)\"\]\]/U';

        preg_match_all($patterns, $string, $matches, PREG_PATTERN_ORDER);
        // dd($patterns, $matches[1]);
        return $matches[1] ?? [];
     }
}
