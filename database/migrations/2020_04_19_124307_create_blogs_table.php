<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('identifier')->unique();
            $table->boolean('status')->default(0);
            $table->boolean('show_post_title')->default(1);
            $table->text('content')->nullable();
            $table->text('short-content')->nullable();
            $table->string('image_path')->nullable();
            $table->boolean('show_top_post_image')->default(0);
            $table->string('top_image_path')->nullable();
            $table->integer('author')->unsigned()->defalt(0);
            $table->string('seo_title')->nullable();
            $table->text('seo_desc')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
