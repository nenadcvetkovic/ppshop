<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('slider_id');
            $table->text('name')->nullable();
            $table->text('status');
            $table->text('image_path')->nullable();
            $table->string('image_type')->nullable()->default('file');
            $table->text('content')->nullable();
            $table->text('caption_1')->nullable();
            $table->text('caption_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider_items');
    }
}
