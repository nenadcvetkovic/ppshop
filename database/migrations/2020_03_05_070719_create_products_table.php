<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->string('identifier')->unique();
            $table->boolean('status')->default(0);
            $table->boolean('stock_status')->default(1);
            $table->text('desc')->nullable();
            $table->text('short_desc')->nullable();
            $table->string('sku')->nullable();
            $table->integer('price')->default(0);
            $table->integer('special_price')->nullable();
            $table->text('images')->nullable();
            $table->string('video_url')->nullable();
            $table->integer('views')->default(0);
            $table->string('seo_title')->nullable();
            $table->text('seo_desc')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
