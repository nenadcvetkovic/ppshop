<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Test extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // Schema::enableForeignKeyConstraints();
        // Schema::disableForeignKeyConstraints();


        Schema::create('test', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });
        Schema::table('test', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');

            // Short answer is: in your case, if you deleted a user, all posts related to him will be deleted too.
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            // $table->dropForeign('posts_user_id_foreign');
            // $table->dropForeign(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test');
    }
}
