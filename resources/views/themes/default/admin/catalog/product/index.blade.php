@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid" id="adminApp">
    <div class="row justify-content-center">

        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            Edit category
                            {{ $settings->test() }}
                            {{-- <a href="{{ url('admin/catalog/product') }}" class="btn btn-success btn-sm">
                                <i class="fa fa-arrow-left"></i> Back to all products
                            </a> --}}
                            {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                                <i class="fa fa-file-excel-o"></i>&nbsp; Export
                            </a>  --}}
                            <a href="{{ route('catalog.product.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                                <i class="fa fa-plus"></i>&nbsp; New Item
                            </a>
                        </div>
                        <div class="card-body">
                            @include('themes.default.layouts.admin.message')

                            <div class="row mt-2 mb-2 ml-1">
                                <form action="/admin/catalog/product" method="get" role="search" class="col-12">
                                    {{ csrf_field() }}
                                        <div class="row">
                                        <div class="input-group col-3">
                                            <input type="text" class="form-control p-0" style="height: 40px; line-height: 1; font-size: 1rem;" name="q" placeholder="{{ isset($_GET['q']) ? $_GET['q'] : 'Pretrazite proizvode' }}">
                                        </div>
                                        <div class="form-group col-3">
                                            {{--  <label for="exampleFormControlSelect1">Kategorija</label>  --}}
                                            <select name="filter_category" class="form-control" id="exampleFormControlSelect1">
                                                <option disabled selected>Filter Kategorija</option>
                                                @foreach ($all_categories as $cat)
                                                <option  {{ request()->has(['filter_category']) && $cat->id == request('filter_category') ? 'selected' : '' }} value="{{ $cat->id }}">{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="input-group-btn">
                                            {{--  @if(isset($_GET['q']))  --}}
                                            @if(request()->has(['q','filter_category']))
                                                <button type="submit" class="btn btn-default ml-1">
                                                    <span class="glyphicon glyphicon-search">Trazi</span>
                                                </button>
                                                <a class="btn btn-danger ml-1" href="{{ url('/admin/catalog/product') }}">Clear</a>
                                            @else
                                                <button type="submit" class="btn btn-default ml-1">
                                                    <span class="glyphicon glyphicon-search">Trazi</span>
                                                </button>
                                            @endif
                                        </span>
                                        </div>
                                    </form>
                            </div>
                            <div class="col-12">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Special Price</th>
                                            <th scope="col">#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($allProducts as $prod)
                                            <tr is="pp-product-list"
                                                :id="{{ $prod->id }}"
                                                :img="'{{ $prod->getHoverImage() ? $prod->getHoverImage() : asset('storage/media/no-image.png') }}'"
                                                :name="'{{ $prod->name }}'"
                                                :status="{{ $prod->status }}"
                                                :price="{{ $prod->price }}"
                                                :special_price="'{{ isset($prod->special_price) ? $prod->special_price : 0 }}'"
                                            >
                                            </tr>
                                        <tr class="d-none">
                                            <th scope="row">{{ $prod->id }}</th>
                                            <th scope="row">
                                                <img src="{{ $prod->getHoverImage() ? $prod->getHoverImage() : asset('storage/media/no-image.png') }}" width="50"/>
                                            </th>
                                            <td>{{ $prod->name }}</td>
                                            <td>{{ $prod->status ? 'Enabled' : 'Disabled' }}</td>
                                            <td>{{ $prod->price }}</td>
                                            <td>{{ $prod->special_price }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Opcije
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="{{ url('admin/catalog/product/edit/'.$prod->id) }}">Izmeni</a>
                                                    <a class="dropdown-item" href="{{ route('catalog.product.delete',$prod->id) }}">Obrisi</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="col-12 m-auto">{{ $allProducts->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
