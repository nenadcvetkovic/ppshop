@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">

        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            Create product

                            <a href="{{ url('admin/catalog/product') }}" class="btn btn-success btn-sm">
                                <i class="fa fa-arrow-left"></i> Back to all products
                            </a>
                            {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                                <i class="fa fa-file-excel-o"></i>&nbsp; Export
                            </a>  --}}
                            <a href="{{ route('catalog.product.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                                <i class="fa fa-plus"></i>&nbsp; New Item
                            </a>
                        </div>
                        <div class="card-body">
                            @include('themes.default.layouts.admin.message')

                            {!! Form::open(['route'=>'catalog.product.create', 'files' => true, 'enctype' => "multipart/form-data"]) !!}

                            @include('themes.default.admin.catalog.product.form', ['page' => 'create'])

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

