<div class="col-12 ml-auto mr-auto" id="adminApp">
    {!! Form::token() !!}

    <div class="form-group pull-right">
        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
    </div>

    <ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
        <li class="nav-item waves-effect waves-light">
          <a class="nav-link active" id="default-tab-md" data-toggle="tab" href="#default-md" role="tab" aria-controls="default-md" aria-selected="true">Default</a>
        </li>
        <li class="nav-item waves-effect waves-light">
          <a class="nav-link" id="seo-tab-md" data-toggle="tab" href="#seo-md" role="tab" aria-controls="seo-md" aria-selected="false">SEO</a>
        </li>
    </ul>

    <div class="tab-content p-5" id="myTabContentMD">
        {{--  Default  --}}
        <div class="tab-pane fade show active" id="default-md" role="tabpanel" aria-labelledby="default-tab-md">
            <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                <div class="col-2"> {!! Form::label('Ime proizvoda') !!} </div>
                <div class="col-10"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime kategorije']) !!} </div>
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
                <div class="col-2">
                    {!! Form::label('Status') !!}
                </div>
                <div class="col-3">
                    {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'],
                    isset($prod->status) ? $prod->status : '0',
                    ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('status') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('stock_status') ? 'has-error' : '' }}">
                <div class="col-2">
                    {!! Form::label('Stock status') !!}
                </div>
                <div class="col-3">
                    {!! Form::select('stock_status', ['1' => 'Enabled', '0' => 'Disabled'],
                    isset($prod->stock_status) ? $prod->stock_status : '0',
                    ['class'=>'form-control alert-success', 'name'=>'stock_status', 'id'=>'stock_status']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('stock_status') }}</span>
            </div>

            <hr class="delimiter">
            <div class="col-2">
                {!! Form::label('Product images') !!}
            </div>


            @if($page == 'edit')
                {{-- <div>
                    <pp-form-image-box :images="{{ $prod->productImages }}" :product_id="{{ $prod->id }}"/>
                </div> --}}
                <div>
                    <pp-nested-images :elements="{{ $prod->productImages }}" />
                </div>
            @endif
                <div class="clearfix"></div>
            <div>
                <pp-dropzone
                @if ($page == 'edit')
                :prod_id="{{ $prod->id }}"
                @else
                :prod_id={{ $configuration->nextId('products') }}
                @endif
                />
            </div>

            {{--  <div class="input-group hdtuto control-group lst increment" >
                <div class="col-12 first-image">
                    {!! Form::file('images[]', ['type'=>'files', 'accept' => 'image/*', 'onchange' => 'preview_image(event)']) !!}
                    <img id="output_image" class="output_image" height="50" />
                </div>
                <hr class="delimiter">
                <div class="input-group-btn">
                  <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                </div>
              </div>
              <div class="clone hide d-none">
                <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                    {!! Form::file('images[]', ['type'=>'files', 'accept' => 'image/*', 'onchange' => 'preview_image(event)']) !!}
                    <img id="output_image" class="output_image" height="50" />

                  <div class="input-group-btn">
                    <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                  </div>
                </div>
              </div>  --}}

            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('video_url') ? 'has-error' : '' }}">
                <div class="col-2"> {!! Form::label('Video URL') !!} </div>
                <div class="col-10"> {!! Form::text('video_url', old('video_url'), ['class'=>'form-control', 'name'=>'video_url', 'placeholder'=>'Unesite video url']) !!} </div>
                <span class="text-danger">{{ $errors->first('video_url') }}</span>
            </div>

            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
                <div class="col-2">
                    {!! Form::label('Opis') !!}
                </div>
                <div class="col-10">

                    @if (@$prod)
                        <pp-edit-content :data="{{ $prod }}" :name="'desc'":cols="5" :rows="5" />
                    @else
                        <pp-edit-content :name="'desc'" :cols="5" :rows="5" />
                    @endif
                </div>
                <span class="text-danger">{{ $errors->first('desc') }}</span>
            </div>


            <div class="form-group row {{ $errors->has('short_desc') ? 'has-error' : '' }}">
                <div class="col-2">
                    {!! Form::label('Kratki Opis') !!}
                </div>
                <div class="col-10">
                    {!! Form::textarea('short_desc', old('short_desc'), ['class'=>'form-control', 'name'=>'short_desc', 'id'=>'short_desc', 'placeholder'=>'Kratki Opis proizvoda', 'rows'=>'3']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('short_desc') }}</span>
            </div>

            <hr class="delimiter" />

            <div class="form-group row">
                <div class="col-2">
                    {!! Form::label('Izaberite kategoriju') !!}
                </div>

                <div class="col-10">
                    <select multiple class="form-control" name="category_list[]" id="exampleFormControlSelect2">
                        @foreach ($categories as $cat)
                            <option
                            @if (isset($prod->productCategory))
                                @foreach ($prod->productCategory as $prodCat)
                                    @if($prodCat->product_category_id == $cat->id)
                                        selected="selected"
                                    @endif
                                @endforeach
                            @endif
                             value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endforeach
                      </select>
                      <small>Hold CTRL + click</small>
                </div>
              </div>

            <hr class="delimiter" />

            <div class="form-group row {{ $errors->has('sku') ? 'has-error' : '' }}">
                <div class="col-2"> {!! Form::label('Kataloski broj (SKU)') !!} </div>
                <div class="col-10"> {!! Form::text('sku', old('sku'), ['class'=>'form-control', 'sku'=>'sku']) !!} </div>
                <span class="text-danger">{{ $errors->first('sku') }}</span>
            </div>

            <hr class="delimiter" />

            <div class="form-group row {{ $errors->has('price') ? 'has-error' : '' }}">
                <div class="col-2"> {!! Form::label('Cena') !!} </div>
                <div class="col-10"> {!! Form::number('price', old('price'), ['class'=>'form-control', 'price'=>'price']) !!} </div>
                <span class="text-danger">{{ $errors->first('price') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('special_price') ? 'has-error' : '' }}">
                <div class="col-2"> {!! Form::label('Specijalna Cena') !!} </div>
                <div class="col-10"> {!! Form::number('special_price', old('special_price'), ['class'=>'form-control', 'special_price'=>'special_price']) !!} </div>
                <span class="text-danger">{{ $errors->first('special_price') }}</span>
            </div>
        </div>

        {{--  SEO  --}}
        <div class="tab-pane fade" id="seo-md" role="tabpanel" aria-labelledby="seo-tab-md">
            <div class="form-group row {{ $errors->has('seo_title') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('SEO Title') !!}
                </div>
                <div class="col-8">
                   {!! Form::text('seo_title', old('seo_title'), ['class'=>'form-control', 'name'=>'seo_title', 'placeholder'=>'SEO Title']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('seo_title') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('seo_desc') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('SEO Descripton') !!}
                </div>
                <div class="col-12">
                    {!! Form::textarea('seo_desc', old('seo_desc'), ['class'=>'form-control', 'name'=>'seo_desc', 'id'=>'seo_desc', 'placeholder'=>'SEO description', 'rows'=>'5']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('seo_desc') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('seo_keywords') ? 'has-error' : '' }}">
              <div class="col-4">
                  {!! Form::label('SEO Keywords') !!}
              </div>
              <div class="col-12">
                  {!! Form::textarea('seo_keywords', old('seo_keywords'), ['class'=>'form-control', 'name'=>'seo_keywords', 'id'=>'seo_keywords', 'placeholder'=>'Seo Keywords', 'rows'=>'5']) !!}
              </div>
              <span class="text-danger">{{ $errors->first('seo_keywords') }}</span>
          </div>

          <hr class="delimiter">
        </div>
      </div>
</div>

{{-- <script type="text/javascript">
    jQuery(document).ready(function() {
      jQuery(".btn-success").click(function(){
          var lsthmtl = jQuery(".clone").html();
          jQuery(".increment").after(lsthmtl);
      });
      jQuery("body").on("click",".btn-danger",function(){
          jQuery(this).parents(".hdtuto.control-group.lst").remove();
          console.log(jQuery(this).parents(".hdtuto.control-group.lst"));
      });
    });

    function preview_image(event){
     var reader = new FileReader();
     reader.onload = function()
     {
      //var output = document.getElementById('output_image');
      //console.log(event.target.parentNode.querySelector('.output_image'));
      var output = event.target.parentNode.querySelector('.output_image');
      output.src = reader.result;
     }
     reader.readAsDataURL(event.target.files[0]);
    }

</script> --}}
