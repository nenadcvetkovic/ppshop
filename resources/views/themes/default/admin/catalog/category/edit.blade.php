@extends('themes.default.layouts.admin.index')

@section('main-content')
<div id="adminAppp">
    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i>
                                Edit category
                                <a href="{{ url('admin/catalog/category') }}" class="btn btn-success btn-sm">
                                    <i class="fa fa-arrow-left"></i> Back to all category
                                </a>
                                {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                                    <i class="fa fa-file-excel-o"></i>&nbsp; Export
                                </a>  --}}
                                <a href="{{ route('catalog.category.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                                    <i class="fa fa-plus"></i>&nbsp; New Category Item
                                </a>
                                <a href="{{ route('catalog.category.products', ['id' => $category->id]) }}" role="button" class="btn btn-success btn-spinner btn-sm pull-right mr-1 m-b-0">
                                    <i class="fa fa-pluss"></i>&nbsp; Category Products
                                </a>
                            </div>
                            <div class="card-body">
                                <h3>{{ $category->name }} ({{ $category->id }})</h3>
                                <hr class="delmiter">
                                @include('themes.default.layouts.admin.message')

                                {{--  {!! Form::open(['route'=>'content.pages.create']) !!}  --}}
                                {{--  {!! Form::open(['route'=>'content.menu.create', 'files' => true, 'enctype' => "multipart/form-data"]) !!}  --}}
                                {!! Form::model($category,array('route'=>array('catalog.category.update',$category->id), 'files' => true, 'enctype' => "multipart/form-data")) !!}

                                @include('themes.default.admin.catalog.category.form', ['view' => 'edit'])

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
