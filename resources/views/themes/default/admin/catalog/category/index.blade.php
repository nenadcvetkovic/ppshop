@extends('themes.default.layouts.admin.index')

@section('main-content')
<div id="adminApp">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Category
                        {{--  <a href="{{ route('content.pages') }}" class="btn btn-success btn-sm">
                            <i class="fa fa-arrow-left"></i> Back to all pages
                        </a>  --}}
                        {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                            <i class="fa fa-file-excel-o"></i>&nbsp; Export
                        </a>  --}}
                        <a href="{{ route('catalog.category.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                            <i class="fa fa-plus"></i>&nbsp; New Item
                        </a>
                    </div>
                    <div class="card-body">
                        @include('themes.default.layouts.admin.message')

                        {{--  @if($allCategories && $allCategories->count() > 0)
                            <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Parent ID</th>
                                <th scope="col">Status</th>
                                <th scope="col">Created</th>
                                <th scope="col">Updated</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($allCategories as $cat)
                                <tr>
                                <td> {{ $cat->name }}</td>
                                <td> {{ $cat->parent_id }}</td>
                                <td class="text-center alert alert-{{ $cat->status ? 'success' : 'danger' }}"> {{ $cat->status ? 'ON' : 'OFF' }}</td>
                                <td> {{ $cat->created_at }}</td>
                                <td> {{ $cat->updated_at }}</td>
                                <td>
                                    <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Opcije
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ url('admin/catalog/category/edit', $cat->id) }}">Edit</a>
                                        <a class="dropdown-item" href="{{ url('admin/catalog/category/delete', $cat->id) }}">Delete</a>
                                    </div>
                                    </div>
                                </td>
                                </tr>
                            @endforeach

                            </tbody>
                            </table>
                        @else
                            <div class="col-12 alert-warning mt-2 p-2 text-center">
                            Trenunutno nema kategorija. Dodajte novu <a class="btn btn-success" href="{{ route('catalog.category.create') }}" role="button">+</a>
                            </div>
                        @endif  --}}

                        {{--  {{ dd($catList) }}  --}}

                        <pp-nested-2 :tip="'category'" :elements='{{ $catList }}' />

                        {{--  {{ dd($allCategories, $catList) }}  --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
