<section class="m-2 col-12" id="adminApp">

    <div class="form-group pull-right">
        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
    </div>

    <ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
      <li class="nav-item waves-effect waves-light">
        <a class="nav-link active" id="default-tab-md" data-toggle="tab" href="#default-md" role="tab" aria-controls="default-md" aria-selected="true">Default</a>
      </li>
      <li class="nav-item waves-effect waves-light">
        <a class="nav-link" id="design-tab-md" data-toggle="tab" href="#design-md" role="tab" aria-controls="design-md" aria-selected="false">Design</a>
      </li>
      <li class="nav-item waves-effect waves-light">
        <a class="nav-link" id="seo-tab-md" data-toggle="tab" href="#seo-md" role="tab" aria-controls="seo-md" aria-selected="false">SEO</a>
      </li>
    </ul>
    <div class="tab-content p-5" id="myTabContentMD">
      <div class="tab-pane fade show active" id="default-md" role="tabpanel" aria-labelledby="default-tab-md">
          <div class="col-12 mt-5 mt-5 ml-auto mr-auto">
              {!! Form::token() !!}
              <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                  <div class="col-4"> {!! Form::label('Ime kategorije') !!} </div>
                  <div class="col-8"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime kategorije']) !!} </div>
                  <span class="text-danger">{{ $errors->first('name') }}</span>
              </div>

              <div class="form-group row {{ $errors->has('show_page_title') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('Show title on category page') !!}
                </div>
                <div class="col-3">
                    {!! Form::select('show_page_title', ['1' => 'Enabled', '0' => 'Disabled'], @$category->show_page_title ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'show_page_title', 'id'=>'show_page_title']) !!}
                </div>
                <span class="text-danger">{{ @$errors->first('show_page_title') }}</span>
            </div>

            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('identifier') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('identifier') !!}
                </div>
                <div class="col-3">
                   {!! Form::text('identifier', old('identifier'), ['class'=>'form-control', 'name'=>'identifier', 'placeholder'=>'identifier']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('identifier') }}</span>
            </div>

            <hr class="delimiter">

              <div class="form-group row {{ $errors->has('parent_id') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('parent_id') !!}
                </div>
                <div class="col-3">
                    {!! Form::select('parent_id', (array)$selectCategories, 0, ['class'=>'form-control alert-success', 'name'=>'parent_id', 'id'=>'parent_id']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('parent_id') }}</span>
            </div>
            <hr class="delimiter">

              <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
                  <div class="col-4">
                      {!! Form::label('Status') !!}
                  </div>
                  <div class="col-3">
                      {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], @$category->status ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
                  </div>
                  <span class="text-danger">{{ $errors->first('status') }}</span>
              </div>

              <hr class="delimiter">

              <div class="form-group row {{ $errors->has('show_desc') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('Show description on category page') !!}
                </div>
                <div class="col-3">
                    {!! Form::select('show_desc', ['1' => 'Enabled', '0' => 'Disabled'], @$category->show_desc ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'show_desc', 'id'=>'show_desc']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('show_desc') }}</span>
            </div>

              <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
                  <div class="col-4">
                      {!! Form::label('Opis') !!}
                  </div>
                  <div class="col-8">
                      {!! Form::textarea('desc', old('desc'), ['class'=>'form-control', 'name'=>'desc', 'id'=>'desc', 'placeholder'=>'Opis kategorije', 'rows'=>'5']) !!}
                  </div>
                  <span class="text-danger">{{ $errors->first('desc') }}</span>
              </div>
          </div>
      </div>
      <div class="tab-pane fade" id="design-md" role="tabpanel" aria-labelledby="design-tab-md">


        @if($view == 'edit')
            <div>
                <pp-cat-form-image-box :images="{{ $category->categoryImages }}" :type="'category'" :id="{{ $category->id }}"/>
            </div>
        @endif

        <div class="input-group hdtuto control-group lst increment" >
            <div class="col-12 first-image">
                {!! Form::file('images[]', ['type'=>'files', 'accept' => 'image/*', 'onchange' => 'preview_image(event)']) !!}
                <img id="output_image" class="output_image" height="50" />
            </div>
        </div>

        <div class="form-group row {{ $errors->has('show_cat_image') ? 'has-error' : '' }}">
            <div class="col-4">
                {!! Form::label('Show category image') !!}
            </div>
            <div class="col-3">
                {!! Form::select('show_cat_image', ['1' => 'Enabled', '0' => 'Disabled'], @$category->show_cat_image ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'show_cat_image', 'id'=>'show_cat_image']) !!}
            </div>
            <span class="text-danger">{{ $errors->first('show_cat_image') }}</span>
        </div>

        <hr class="delimiter">
          {{-- <ul>
              <li class="nav-item level0">
                  <a class="nav-link btn-lg btn-block btn-outline-primary" data-toggle="collapse" href="#categoryDesign">
                  <p>Catalog</p>
                  </a>
                  <div class="collapse card m-1" id="categoryDesign">
                      <ul class="nav">
                          <li class="nav-item ">
                              <a class="nav-link" href="{{ route('catalog', 'category') }}">
                                  <span class="sidebar-mini">CA</span>
                                  <span class="sidebar-normal">Category</span>
                              </a>
                          </li>
                          <li class="nav-item ">
                              <a class="nav-link" href="{{ route('catalog', 'product') }}">
                                  <span class="sidebar-mini">PR</span>
                                  <span class="sidebar-normal">Products</span>
                              </a>
                          </li>
                      </ul>
                  </div>
              </li>
          </ul> --}}
      </div>

      {{--  SEO  --}}
      <div class="tab-pane fade" id="seo-md" role="tabpanel" aria-labelledby="seo-tab-md">
        <div class="form-group row {{ $errors->has('seo_title') ? 'has-error' : '' }}">
            <div class="col-4">
                {!! Form::label('SEO Title') !!}
            </div>
            <div class="col-8">
               {!! Form::text('seo_title', old('seo_title'), ['class'=>'form-control', 'name'=>'seo_title', 'placeholder'=>'SEO Title']) !!}
            </div>
            <span class="text-danger">{{ $errors->first('seo_title') }}</span>
        </div>

        <div class="form-group row {{ $errors->has('seo_desc') ? 'has-error' : '' }}">
            <div class="col-4">
                {!! Form::label('SEO Descripton') !!}
            </div>
            <div class="col-12">
                {!! Form::textarea('seo_desc', old('seo_desc'), ['class'=>'form-control', 'name'=>'seo_desc', 'id'=>'seo_desc', 'placeholder'=>'SEO description', 'rows'=>'5']) !!}
            </div>
            <span class="text-danger">{{ $errors->first('seo_desc') }}</span>
        </div>

        <div class="form-group row {{ $errors->has('seo_keywords') ? 'has-error' : '' }}">
          <div class="col-4">
              {!! Form::label('SEO Keywords') !!}
          </div>
          <div class="col-12">
              {!! Form::textarea('seo_keywords', old('seo_keywords'), ['class'=>'form-control', 'name'=>'seo_keywords', 'id'=>'seo_keywords', 'placeholder'=>'Seo Keywords', 'rows'=>'5']) !!}
          </div>
          <span class="text-danger">{{ $errors->first('seo_keywords') }}</span>
      </div>

        <hr class="delimiter">
      </div>
    </div>

  </section>

  <script type="text/javascript">
    jQuery(document).ready(function() {
      jQuery(".btn-success").click(function(){
          var lsthmtl = jQuery(".clone").html();
          jQuery(".increment").after(lsthmtl);
      });
      jQuery("body").on("click",".btn-danger",function(){
          jQuery(this).parents(".hdtuto.control-group.lst").remove();
          console.log(jQuery(this).parents(".hdtuto.control-group.lst"));
      });
    });

    function preview_image(event){
     var reader = new FileReader();
     reader.onload = function()
     {
      //var output = document.getElementById('output_image');
      //console.log(event.target.parentNode.querySelector('.output_image'));
      var output = event.target.parentNode.querySelector('.output_image');
      output.src = reader.result;
     }
     reader.readAsDataURL(event.target.files[0]);
    }

</script>
