@extends('themes.default.layouts.admin.index')
{{--{{ dd($cat_prod) }}--}}
@section('main-content')
<div id="adminApp">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Category <h4>{{ $cat->name }}</h4>
                         <a href="{{ url('admin/catalog/category') }}" class="btn btn-success btn-sm">
                            <i class="fa fa-arrow-left"></i> Back to all category
                        </a>
                        {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                            <i classcd .="fa fa-file-excel-o"></i>&nbsp; Export
                        </a>  --}}
                        <a href="{{ route('catalog.category.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                            <i class="fa fa-plus"></i>&nbsp; New Category Item
                        </a>
                        <a href="{{ route('catalog.category.edit', $cat->id) }}" role="button" class="btn btn-danger btn-spinner btn-sm pull-right mr-1 m-b-0">
                                                <i class="fa fa-pluss"></i>&nbsp; Category edit
                        </a>
                    </div>
                    <div class="card-body">
                        @include('themes.default.layouts.admin.message')
                        <pp-nested-products :tip="'category.products'" :elements='{{ $cat_prod }}' />

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
