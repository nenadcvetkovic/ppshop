@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 p-0">
                    <div class="col-12 alert-danger p-3">
                        hello from here {{  __DIR__ }}
                    </div>
                    <div class="alert alert-success">
                        product attributes index
                    </div>
                    <div class="col-12">
                        <table class="table">
                            <thead class="thead-dark">
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">First</th>
                                <th scope="col">Last</th>
                                <th scope="col">Handle</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                              </tr>
                              <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                              </tr>
                              <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                              </tr>
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
