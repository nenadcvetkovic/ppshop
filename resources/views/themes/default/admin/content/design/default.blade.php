<div class="form-group row {{ $errors->has('content/design/configuration/body/color') ? 'has-error' : '' }}">
    <div class="col-5 text-right"> {!! Form::label('Body color') !!} </div>
    <div class="col-6">
        {!! Form::text(
            'content/design/configuration/body/color',
            old('content/design/configuration/body/color',
            $settings_configuration->getConfigValue('content/design/configuration/body/color')),
            [
                'class'=>'form-control colorpicker',
                'id' => 'colorpicker1',
                'name'=>'content/design/configuration/body/color',
                'placeholder'=>'Unesite boju pozadine sajta',
                'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/body/color') : '')
            ]
        ) !!}
        <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#fff (Bela)"</span>
    </div>
    <span class="text-danger">{{ $errors->first('content/design/configuration/body/color') }}</span>
</div>
