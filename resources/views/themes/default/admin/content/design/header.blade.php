<div class="form-group row {{ $errors->has('content/design/configuration/header/color') ? 'has-error' : '' }}">
    <div class="col-5 text-right"> {!! Form::label('Header color') !!} </div>
    <div class="col-6">
        {!! Form::text(
            'content/design/configuration/header/color',
            old('content/design/configuration/header/color',
            $settings_configuration->getConfigValue('content/design/configuration/header/color')),
            [
                'class'=>'form-control colorpicker',
                'id' => 'colorpicker1',
                'name'=>'content/design/configuration/header/color',
                'placeholder'=>'Unesite boju headera',
                'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/header/color') : '')
            ]
        ) !!}
        <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#fff (Bela)"</span>
    </div>
    <span class="text-danger">{{ $errors->first('content/design/configuration/header/color') }}</span>
</div>
