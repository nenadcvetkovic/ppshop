<div class="form-group row {{ $errors->has('content/design/configuration/links/active') ? 'has-error' : '' }}">
    <div class="col-5 text-right"> {!! Form::label('Boja linkova') !!} </div>
    <div class="col-6">
        {!! Form::text(
            'content/design/configuration/links/active',
            old('content/design/configuration/links/active',
            $settings_configuration->getConfigValue('content/design/configuration/links/active')),
            [
                'class'=>'form-control colorpicker',
                'id' => 'colorpicker1',
                'name'=>'content/design/configuration/links/active',
                'placeholder'=>'Unesite boju linkova',
                'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/links/active') : '')
            ]
        ) !!}
        <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#3D4095"</span>
    </div>
    <span class="text-danger">{{ $errors->first('content/design/configuration/links/active') }}</span>
</div>

<div class="form-group row {{ $errors->has('content/design/configuration/links/hover') ? 'has-error' : '' }}">
    <div class="col-5 text-right"> {!! Form::label('Boja linkova - hover') !!} </div>
    <div class="col-6">
        {!! Form::text(
            'content/design/configuration/links/hover',
            old('content/design/configuration/links/hover',
            $settings_configuration->getConfigValue('content/design/configuration/links/hover')),
            [
                'class'=>'form-control colorpicker',
                'id' => 'colorpicker1',
                'name'=>'content/design/configuration/links/hover',
                'placeholder'=>'Unesite boju linkova',
                'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/links/hover') : '')
            ]
        ) !!}
        <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#3D4095"</span>
    </div>
    <span class="text-danger">{{ $errors->first('content/design/configuration/links/hover') }}</span>
</div>
