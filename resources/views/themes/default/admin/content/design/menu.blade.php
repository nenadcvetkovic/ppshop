<div class="form-group row {{ $errors->has('content/design/configuration/menu/links/active') ? 'has-error' : '' }}">
    <div class="col-5 text-right"> {!! Form::label('Boja linkova') !!} </div>
    <div class="col-6">
        {!! Form::text(
            'content/design/configuration/menu/links/active',
            old('content/design/configuration/menu/links/active',
            $settings_configuration->getConfigValue('content/design/configuration/menu/links/active')),
            [
                'class'=>'form-control colorpicker',
                'id' => 'colorpicker1',
                'name'=>'content/design/configuration/menu/links/active',
                'placeholder'=>'Unesite boju linkova',
                'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/menu/links/active') : '')
            ]
        ) !!}
        <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#3D4095"</span>
    </div>
    <span class="text-danger">{{ $errors->first('content/design/configuration/menu/links/active') }}</span>
</div>

<div class="form-group row {{ $errors->has('content/design/configuration/menu/links/hover') ? 'has-error' : '' }}">
    <div class="col-5 text-right"> {!! Form::label('Boja linkova - hover') !!} </div>
    <div class="col-6">
        {!! Form::text(
            'content/design/configuration/menu/links/hover',
            old('content/design/configuration/menu/links/hover',
            $settings_configuration->getConfigValue('content/design/configuration/menu/links/hover')),
            [
                'class'=>'form-control colorpicker',
                'id' => 'colorpicker1',
                'name'=>'content/design/configuration/menu/links/hover',
                'placeholder'=>'Unesite boju linkova',
                'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/menu/links/hover') : '')
            ]
        ) !!}
        <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#3D4095"</span>
    </div>
    <span class="text-danger">{{ $errors->first('content/design/configuration/menu/links/hover') }}</span>
</div>

<div class="form-group row {{ $errors->has('content/design/configuration/menu/links/bg') ? 'has-error' : '' }}">
    <div class="col-5 text-right"> {!! Form::label('Boja pozadine linkova') !!} </div>
    <div class="col-6">
        {!! Form::text(
            'content/design/configuration/menu/links/bg',
            old('content/design/configuration/menu/links/bg',
            $settings_configuration->getConfigValue('content/design/configuration/menu/links/bg')),
            [
                'class'=>'form-control colorpicker',
                'id' => 'colorpicker1',
                'name'=>'content/design/configuration/menu/links/bg',
                'placeholder'=>'Unesite boju linkova',
                'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/menu/links/bg') : '')
            ]
        ) !!}
        <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#3D4095"</span>
    </div>
    <span class="text-danger">{{ $errors->first('content/design/configuration/menu/links/bg') }}</span>
</div>

<div class="form-group row {{ $errors->has('content/design/configuration/menu/links/bghover') ? 'has-error' : '' }}">
    <div class="col-5 text-right"> {!! Form::label('Boja pozadine linkova - hover') !!} </div>
    <div class="col-6">
        {!! Form::text(
            'content/design/configuration/menu/links/bghover',
            old('content/design/configuration/menu/links/bghover',
            $settings_configuration->getConfigValue('content/design/configuration/menu/links/bghover')),
            [
                'class'=>'form-control colorpicker',
                'id' => 'colorpicker1',
                'name'=>'content/design/configuration/menu/links/bghover',
                'placeholder'=>'Unesite boju linkova',
                'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/menu/links/bghover') : '')
            ]
        ) !!}
        <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#3D4095"</span>
    </div>
    <span class="text-danger">{{ $errors->first('content/design/configuration/menu/links/bghover') }}</span>
</div>
