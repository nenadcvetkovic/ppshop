{{--  @foreach(Storage::directories('public/themes') as $theme)
    <li>{{ $theme }}</li>
@endforeach  --}}

{{--  $files = File::files(public_path());  --}}

{{--  // If you would like to retrieve a list of
// all files within a given directory including all sub-directories  --}}
{{--  $files = File::allFiles(public_path());  --}}


@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-file"></i>
                    Design configuration
                    {{--  <a href="{{ route('content.block.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                        <i class="fa fa-plus"></i>&nbsp; New CMS Block
                    </a>  --}}
                </div>
                <div class="card-body">
                    @include('themes.default.layouts.admin.message')

                     {{-- {!! Form::open(['route'=>'content.pages.create']) !!} --}}
                    {!! Form::open(['route'=>'content.design.configuration', 'files' => true, 'enctype' => "multipart/form-data"]) !!}
                    {{--  {!! Form::model($menu,array('route'=>array('content.menu.update',$menu->id))) !!}  --}}

                    <div class="form-group pull-right">
                        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
                        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
                    </div>
                    <div class="clearfix"></div>

                    <section class="m-2 col-12">
                        <ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link active" id="default-tab-md" data-toggle="tab" href="#default-md" role="tab" aria-controls="default-md" aria-selected="true">Default</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="header-tab-md" data-toggle="tab" href="#header-md" role="tab" aria-controls="header-md" aria-selected="false">Header</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="links-tab-md" data-toggle="tab" href="#links-md" role="tab" aria-controls="links-md" aria-selected="false">links</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="menu-tab-md" data-toggle="tab" href="#menu-md" role="tab" aria-controls="menu-md" aria-selected="false">Menu</a>
                            </li>
                        </ul>

                        <div class="tab-content p-5" id="myTabContentMD">
                            {{-- Default Configuration --}}
                            <div class="tab-pane fade show active" id="default-md" role="tabpanel" aria-labelledby="default-tab-md">
                                <div class="col-12 mt-5 mt-5 ml-auto mr-auto">
                                    {!! Form::token() !!}
                                    @include('themes.default.admin.content.design.default')
                                </div>
                            </div>

                            {{-- Header Configuration --}}
                            <div class="tab-pane fade" id="header-md" role="tabpanel" aria-labelledby="shop-tab-md">
                            @include('themes.default.admin.content.design.header')
                            </div>

                            {{-- Links Configuration --}}
                            <div class="tab-pane fade" id="links-md" role="tabpanel" aria-labelledby="links-tab-md">
                                @include('themes.default.admin.content.design.links')
                            </div>

                            {{-- Menu Configuration --}}
                            <div class="tab-pane fade" id="menu-md" role="tabpanel" aria-labelledby="menu-tab-md">
                                @include('themes.default.admin.content.design.menu')
                            </div>
                        </div>
                    </section>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
