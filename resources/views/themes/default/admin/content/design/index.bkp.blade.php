{{--  @foreach(Storage::directories('public/themes') as $theme)
    <li>{{ $theme }}</li>
@endforeach  --}}

{{--  $files = File::files(public_path());  --}}

{{--  // If you would like to retrieve a list of
// all files within a given directory including all sub-directories  --}}
{{--  $files = File::allFiles(public_path());  --}}


@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-file"></i>
                    Design configuration
                    {{--  <a href="{{ route('content.block.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                        <i class="fa fa-plus"></i>&nbsp; New CMS Block
                    </a>  --}}
                </div>
                <div class="card-body">
                    @include('themes.default.layouts.admin.message')

                     {{-- {!! Form::open(['route'=>'content.pages.create']) !!} --}}
                    {!! Form::open(['route'=>'content.design.configuration', 'files' => true, 'enctype' => "multipart/form-data"]) !!}
                    {{--  {!! Form::model($menu,array('route'=>array('content.menu.update',$menu->id))) !!}  --}}

                    <div class="form-group pull-right">
                        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
                        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
                    </div>
                    <div class="clearfix"></div>

                    <div class="accordion d-none" id="accordionExample">
                        <div class="cardd">
                          <div class="card-headerd" id="headingOne">
                            <h2 class="mb-0">
                              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#designHeader" aria-expanded="true" aria-controls="designHeader">
                                <h5>Header</h5>
                              </button>
                            </h2>
                          </div>

                          <div id="designHeader" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                              test 1
                            </div>
                          </div>
                        </div>
                        <div class="cardd">
                          <div class="card-headerh" id="headingTwo">
                            <h2 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#designContent" aria-expanded="false" aria-controls="designContent">
                                <h5>Content</h5>
                              </button>
                            </h2>
                          </div>
                          <div id="designContent" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                              Test 2
                            </div>
                          </div>
                        </div>
                        <div class="cardd">
                          <div class="card-headerd" id="headingThree">
                            <h2 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#designFooter" aria-expanded="false" aria-controls="designFooter">
                                <h5>Footer</h5>
                              </button>
                            </h2>
                          </div>
                          <div id="designFooter" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                Test 3
                            </div>
                          </div>
                        </div>
                    </div>

                    <!--Accordion wrapper-->
                    <div class="accordion md-accordion accordion-blocks" id="accordionDesign" role="tablist" aria-multiselectable="true">

                      <!-- Accordion card -->
                      <div class="card">

                        <!-- Card header -->
                        <div class="card-header" role="tab" id="heading">

                          <!-- Heading -->
                          <a data-toggle="collapse" data-parent="#accordionDesign" href="#collapseHeader" aria-expanded="true" aria-controls="collapseHeader">
                            <h5 class="mt-1 mb-0">
                              <span>Header</span>
                              <i class="fas fa-angle-down rotate-icon"></i>
                            </h5>
                          </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseHeader" class="collapse show" role="tabpanel" aria-labelledby="heading" data-parent="#accordionDesign">
                          <div class="card-body">
                            {{-- <input id="colorpicker" name="header-color" type="text" class="form-control" value="#fff" /> --}}
                          </div>


                          <div class="form-group row {{ $errors->has('content/design/configuration/header/color') ? 'has-error' : '' }}">
                            <div class="col-5 text-right"> {!! Form::label('Header color') !!} </div>
                            <div class="col-6">
                                {!! Form::text(
                                    'content/design/configuration/header/color',
                                    old('content/design/configuration/header/color',
                                    $settings_configuration->getConfigValue('content/design/configuration/header/color')),
                                    [
                                        'class'=>'form-control colorpicker',
                                        'id' => 'colorpicker1',
                                        'name'=>'content/design/configuration/header/color',
                                        'placeholder'=>'Unesite boju headera',
                                        'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/header/color') : '')
                                    ]
                                ) !!}
                                <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#fff (Bela)"</span>
                            </div>
                            <span class="text-danger">{{ $errors->first('content/design/configuration/header/color') }}</span>
                        </div>
                        </div>
                      </div>
                      <!-- Accordion card -->

                      <!-- Accordion card -->
                      <div class="card">

                        <!-- Card header -->
                        <div class="card-header" role="tab" id="heading">

                          <!-- Heading -->
                          <a data-toggle="collapse" data-parent="#accordionDesign" href="#collapseContent" aria-expanded="true" aria-controls="collapseContent">
                            <h5 class="mt-1 mb-0">
                              <span>Content</span>
                              <i class="fas fa-angle-down rotate-icon"></i>
                            </h5>
                          </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseContent" class="collapse" role="tabpanel" aria-labelledby="heading"
                          data-parent="#accordionDesign">
                          <div class="card-body">
                            <div class="form-group row {{ $errors->has('content/design/configuration/header/color') ? 'has-error' : '' }}">
                                <div class="col-5 text-right"> {!! Form::label('Header color') !!} </div>
                                <div class="col-6">
                                    {!! Form::text(
                                        'content/design/configuration/header/color',
                                        old('content/design/configuration/header/color',
                                        $settings_configuration->getConfigValue('content/design/configuration/header/color')),
                                        [
                                            'class'=>'form-control colorpicker',
                                            'id' => 'colorpicker2',
                                            'name'=>'content/design/configuration/header/color',
                                            'placeholder'=>'Unesite boju headera',
                                            'style' => ($settings_configuration->getHeaderColor() ? 'background: '. $settings_configuration->getConfigValue('content/design/configuration/header/color') : '')
                                        ]
                                    ) !!}
                                    <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "#fff (Bela)"</span>
                                </div>
                                <span class="text-danger">{{ $errors->first('content/design/configuration/header/color') }}</span>
                            </div>

                          </div>
                        </div>
                      </div>
                      <!-- Accordion card -->

                      <!-- Accordion card -->
                      <div class="card">

                        <!-- Card header -->
                        <div class="card-header" role="tab" id="heading">

                          <!-- Heading -->
                          <a data-toggle="collapse" data-parent="#accordionDesign" href="#collapseFooter" aria-expanded="true" aria-controls="collapseFooter">
                            <h5 class="mt-1 mb-0">
                              <span>Footer</span>
                              <i class="fas fa-angle-down rotate-icon"></i>
                            </h5>
                          </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapseFooter" class="collapse" role="tabpanel" aria-labelledby="heading"
                          data-parent="#accordionDesign">
                          <div class="card-body">


                          </div>
                        </div>
                      </div>
                      <!-- Accordion card -->
                    </div>
                    <!--/.Accordion wrapper-->

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
