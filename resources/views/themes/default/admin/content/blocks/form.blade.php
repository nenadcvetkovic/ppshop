<div id="adminApp" class="col-12 ml-auto mr-auto">
    <div class="row">
        {!! Form::token() !!}
        <div class="col-lg-9 col-sm-12 col-md-12">
            <div class="form-group row {{ $errors->has('content') ? 'has-error' : '' }}">
                {{--  <div class="col-12">
                    {!! Form::label('Block content') !!}
                </div>  --}}

                    @if (@$block)
                        <pp-edit-content :data="{{ $block }}" :cols="10" :rows="50" />
                    @else
                        <pp-edit-content :cols="10" :rows="50" />
                    @endif

                <hr class="delimiter">

                <div class="col-12">
                    {{--  {!! Form::textarea('content', old('content'), ['class'=>'form-control', 'name'=>'content', 'id'=>'content', 'placeholder'=>'Page content', 'rows'=>'20']) !!}  --}}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'content'])
            </div>
            <hr class="delimiter">
        </div>
        <div class="col-lg-3 col-sm-12 col-md-12">
            <div class="form-group pull-right">
                <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
                <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
            </div>
            <div class="clearfix"></div>
            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                <div class="col-12"> {!! Form::label('Ime blocka') !!} </div>
                <div class="col-12"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime bloka']) !!} </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'name'])
            </div>

            <div class="form-group row {{ $errors->has('identifier') ? 'has-error' : '' }}">
                <div class="col-12"> {!! Form::label('identifier') !!} </div>
                <div class="col-12">
                    {!! Form::text('identifier', old('identifier'), ['class'=>'form-control ', ($page == 'edit' ? 'readonly' : ''), 'identifier'=>'identifier', 'placeholder'=>'Unesite identifier']) !!}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'identifier'])
            </div>
            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Status') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], @$block->status ? @$block->status : 0, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'status'])
            </div>

            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Opis') !!}
                </div>
                <div class="col-12">
                    {!! Form::textarea('desc', old('desc'), ['class'=>'form-control', 'name'=>'desc', 'id'=>'desc', 'placeholder'=>'Kratki opis', 'rows'=>'3']) !!}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'desc'])
            </div>

            <hr class="delimiter">
        </div>
    </div>
</div>
