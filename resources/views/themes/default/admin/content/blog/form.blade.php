<div id="adminApp" class="col-12 ml-auto mr-auto">
    {!! Form::token() !!}

    <div class="row">
        <div class="col-12">
            <div class="form-group pull-right">
                <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
                <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-9">
            <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                {{--  <div class="col-6"> {!! Form::label('Naslov Posta') !!} </div>  --}}
                <div class="col-12"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime stranice']) !!} </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'name'])
            </div>

            <div class="form-group row {{ $errors->has('content') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Content') !!}
                </div>
                <hr class="delimiter">

                <div class="container-fluid">
                    @if (@$post)
                        <pp-edit-content :name="'content'" :data="{{ $post }}" :cols="30" :rows="30" />
                    @else
                        <pp-edit-content
                        :name="'content'"
                        @if (old('content'))
                        :data="{{ ['content' => old('content')] }}"
                        @endif
                        :cols="10" :rows="50" />
                    @endif
                </div>

                <hr class="delimiter">

                @include('themes.default.layouts.admin.form.message', ['errName' => 'content'])
            </div>

            <div class="form-group row {{ $errors->has('short_content') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Short Content') !!}
                </div>
                <div class="form-group container-fluid">
                    <textarea name="short_content" class="form-control" cols="5" rows="5">{{ old('short_content', @$post->short_content) }}</textarea>
                </div>

                @include('themes.default.layouts.admin.form.message', ['errName' => 'short_content'])
            </div>

            <hr class="delimiter">
            <h3>SEO Configuration</h3>
            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('seo_title') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('SEO Title') !!}
                </div>
                <div class="col-8">
                   {!! Form::text('seo_title', old('seo_title'), ['class'=>'form-control', 'name'=>'seo_title', 'placeholder'=>'SEO Title']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('seo_title') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('seo_desc') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('SEO Descripton') !!}
                </div>
                <div class="col-12">
                    {!! Form::textarea('seo_desc', old('seo_desc'), ['class'=>'form-control', 'name'=>'seo_desc', 'id'=>'seo_desc', 'placeholder'=>'SEO description', 'rows'=>'5']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('seo_desc') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('seo_keywords') ? 'has-error' : '' }}">
              <div class="col-4">
                  {!! Form::label('SEO Keywords') !!}
              </div>
              <div class="col-12">
                  {!! Form::textarea('seo_keywords', old('seo_keywords'), ['class'=>'form-control', 'name'=>'seo_keywords', 'id'=>'seo_keywords', 'placeholder'=>'Seo Keywords', 'rows'=>'5']) !!}
              </div>
              <span class="text-danger">{{ $errors->first('seo_keywords') }}</span>
          </div>

        </div>

        <div class="col-3">
            @if ($view == 'edit')
            <div class="card">
                <div class="card-header ">
                    <p class="p-0 m-0">Created by: {{ $post->getAuthorData()->name }}</p>
                </div>
                <div class="card-body">
                    <p>Created: {{ $post->getPostCreated('d/m/Y') }}</p>
                    <p>Updated: {{ $post->getPostUpdated('d/m/Y') }}</p>
                </div>
            </div>

            <hr class="delimiter">
            @endif

            <div class="form-group row {{ $errors->has('identifier') ? 'has-error' : '' }}">
                <div class="col-12"> {!! Form::label('identifier') !!} </div>
                <div class="col-12">
                    {!! Form::text('identifier', old('identifier'), ['class'=>'form-control ', /* ($view == 'edit' ? 'readonly' : ''), */ 'identifier'=>'identifier', 'placeholder'=>'Unesite identifier']) !!}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'identifier'])
            </div>

            <hr class="delimiter">
            <div class="col-12"> {!! Form::label('Naslovna slika posta') !!} </div>
            @if($view == 'edit' && isset($post->image_path))
                <img src="{{ asset('storage/'.$post->image_path) }}" width="200"/>
            @endif

            {{--  <div class="input-group hdtuto control-group lst increment" >
                <div class="col-12 first-image">
                    {!! Form::file('images[]', ['type'=>'files', 'accept' => 'image/*', 'onchange' => 'preview_image(event)']) !!}
                    <img id="output_image" class="output_image" height="50" />
                </div>
                <small class="text-muted">Image size 840x450</small>
            </div>  --}}
            <div>
                <pp-import-form-image-box :type="'post'" :label="''"/>
                <small class="text-muted">Image size 840x450</small>
            </div>
            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Status') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], @$post->status ? @$post->status : 0, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'status'])
            </div>

            <div class="form-group row {{ $errors->has('show_post_title') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Show post title') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('show_post_title', ['1' => 'Enabled', '0' => 'Disabled'], @$post->show_post_title ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'show_post_title', 'id'=>'show_post_title']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('show_post_title') }}</span>
            </div>

            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('show_post_image') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Show post top image') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('show_top_post_image', ['1' => 'Enabled', '0' => 'Disabled'], @$post->show_top_post_image ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'show_top_post_image', 'id'=>'show_top_post_image']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('show_post_image') }}</span>
            </div>


            <div class="col-12">
                {!! Form::label('Top slika posta') !!}
            </div>
            @if($view == 'edit' && isset($post->top_image_path))
                <img src="{{ asset('storage/'.$post->top_image_path) }}" width="200"/>
            @endif

            <div>
                <pp-import-form-image-box :type="'post'" :name="'top_image'" :label="''"/>
                <small class="text-muted">Image size 840x450</small>
            </div>

            <hr class="delimiter">

        </div>
    </div>

    <hr class="delimiter" />
</div>

{{--  <script type="text/javascript">
function preview_image(event){
    var reader = new FileReader();
    reader.onload = function(){
        //var output = document.getElementById('output_image');
        //console.log(event.target.parentNode.querySelector('.output_image'));
        var output = event.target.parentNode.querySelector('.output_image');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}

</script>  --}}
