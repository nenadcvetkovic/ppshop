<div class="col-12 ml-auto mr-auto">
    <div class="form-group pull-right">
        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
    </div>
    <div class="clearfix"></div>
    <hr class="delimiter">

    {!! Form::token() !!}

    <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
        <div class="col-4 text-right"> {!! Form::label('Ime brenda') !!} </div>
        <div class="col-8"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime brenda']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'name'])
    </div>

    <div class="form-group row {{ $errors->has('identifier') ? 'has-error' : '' }}">
        <div class="col-4 text-right"> {!! Form::label('identifier') !!} </div>
        <div class="col-8">
            {!! Form::text('identifier', old('identifier'), ['class'=>'form-control ', ($view == 'edit' ? 'readonly' : ''), 'identifier'=>'identifier', 'placeholder'=>'Unesite identifier']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'identifier'])
    </div>
    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Status') !!}
        </div>
        <div class="col-8">
            {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], $view == 'create' ? 0 : $brands->status, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'status'])
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('show_in_slider') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show in slider') !!}
        </div>
        <div class="col-8">
            {!! Form::select('show_in_slider', ['1' => 'Enabled', '0' => 'Disabled'], $view == 'create' ? 0 : $brands->show_in_slider, ['class'=>'form-control alert-success', 'name'=>'show_in_slider', 'id'=>'show_in_slider']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'show_in_slider'])
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Opis') !!}
        </div>
        <div class="col-8">
            {!! Form::textarea('desc', old('desc'), ['class'=>'form-control', 'name'=>'desc', 'id'=>'desc', 'placeholder'=>'Kratki opis', 'rows'=>'3']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'desc'])
    </div>

    <hr class="delimiter" />

    <div>
        <pp-import-form-image-box
            @if ($view == 'edit')
                :data="{{ $brands }}"
                :old_image="'/storage/{{ $brands->image_path }}'"
            @endif
            :image_width="'400px'"
            :type="'brand'"
            :label="'Izaberite logo brenda'"
        />
    </div>
</div>
