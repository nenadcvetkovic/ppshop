@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid" id="adminApp">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>
                    Edit Brands
                    <a href="{{ route('content.brands.index') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-arrow-left"></i> Back to all brands
                    </a>
                    {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                        <i class="fa fa-file-excel-o"></i>&nbsp; Export
                    </a>  --}}
                    <a href="{{ route('content.brands.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                        <i class="fa fa-plus"></i>&nbsp; New Brand
                    </a>
                </div>
                <div class="card-body">
                    {{--  {{ dd($brands) }}  --}}
                    @include('themes.default.layouts.admin.message')

                    {{--  {!! Form::open(['route'=>'content.block.create']) !!}  --}}
                    {{--  {!! Form::open(['route'=>'content.brands.create', 'files' => true, 'enctype' => "multipart/form-data"]) !!}  --}}
                    {!! Form::model($brands,['route'=>array('content.brands.update',$brands->id),'files' => true, 'enctype' => "multipart/form-data"]) !!}

                    @include('themes.default.admin.content.brands.form', ['view' => 'edit'])

                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
