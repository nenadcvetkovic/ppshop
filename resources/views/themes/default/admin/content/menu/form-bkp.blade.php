<div class="col-12 mt-5 mt-5 ml-auto mr-auto">
    {!! Form::token() !!}

    <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('Ime proizvoda') !!} </div>
        <div class="col-10"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime kategorije']) !!} </div>
        <span class="text-danger">{{ $errors->first('name') }}</span>
    </div>

    <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Status') !!}
        </div>
        <div class="col-3">
            {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], 0, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('status') }}</span>
    </div>

    <hr class="delimiter">

    <div class="input-group hdtuto control-group lst increment" >
        <div class="col-12 first-image">
            {!! Form::file('images[]', ['type'=>'files', 'accept' => 'image/*', 'onchange' => 'preview_image(event)']) !!}
            <img id="output_image" class="output_image" height="50" />
        </div>
        <hr class="delimiter">
        <div class="input-group-btn">
          <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
        </div>
      </div>
      <div class="clone hide d-none">
        <div class="hdtuto control-group lst input-group" style="margin-top:10px">
            {!! Form::file('images[]', ['type'=>'files', 'accept' => 'image/*', 'onchange' => 'preview_image(event)']) !!}
            <img id="output_image" class="output_image" height="50" />

          <div class="input-group-btn">
            <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
          </div>
        </div>
      </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Opis') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('desc', old('desc'), ['class'=>'form-control', 'name'=>'desc', 'id'=>'desc', 'placeholder'=>'Opis prozvoda', 'rows'=>'3']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('desc') }}</span>
    </div>


    <div class="form-group row {{ $errors->has('short_desc') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Kratki Opis') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('short_desc', old('short_desc'), ['class'=>'form-control', 'name'=>'short_desc', 'id'=>'short_desc', 'placeholder'=>'Kratki Opis proizvoda', 'rows'=>'3']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('short_desc') }}</span>
    </div>

    <hr class="delimiter" />

    <div class="form-group row">
        <div class="col-2">
            {!! Form::label('Izaberite kategoriju') !!}
        </div>
        <div class="col-10">
            <select multiple class="form-control" name="category_list[]" id="exampleFormControlSelect2">
                @foreach ($categories as $cat)
                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                @endforeach
              </select>
              <small>Hold CTRL + click</small>
        </div>
      </div>

    <hr class="delimiter" />

    <div class="form-group row {{ $errors->has('price') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('Cena') !!} </div>
        <div class="col-10"> {!! Form::number('price', old('price'), ['class'=>'form-control', 'price'=>'price']) !!} </div>
        <span class="text-danger">{{ $errors->first('price') }}</span>
    </div>

    <div class="form-group row {{ $errors->has('special_price') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('Specijalna Cena') !!} </div>
        <div class="col-10"> {!! Form::number('special_price', old('special_price'), ['class'=>'form-control', 'special_price'=>'special_price']) !!} </div>
        <span class="text-danger">{{ $errors->first('special_price') }}</span>
    </div>

    <hr class="delimiter" />

    <div class="form-group">
        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
      jQuery(".btn-success").click(function(){
          var lsthmtl = jQuery(".clone").html();
          jQuery(".increment").after(lsthmtl);
      });
      jQuery("body").on("click",".btn-danger",function(){
          jQuery(this).parents(".hdtuto.control-group.lst").remove();
          console.log(jQuery(this).parents(".hdtuto.control-group.lst"));
      });
    });

    function preview_image(event){
     var reader = new FileReader();
     reader.onload = function()
     {
      //var output = document.getElementById('output_image');
      //console.log(event.target.parentNode.querySelector('.output_image'));
      var output = event.target.parentNode.querySelector('.output_image');
      output.src = reader.result;
     }
     reader.readAsDataURL(event.target.files[0]);
    }

</script>
