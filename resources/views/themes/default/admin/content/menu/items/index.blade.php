@extends('themes.default.layouts.admin.index')

@section('main-content')
<div id="adminApp">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Items
                        <a href="{{ route('content.menu') }}" class="btn btn-success btn-sm">
                            <i class="fa fa-arrow-left"></i> Back to all menu
                        </a>
                        {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                            <i class="fa fa-file-excel-o"></i>&nbsp; Export
                        </a>  --}}
                        <a href="{{ route('content.menu.items.add', ['id' => $id]) }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                            <i class="fa fa-plus"></i>&nbsp; New Item
                        </a>
                    </div>
                    <div class="card-body">

                        @include('themes.default.layouts.admin.message')

                        <pp-nested-2 :elements='{{ $menuItems }}' :tip="'menu'" :action="'menu/items/updateOrder'"/>

                        {{-- <form>
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-7 col-xl-5 form-group">
                                    <div class="input-group">
                                        <input placeholder="Search" class="form-control">
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-primary">
                                                <i class="fa fa-search"></i>&nbsp; Search
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-auto form-group ">
                                    <select class="form-control">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </form> --}}
                        @if ($menuItems->count())
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th><a><span class="fa fa-sort-amount-asc"></span> ID</a></th>
                                        <th><a><span class="fa"></span> Title</a></th>
                                        <th><a><span class="fa"></span> Parent ID</a></th>
                                        <th><a><span class="fa"></span> Type</a></th>
                                        <th><a><span class="fa"></span> URL</a></th>
                                        <th><a><span class="fa"></span> Status</a></th>
                                        <th><a><span class="fa"></span> Updated at</a></th>
                                        <th><a><span class="fa"></span> Created at</a></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($menuItems as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->parent_id }}</td>
                                        <td>{{ $item->type }}</td>
                                        <td>
                                            <label class="switch switch-3d switch-success">
                                                <input type="checkbox" {{ $item->status ? 'checked' : '' }} class="switch-input">
                                                <span class="switch-slider"></span>
                                            </label>
                                        </td>
                                        <td>{{ $item->url }}</td>
                                        <td>{{ $item->updated_at }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto mr-1">
                                                    <a href="{{ url('admin/content/menu/'.$id.'/items/'.$item->id.'/edit') }}" title="Edit" role="button" class="btn btn-sm btn-spinner btn-info">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </div>
                                                <div class="col-auto mr-1">
                                                    <a href="{{ url('admin/content/menu/'.$id.'/items/'.$item->id.'/delete') }}" title="Edit" role="button" class="btn btn-sm btn-danger btn-spinner">
                                                        <i class="fa fa-trash-o"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if($menuItems->count() > 10)
                                <div class="row">
                                    <div class="col-sm">
                                        <span class="pagination-caption">Displaying items from 1 to 10 of total 20 items.</span>
                                    </div>
                                    <div class="col-sm-auto">
                                        <nav>
                                            <ul class="pagination sizeClass">
                                                <li class="disabled page-item">
                                                    <span class="page-link">
                                                        <span aria-hidden="true">«</span>
                                                    </span>
                                                    <!---->
                                                </li>
                                                <li class="active page-item">
                                                    <a href="#" class="page-link">1</a>
                                                </li>
                                                <li class="page-item">
                                                    <a href="#" class="page-link">2</a>
                                                </li>
                                                <li class="page-item">
                                                    <!---->
                                                    <a href="#" aria-label="Next" class="page-link"><span aria-hidden="true">»</span></a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div> <!---->
                            @endif
                        @else
                            <div class="alert alert-warning">
                                <h4>Trenunto nema nista u meniju. Dodajte prvi item</h4>
                                <a href="{{ route('content.menu.items.add', ['id' => $id]) }}" role="button" class="btn btn-primary">
                                    <i class="fa fa-plus"></i>&nbsp; New Item
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
