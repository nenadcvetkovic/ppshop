@extends('themes.default.layouts.admin.index')

@section('main-content')
<div id="adminApp">
<div class="container-fluid">
    <div class="row justify-content-center">
        @include('themes.default.layouts.admin.message')
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>
                    Menu Items
                    <a href="{{ route('content.menu') }}" class="btn btn-success btn-sm">All menus</a>
                    <a href="{{ url('admin/content/menu/'.$id.'/items') }}" class="btn btn-dark btn-sm">All menu items</a>
                    {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                        <i class="fa fa-file-excel-o"></i>&nbsp; Export
                    </a>  --}}
                    {{-- <a href="{{ route('content.menu.items.add', ['id' => $id]) }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                        <i class="fa fa-plus"></i>&nbsp; New Item
                    </a> --}}
                </div>
                <div class="card-body" id="admin-app">
                    <pp-menu-items-add
                        :menu_id="{{ $id }}"
                        :menu_items="{{ $menuItems }}"
                    />
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
