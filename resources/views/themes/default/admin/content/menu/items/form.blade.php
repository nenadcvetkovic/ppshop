<div class="col-12 ml-auto mr-auto">
    @if($page == 'edit')
        <div class="form-group pull-left">
            <a onclick="return confirm('Do you want to delete this item?')" href="{{ route('content.menu.items.delete', ['id' => $id, 'item_id' => $item_id]) }}" role="button" class="btn btn-danger btn-spinner btn-sm m-b-0">
                <i class="fa fa-trash"></i>&nbsp; Delete
            </a>

        </div>
    @endif
    <div class="form-group pull-right">
        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
    </div>
    <div class="clearfix"></div>
    <hr class="delimiter mt-0">

    {!! Form::token() !!}

    <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('Ime') !!} </div>
        <div class="col-10"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime itema']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'name'])
    </div>

    {{--  <div class="form-group row {{ $errors->has('identifier') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('identifier') !!} </div>
        <div class="col-10">
            {!! Form::text('identifier', old('identifier'), ['class'=>'form-control ', ($page == 'edit' ? 'readonly' : ''), 'identifier'=>'identifier', 'placeholder'=>'Unesite identifier']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'identifier'])
    </div>  --}}
    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('type') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('Tip') !!} </div>
        <div class="col-10"> {!! Form::text('type', old('type'), ['class'=>'form-control', 'name'=>'type', 'placeholder'=>'Tip', 'readonly']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'type'])
    </div>

    <div class="form-group row {{ $errors->has('design_type') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Status') !!}
        </div>
        <div class="col-3">
            {!! Form::select('design_type', ['dropdown' => 'Dropdown', 'full-width' => 'Full-width'], @$menuItem->design_type ? @$menuItem->design_type : 'full-width', ['class'=>'form-control alert-success', 'name'=>'design_type', 'id'=>'design_type']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'design_type'])
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('url') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('URL') !!} </div>
        <div class="col-10"> {!! Form::text('url', old('url'), ['class'=>'form-control', 'name'=>'url', 'placeholder'=>'URL']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'url'])
    </div>


    <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Status') !!}
        </div>
        <div class="col-3">
            {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], @$menuItem->status ? @$menuItem->status : 0, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'status'])
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Opis') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('desc', old('desc'), ['class'=>'form-control', 'name'=>'desc', 'id'=>'desc', 'placeholder'=>'Kratki opis', 'rows'=>'3']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'desc'])
    </div>

    <hr class="delimiter" />
</div>
