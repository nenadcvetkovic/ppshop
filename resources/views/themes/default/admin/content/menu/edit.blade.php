@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">

        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i>
                                Edit menu
                                <a href="{{ route('content.menu') }}" class="btn btn-success btn-sm">
                                    <i class="fa fa-arrow-left"></i> Back to all menu
                                </a>
                                {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                                    <i class="fa fa-file-excel-o"></i>&nbsp; Export
                                </a>  --}}
                                {{-- <a href="{{ route('content.menu.items.add', ['id' => 1]) }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                                    <i class="fa fa-plus"></i>&nbsp; New Item
                                </a> --}}
                            </div>
                            <div class="card-body">

                                @include('themes.default.layouts.admin.message')

                                {{--  {!! Form::open(['route'=>'content.menu.create']) !!}  --}}
                                {{--  {!! Form::open(['route'=>'content.menu.create', 'files' => true, 'enctype' => "multipart/form-data"]) !!}  --}}
                                {!! Form::model($menu,array('route'=>array('content.menu.update',$menu->id))) !!}

                                @include('themes.default.admin.content.menu.form', ['page' => 'edit'])

                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
