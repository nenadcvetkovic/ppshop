@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid" id="adminApp">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i>
                    Menu configuration
                    <a href="{{ route('content.menu.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                        <i class="fa fa-plus"></i>&nbsp; New Menu
                    </a>
                </div>
                <div class="card-body">
                    @include('themes.default.layouts.admin.message')

                    <form>
                        <div class="row justify-content-md-between">
                            <div class="col col-lg-7 col-xl-5 form-group">
                                <div class="input-group">
                                    <input placeholder="Search" class="form-control">
                                    <span class="input-group-append">
                                        <button type="button" class="btn btn-primary">
                                            <i class="fa fa-search"></i>&nbsp; Search
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-auto form-group ">
                                <select class="form-control">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <table class="table table-hover table-listing">
                        <thead>
                            <tr>
                                <th><a><span class="fa fa-sort-amount-asc"></span> ID</a></th>
                                <th><a><span class="fa"></span> Name</a></th>
                                <th><a><span class="fa"></span> Identifier</a></th>
                                <th><a><span class="fa"></span> Status</a></th>
                                <th><a><span class="fa"></span> Created</a></th>
                                <th><a><span class="fa"></span> Updated</a></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($allMenus as $menu)
                            <tr>
                                <td>{{ $menu->id }}</td>
                                <td>{{ $menu->name }}</td>
                                <td>{{ $menu->identifier }}</td>
                                <td>
                                    <label class="switch switch-3d switch-success">
                                        <input type="checkbox" {{ $menu->status ? 'checked' : 'dfgfdhfgh' }} class="switch-input">
                                        <span class="switch-slider" v-on:click="updateMenuStatus({{ $menu->id }})"></span>
                                    </label>
                                </td>
                                <td>{{ $menu->created_at }}</td>
                                <td>{{ $menu->updated_at }}</td>
                                <td>
                                    <div class="row no-gutters d-none">
                                        <div class="col-auto">
                                            <a href="{{ url('admin/content/menu/'.$menu->id.'/edit') }}" title="Edit" role="button" class="btn btn-sm btn-spinner btn-info">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                        <form class="col">
                                            <button type="submit" title="Delete" class="btn btn-sm btn-danger">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </form>
                                    </div>
                                    <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Opcije
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ url('admin/content/menu/'.$menu->id.'/edit') }}">Edit</a>
                                        <a class="dropdown-item" href="{{ url('admin/content/menu/'.$menu->id.'/items') }}">Menu items</a>
                                        <a class="dropdown-item" href="{{ url('admin/content/menu/'.$menu->id.'/delete') }}">Delete</a>
                                    </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            <tr class="d-none">
                                <td>7</td>
                                <td>In est repellendus dolores aut quam facilis.</td>
                                <td>1986-02-04</td>
                                <td>
                                    <label class="switch switch-3d switch-success">
                                        <input type="checkbox" class="switch-input">
                                        <span class="switch-slider"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="row no-gutters">
                                        <div class="col-auto mr-1">
                                            <a href="https://demo.getcraftable.com/admin/exports/1/edit" title="Edit" role="button" class="btn btn-sm btn-spinner btn-info">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                        <form class="col">
                                            <button type="submit" title="Delete" class="btn btn-sm btn-danger">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm">
                            <span class="pagination-caption">Displaying items from 1 to 10 of total 20 items.</span>
                        </div>
                        <div class="col-sm-auto">
                            <nav>
                                <ul class="pagination sizeClass">
                                    <li class="disabled page-item">
                                        <span class="page-link">
                                            <span aria-hidden="true">«</span>
                                        </span>
                                        <!---->
                                    </li>
                                    <li class="active page-item">
                                        <a href="#" class="page-link">1</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="#" class="page-link">2</a>
                                    </li>
                                    <li class="page-item">
                                        <!---->
                                        <a href="#" aria-label="Next" class="page-link"><span aria-hidden="true">»</span></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div> <!---->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
