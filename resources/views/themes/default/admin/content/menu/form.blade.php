<div class="col-12 ml-auto mr-auto">
    <div class="form-group pull-right">
        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
    </div>
    <div class="clearfix"></div>
    <hr class="delimiter">

    {!! Form::token() !!}

    <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('Ime menija') !!} </div>
        <div class="col-10"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime kategorije']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'name'])
    </div>

    <div class="form-group row {{ $errors->has('identifier') ? 'has-error' : '' }}">
        <div class="col-2"> {!! Form::label('identifier') !!} </div>
        <div class="col-10">
            {!! Form::text('identifier', old('identifier'), ['class'=>'form-control ', ($page == 'edit' ? 'readonly' : ''), 'identifier'=>'identifier', 'placeholder'=>'Unesite identifier']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'identifier'])
    </div>
    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Status') !!}
        </div>
        <div class="col-3">
            {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], @$menu->status ? @$menu->status : 0, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'status'])
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Opis') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('desc', old('desc'), ['class'=>'form-control', 'name'=>'desc', 'id'=>'desc', 'placeholder'=>'Kratki opis', 'rows'=>'3']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'desc'])
    </div>

    <hr class="delimiter" />
</div>
