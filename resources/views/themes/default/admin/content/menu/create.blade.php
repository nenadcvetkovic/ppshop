@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 p-0">
                    <hr class="delimiter">
                        <div class="row pl-3">
                            <h3 class="col-3">Create Menu</h3>
                        </div>
                    <hr class="delimiter">
                </div>

                @include('themes.default.layouts.admin.message')


                <div class="col-12">
                    {!! Form::open(['route'=>'content.menu.create']) !!}
                    {{--  {!! Form::open(['route'=>'content.menu.create', 'files' => true, 'enctype' => "multipart/form-data"]) !!}  --}}

                    @include('themes.default.admin.content.menu.form', ['page' => 'create'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
