<div id="adminApp" class="col-12 ml-auto mr-auto">
    <div class="row">
        {!! Form::token() !!}
        <div class="col-lg-9 col-sm-12 col-md-12">
            <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                {{--  <div class="col-12"> {!! Form::label('Ime stranice') !!} </div>  --}}
                <div class="col-12"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime stranice']) !!} </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'name'])
            </div>
            <div class="form-group row {{ $errors->has('content') ? 'has-error' : '' }}">
                {{--  <div class="col-12">
                    {!! Form::label('Page content') !!}
                </div>  --}}

                @if (@$page)
                    <pp-edit-content :data="{{ $page }}" :cols="10" :rows="50" />
                @else
                    <pp-edit-content :cols="10" :rows="50" />
                @endif

                <hr class="delimiter">

                <div class="col-12">
                    {{--  {!! Form::textarea('content', old('content'), ['class'=>'form-control', 'name'=>'content', 'id'=>'content', 'placeholder'=>'Page content', 'rows'=>'20']) !!}  --}}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'content'])
            </div>
            <hr class="delimiter">

            {{--  SEO  --}}
            <div class="form-group row {{ $errors->has('seo_title') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('SEO Title') !!}
                </div>
                <div class="col-8">
                   {!! Form::text('seo_title', old('seo_title'), ['class'=>'form-control', 'name'=>'seo_title', 'placeholder'=>'SEO Title']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('seo_title') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('seo_desc') ? 'has-error' : '' }}">
                <div class="col-4">
                    {!! Form::label('SEO Descripton') !!}
                </div>
                <div class="col-12">
                    {!! Form::textarea('seo_desc', old('seo_desc'), ['class'=>'form-control', 'name'=>'seo_desc', 'id'=>'seo_desc', 'placeholder'=>'SEO description', 'rows'=>'5']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('seo_desc') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('seo_keywords') ? 'has-error' : '' }}">
              <div class="col-4">
                  {!! Form::label('SEO Keywords') !!}
              </div>
              <div class="col-12">
                  {!! Form::textarea('seo_keywords', old('seo_keywords'), ['class'=>'form-control', 'name'=>'seo_keywords', 'id'=>'seo_keywords', 'placeholder'=>'Seo Keywords', 'rows'=>'5']) !!}
              </div>
              <span class="text-danger">{{ $errors->first('seo_keywords') }}</span>
          </div>

        </div>
        <div class="col-lg-3 col-sm-12 col-md-12">
            <div class="form-group">
                <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
                <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
            </div>
            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('identifier') ? 'has-error' : '' }}">
                <div class="col-12"> {!! Form::label('identifier') !!} </div>
                <div class="col-12">
                    {!! Form::text('identifier', old('identifier'), ['class'=>'form-control ', ($view == 'edit' ? 'readonly' : ''), 'identifier'=>'identifier', 'placeholder'=>'Unesite identifier']) !!}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'identifier'])
            </div>

            <hr class="delimiter">


            {{--  <div class="input-group hdtuto control-group lst increment" >
                <div class="col-12 first-image">
                    {!! Form::file('images[]', ['type'=>'files', 'accept' => 'image/*', 'onchange' => 'preview_image(event)']) !!}
                    <img id="output_image" class="output_image" height="50" />
                </div>
            </div>  --}}

            <div>
                <pp-import-form-image-box
                :type="'page'"
                :label="''"
                :btn_text="'Izaberi sliku'"
                @if($view == 'edit' && isset($page->images[0]))
                    :old_image = "'{{ asset('storage/'.$page->images[0]->image_path) }}'"
                @endif
                    />
                <small class="text-muted">Image size 840x450</small>
            </div>

            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('layout') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Page Layout') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('layout',
                    [
                        '1-column' => '1-column',
                        '1-column-full-width' => '1-column-full-width',
                        '1-column-full-width-page' => '1-column-full-width-page',
                        '2-column-left' => '2-column-left',
                        '2-column-left-full-width' => '2-column-left-full-width',
                        '2-column-right' => '2-column-right',
                        '2-column-right-full-width' => '2-column-right-full-width',
                    ], @$page->layout ? @$page->layout : 0, ['class'=>'form-control alert-success', 'name'=>'layout', 'id'=>'layout']) !!}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'layout'])
            </div>

            <hr class="delimiter">

            <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Status') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], @$page->status ? @$page->status : 0, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
                </div>
                @include('themes.default.layouts.admin.form.message', ['errName' => 'status'])
            </div>


            <div class="form-group row {{ $errors->has('show_page_image') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Show header image on page') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('show_page_image', ['1' => 'Enabled', '0' => 'Disabled'], @$page->show_page_image ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'show_page_image', 'id'=>'show_page_image']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('show_page_image') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('show_page_title') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Show page title') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('show_page_title', ['1' => 'Enabled', '0' => 'Disabled'], @$page->show_page_title ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'show_page_title', 'id'=>'show_page_title']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('show_page_title') }}</span>
            </div>

            <div class="form-group row {{ $errors->has('show_desc') ? 'has-error' : '' }}">
                <div class="col-12">
                    {!! Form::label('Show description') !!}
                </div>
                <div class="col-12">
                    {!! Form::select('show_desc', ['1' => 'Enabled', '0' => 'Disabled'], @$page->show_desc ? '1' : '0', ['class'=>'form-control alert-success', 'name'=>'show_desc', 'id'=>'show_desc']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('show_desc') }}</span>
            </div>
        </div>
    </div>
</div>
