@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i>
                                Slider Configuraion
                                <a href="{{ route('content.slider') }}" class="btn btn-success btn-sm">
                                    <i class="fa fa-arrow-left"></i> Back to all sliders
                                </a>
                                {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                                    <i class="fa fa-file-excel-o"></i>&nbsp; Export
                                </a>  --}}
                                <a href="{{ route('content.slider.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                                    <i class="fa fa-plus"></i>&nbsp; New slider
                                </a>
                            </div>
                            <div class="card-body">
                                @include('themes.default.layouts.admin.message')

                                <div class="col-12">
                                    {!! Form::open(['url'=>'admin/content/slider/create']) !!}

                                    @include('themes.default.admin.content.slider.form', ['view' => 'create'])

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
