@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="container-fluid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i>
                                Slider Configuraion
                                {{--  <a href="{{ route('content.pages') }}" class="btn btn-success btn-sm">
                                    <i class="fa fa-arrow-left"></i> Back to all pages
                                </a>  --}}
                                {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                                    <i class="fa fa-file-excel-o"></i>&nbsp; Export
                                </a>  --}}
                                <a href="{{ route('content.slider.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                                    <i class="fa fa-plus"></i>&nbsp; New slider
                                </a>
                            </div>
                            <div class="card-body">
                                @include('themes.default.layouts.admin.message')

                                @if($allSliders->count())
                                <div class="col-12">
                                    <table class="table">
                                        <thead class="thead-dark">
                                          <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Idenitifier</th>
                                            <th scope="col"></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($allSliders as $slider)
                                            <tr>
                                                <th scope="row">{{ $slider->id }}</th>
                                                <td>{{ $slider->name }}</td>
                                                <td class="alert-{{ $slider->status ? 'success' : 'danger'}}">{{ $slider->status ? 'ON' : 'OFF' }}</td>
                                                <td>{{ $slider->identifier }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Opcije
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item" href="{{ url('admin/content/slider/'.$slider->id.'/edit') }}">Edit</a>
                                                        <a class="dropdown-item" href="{{ url('admin/content/slider/'.$slider->id.'/items') }}">Add items</a>
                                                        <a class="dropdown-item" href="{{ url('admin/content/slider/'.$slider->id.'/delete') }}">Delete</a>
                                                    </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                      </table>
                                </div>
                                @else
                                <div class="col-12 alert-danger p-3">
                                    <h1>No slider awailable</h1>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
