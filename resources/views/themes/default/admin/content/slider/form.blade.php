<section class="m-2 col-12">
    <div class="form-group pull-right">
        <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
        <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
    </div>

    <ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
      <li class="nav-item waves-effect waves-light">
        <a class="nav-link active" id="default-tab-md" data-toggle="tab" href="#default-md" role="tab" aria-controls="default-md" aria-selected="true">Default</a>
      </li>
      <li class="nav-item waves-effect waves-light">
        <a class="nav-link" id="design-tab-md" data-toggle="tab" href="#design-md" role="tab" aria-controls="design-md" aria-selected="false">Design</a>
      </li>
    </ul>
    <div class="tab-content p-5" id="myTabContentMD">
      <div class="tab-pane fade show active" id="default-md" role="tabpanel" aria-labelledby="default-tab-md">
          <div class="col-12 mt-5 mt-5 ml-auto mr-auto">
              {!! Form::token() !!}
              <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                  <div class="col-2"> {!! Form::label('Ime slajdera') !!} </div>
                  <div class="col-10"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime slajdera']) !!} </div>
                  <span class="text-danger">{{ $errors->first('name') }}</span>
              </div>
              <div class="form-group row {{ $errors->has('idenitifier') ? 'has-error' : '' }}">
                <div class="col-2"> {!! Form::label('Idenitifier') !!} </div>
                <div class="col-10">
                    {!! Form::text('idenitifier', old('idenitifier'), ['class'=>'form-control ', ($view == 'edit' ? 'readonly' : ''), 'idenitifier'=>'idenitifier', 'placeholder'=>'Unesite idenitifier']) !!}
                </div>
                <span class="text-danger">{{ $errors->first('idenitifier') }}</span>
            </div>
              <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
                  <div class="col-2">
                      {!! Form::label('Status') !!}
                  </div>
                  <div class="col-3">
                      {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], @$slider->status ? @$slider->status : 0, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
                  </div>
                  <span class="text-danger">{{ $errors->first('status') }}</span>
              </div>
              <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
                  <div class="col-2">
                      {!! Form::label('Opis') !!}
                  </div>
                  <div class="col-10">
                      {!! Form::textarea('desc', old('desc'), ['class'=>'form-control', 'name'=>'desc', 'id'=>'desc', 'placeholder'=>'Opis', 'rows'=>'3']) !!}
                  </div>
                  <span class="text-danger">{{ $errors->first('desc') }}</span>
              </div>
          </div>
      </div>
      <div class="tab-pane fade" id="design-md" role="tabpanel" aria-labelledby="design-tab-md">

        {{--  <div class="form-group row">
            <label class="col-5" for="slider_type">Slider Type</label>
            <select class="form-control col-4" name="slider_type" id="slider_type">
            @foreach (new DirectoryIterator('../resources/views/themes/default/frontend/plugins/slider') as $fileInfo)
                @if($fileInfo->getFilename() != '.' && $fileInfo->getFilename() != '..' && !is_dir($fileInfo->getFilename()))
                {{  $name = preg_replace('/.blade.php/', '', $fileInfo->getFilename()) }}
                <option {{ (@$slider->slider_type == $name) ? 'selected' : '' }} value="{{ $name }}">{{ $name }}</option>
                @endif
            @endforeach
            </select>
          </div>  --}}


          <hr class="delimiter">


          <div class="form-group row {{ $errors->has('slider_min_heigh') ? 'has-error' : '' }}">
            <div class="col-5"> {!! Form::label('Slider min height') !!} </div>
            <div class="col-4"> {!! Form::text('slider_min_heigh', old('slider_min_heigh', @$configuration->getSliderHeight($slider->id) ?? ''), ['class'=>'form-control', 'name'=>'slider_min_heigh', 'placeholder'=>'Unesite max height slider']) !!} </div>
            <span class="text-danger">{{ $errors->first('slider_min_heigh') }}</span>
        </div>

          <div class="form-group row {{ $errors->has('slider_max_heigh') ? 'has-error' : '' }}">
            <div class="col-5"> {!! Form::label('Slider max height') !!} </div>
            <div class="col-4"> {!! Form::text('slider_max_heigh', old('slider_max_heigh', @$configuration->getSliderHeight($slider->id) ?? ''), ['class'=>'form-control', 'name'=>'slider_max_heigh', 'placeholder'=>'Unesite max height slider']) !!} </div>
            <span class="text-danger">{{ $errors->first('slider_max_heigh') }}</span>
        </div>

      </div>
    </div>
  </section>
