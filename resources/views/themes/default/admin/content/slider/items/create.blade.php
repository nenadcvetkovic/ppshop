@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 p-0">
                    <hr class="delimiter">
                    <h3 class="pl-2">Crate new slider</h3>
                    <hr class="delimiter">
                    <div class="col-12 p-1">
                        <a class="btn btn-danger" href="{{ url('admin/content/slider') }}">All sliders</a>
                    </div>
                    <hr class="delimiter">

                    @include('themes.default.layouts.admin.message')
                    <div class="col-12">
                        @php
                            $slider_id = request()->route()->parameters['slider_id'];
                        @endphp
                        {!! Form::open(['route'=>'content.slider.items.create'], ['slider_id' => $slider_id]) !!}

                        @include('themes.default.admin.content.slider.items.form', ['page' => 'create'])

                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
