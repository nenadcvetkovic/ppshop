{{--  {{ dd($sliderItem) }}  --}}
<input type="hidden" value="{{ csrf_token() }}" name="_token"/>
<input type="hidden" value="{{ $sliderItem->id }}" name="sliderItem"/>
<div class="form-group row">
    <label class="col-3" for="name">Name</label>
    <input class="form-control col-4" type="text" name="name" value="{{ $sliderItem->name }}" placeholder="Item name">
  <label class="col-2" for="name">Status</label>
  <div class="col-3">
    <select id="inputState" class="form-control" name="status">
        <option value="0" {{ $sliderItem->status ? 'selected' : '' }}>Disable</option>
        <option value="1" {{ $sliderItem->status ? 'selected' : '' }}>Enable</option>
    </select>
  </div>
</div>
<div class="form-group row">
</div>
<hr class="delimiter">
<div class="form-group row">
    <label class="col-3" for="content_from_url">Change current URL</label>
    <input class="form-control col-8" type="text" value="{{ $sliderItem->image_path ? '/storage/' . $sliderItem->image_path : '' }}" name="content_from_url" placeholder="Content URL from web">
    <small id="emailHelp" class="form-text text-muted col-12">Add image URL from web</small>
</div>
<h6>OR</h6>
<div class="form-group row">
    <label class="col-3" for="content_from_file">Upload new image</label>
    <input type="file" name="content_from_file" class="form-control-file col-3" id="content_from_file" onchange='preview_image(event)'/>
    <div id="imagePreview" class="col-3">
        @if ($sliderItem->image_path)
        <img src="/storage/{{ $sliderItem->image_path }}" />
        @endif
    </div>
    <div class="col-3">
        <img id="output_image" class="output_image" height="50" />
    </div>
</div>
<hr class="delimiter">
<div class="form-group row">
    <label class="col-3" for="content-1">Content 1</label>
    <input class="form-control col-8" name="caption_1" type="text" value="{{ $sliderItem->caption_1 }}" placeholder="Content 1">
</div>
<div class="form-group row">
    <label class="col-3" for="content-2">Content 2</label>
    <input class="form-control col-8" name="caption_2" type="text" value="{{ $sliderItem->caption_2 }}" placeholder="Content 2">
</div>
<hr class="delimiter">
<button type="submit" class="btn btn-primary">Submit</button>

<script>
    function preview_image(event)
    {
        $('#imagePreview').hide();
        var reader = new FileReader();
        reader.onload = function()
        {
            //var output = document.getElementById('output_image');
            //console.log(event.target.parentNode.querySelector('.output_image'));
            var output = event.target.parentNode.querySelector('.output_image');
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>
