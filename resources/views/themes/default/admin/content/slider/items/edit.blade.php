@extends('themes.default.layouts.admin.index')

@section('main-content')
<div id="adminAppp">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Slider Item ID: {{ @$sliderItem->id ? $sliderItem->id : $sliderItem }}
                        <a href="{{ route('content.slider') }}" class="btn btn-success btn-sm">
                            <i class="fa fa-arrow-left"></i> Back to all sliders
                        </a>
                        <a href="{{ route('content.slider.items', $sliderItem->slider_id) }}" class="btn btn-black btn-sm">
                            <i class="fa fa-arrow-left"></i> Back to slider items
                        </a>
                        {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                            <i class="fa fa-file-excel-o"></i>&nbsp; Export
                        </a>  --}}
                        {{--  <a href="{{ route('content.slider.items.create', $slider->id) }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                            <i class="fa fa-plus"></i>&nbsp; New item
                        </a>  --}}
                        {{--  <div id="items-slider-app"  class="btn-sm pull-right m-b-0">
                            <pp-items-slider-add :slider="{{ $sliderItem }}"/>
                        </div>  --}}
                    </div>
                    <div class="card-body">
                        @include('themes.default.layouts.admin.message')

                        <div class="col-12 pt-2">
                            <form method="post" action="{{ url('admin/content/slider/'.$sliderItem->slider_id.'/items/'.$sliderItem->id.'/edit') }}" enctype="multipart/form-data">
                                @include('themes.default.admin.content.slider.items.simple-form')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

