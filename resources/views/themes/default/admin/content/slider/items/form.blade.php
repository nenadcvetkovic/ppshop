{{--  {{ dd($sliderItem) }}  --}}
<input type="hidden" value="{{ csrf_token() }}" name="_token"/>
<input type="hidden" value="{{ $sliderItem->id }}" name="sliderItem"/>
<div class="form-group row">
    <label class="col-3" for="name">Name</label>
    <input class="form-control col-4" type="text" name="name" value="{{ $sliderItem->name }}" placeholder="Item name">
  <label class="col-2" for="name">Status</label>
  <div class="col-3">
    <select id="inputState" class="form-control" name="status">
        <option value="0" {{ $sliderItem->status ? 'selected' : '' }}>Disable</option>
        <option value="1" {{ $sliderItem->status ? 'selected' : '' }}>Enable</option>
    </select>
  </div>
</div>
<div class="form-group row">
</div>
<hr class="delimiter">
<div class="form-group row">
    <label class="col-3" for="content_from_url">Change current URL</label>
    <input class="form-control col-8" type="text" value="{{ $sliderItem->image_url }}" name="content_from_url" placeholder="Content URL from web">
    <small id="emailHelp" class="form-text text-muted col-12">Add image URL from web</small>
</div>
<h6>OR</h6>
<div class="form-group row">
    <label class="col-3" for="content_from_file">Upload new image</label>
    <input type="file" @change="onFileChange" name="content_from_file" class="form-control-file col-5" id="content_from_file"/>
    <div id="imagePreview" class="col-4">
        <img v-if="imageUrl" src="{{ public_path() . '/storage/' .  $sliderItem->image_url }}" />
    </div>
</div>
<hr class="delimiter">
<div class="form-group row">
    <label class="col-3" for="content-1">Content 1</label>
    <input class="form-control col-8" name="caption_1" type="text" value="{{ $sliderItem->caption_1 }}" placeholder="Content 1">
</div>
<div class="form-group row">
    <label class="col-3" for="content-2">Content 2</label>
    <input class="form-control col-8" name="caption_2" type="text" value="{{ $sliderItem->caption_2 }}" placeholder="Content 2">
</div>
<hr class="delimiter">
<button type="submit" class="btn btn-primary">Submit</button>
