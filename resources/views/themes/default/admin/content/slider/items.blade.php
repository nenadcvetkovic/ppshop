@extends('themes.default.layouts.admin.index')

@section('main-content')
<div id="adminApp">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>
                        Slider ID: {{ @$slider->id ? $slider->id : $slider }}
                        <a href="{{ route('content.slider') }}" class="btn btn-success btn-sm">
                            <i class="fa fa-arrow-left"></i> Back to all sliders
                        </a>
                        {{--  <a href="{{ route('content.menu.items.export', ['id' => $id]) }}" role="button" class="btn btn-primary btn-sm pull-right m-b-0 ml-2">
                            <i class="fa fa-file-excel-o"></i>&nbsp; Export
                        </a>  --}}
                        {{--  <a href="{{ route('content.slider.items.create', $slider->id) }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                            <i class="fa fa-plus"></i>&nbsp; New item
                        </a>  --}}
                        <div id="items-slider-app"  class="btn-sm pull-right m-b-0">
                            <pp-items-slider-add :slider="{{ $slider }}"/>
                        </div>
                    </div>
                    <div class="card-body">
                        @include('themes.default.layouts.admin.message')

                        @if($sliderItems)
                        <div class="col-12">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Image URL</th>
                                    <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($sliderItems as $item)
                                    <tr>
                                        <th scope="row">{{ $slider->id }}</th>
                                        <td>{{ $item->name }}</td>
                                        <td class="alert-{{ $item->status ? 'success' : 'danger'}}">{{ $item->status ? 'ON' : 'OFF' }}</td>
                                        <td><img src="{{ $item->image_type == 'url' ? $item->image_path : asset('storage/'.$item->image_path) }}" height="50" /></td>
                                        <td>
                                            <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Opcije
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{ url('admin/content/slider/'.$slider->id.'/items/'.$item->id.'/edit') }}">Edit</a>
                                                <a class="dropdown-item" href="{{ url('admin/content/slider/'.$slider->id.'/items/'.$item->id.'/delete') }}">Delete</a>
                                            </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                </table>
                        </div>
                        @else
                        <div class="col-12 alert-danger p-3">
                            <h1>No slider items awailable</h1>
                            Create new slider item
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
