@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-gears"></i>
                    Cache Management
                </div>
                <div class="card-body">
                    @include('themes.default.layouts.admin.message')

                    <div class="pull-right">
                        <a href="{{ route('system.tools.action', 'clear-cache') }}" role="button" class="btn btn-primary btn-spinner btn-sm m-b-0 mr-10">
                            <i class="fa fa-minus"></i>&nbsp; Clear Cache
                        </a>
                        <a href="{{ route('system.tools.action', 'clear-route') }}" role="button" class="btn btn-success btn-spinner btn-sm m-b-0 mr-10">
                            <i class="fa fa-minus"></i>&nbsp; Clear Route Cache
                        </a>
                        <a href="{{ route('system.tools.action', 'clear-config') }}" role="button" class="btn btn-danger btn-spinner btn-sm m-b-0 mr-10">
                            <i class="fa fa-minus"></i>&nbsp; Clear Config Cache
                        </a>
                        <a href="{{ route('system.tools.action', 'clear-view') }}" role="button" class="btn btn-primary btn-spinner btn-sm m-b-0 mr-10">
                            <i class="fa fa-minus"></i>&nbsp; Clear View Cache
                        </a>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
