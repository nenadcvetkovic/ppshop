@extends('themes.default.layouts.admin.index')

@section('main-content')
<div id="adminApp">
    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="col-12 alert-danger p-3">
                        </div>
                        <pp-edit-content :cols="10" :rows="50"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
