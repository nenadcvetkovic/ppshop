@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid" id="adminAppp">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-gears"></i>
                    Stores configuration
                    {{-- <a href="{{ route('content.menu.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                        <i class="fa fa-plus"></i>&nbsp; New Menu
                    </a> --}}
                </div>
                <div class="card-body">
                    @include('themes.default.layouts.admin.message')

                    {!! Form::model($settings_configuration,array('route'=>array('store.settings.general'), 'files' => true, 'enctype' => "multipart/form-data")) !!}

                    <section class="m-2 col-12">

                        <div class="form-group pull-right">
                            <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
                            <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
                        </div>

                        <ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link active" id="default-tab-md" data-toggle="tab" href="#default-md" role="tab" aria-controls="default-md" aria-selected="true">Default</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="head-tab-md" data-toggle="tab" href="#head-md" role="tab" aria-controls="head-md" aria-selected="false">Head</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="body-tab-md" data-toggle="tab" href="#body-md" role="tab" aria-controls="body-md" aria-selected="false">Body</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="seo-tab-md" data-toggle="tab" href="#seo-md" role="tab" aria-controls="seo-md" aria-selected="false">SEO</a>
                            </li>
                        </ul>

                        <div class="tab-content p-5" id="myTabContentMD">
                            <div class="tab-pane fade show active" id="default-md" role="tabpanel" aria-labelledby="default-tab-md">
                                @include('themes.default.admin.stores.settings.general.default-form')
                            </div>

                            {{-- Head Configuration --}}
                            <div class="tab-pane fade" id="head-md" role="tabpanel" aria-labelledby="head-tab-md">
                                @include('themes.default.admin.stores.settings.general.head-form')
                            </div>

                            {{-- Body Configuration --}}
                            <div class="tab-pane fade" id="body-md" role="tabpanel" aria-labelledby="body-tab-md">
                                @include('themes.default.admin.stores.settings.general.body-form')
                            </div>

                            {{-- SEO Configuration --}}
                            <div class="tab-pane fade" id="seo-md" role="tabpanel" aria-labelledby="seo-tab-md">
                                @include('themes.default.admin.stores.settings.general.seo')
                            </div>
                        </div>
                    </section>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
