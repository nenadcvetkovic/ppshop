    <h4>Body configuration</h4>
    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('store/settings/general/top-body-script') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Top Body scripts') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('store/settings/general/top-body-script', old('store/settings/general/top-body-script', $settings_configuration->getConfigValue('store/settings/general/top-body-script')), ['class'=>'form-control', 'name'=>'store/settings/general/top-body-script', 'id'=>'store/settings/general/top-body-script', 'placeholder'=>'Head Scripts', 'rows'=>'5']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('store/settings/general/top-body-script') }}</span>
    </div>


    <div class="form-group row {{ $errors->has('store/settings/general/bottom-body-script') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Bottom Body scripts') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('store/settings/general/bottom-body-script', old('store/settings/general/bottom-body-script', $settings_configuration->getConfigValue('store/settings/general/bottom-body-script')), ['class'=>'form-control', 'name'=>'store/settings/general/bottom-body-script', 'id'=>'store/settings/general/bottom-body-script', 'placeholder'=>'Head Scripts', 'rows'=>'5']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('store/settings/general/bottom-body-script') }}</span>
    </div>
