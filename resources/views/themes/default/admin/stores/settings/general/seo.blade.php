
    <h4>General configuration</h4>
    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('store/settings/general/default/title') ? 'has-error' : '' }}">
        <div class="col-5"> {!! Form::label('Title') !!} </div>
        <div class="col-5"> {!! Form::text('store/settings/general/default/title', old('store/settings/general/default/title', $settings_configuration->getConfigValue('store/settings/general/default/title')), ['class'=>'form-control', 'name'=>'store/settings/general/default/title', 'placeholder'=>'Unesite title']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'store/settings/general/default/title'])
    </div>

    <div class="form-group row {{ $errors->has('store/settings/general/default/author') ? 'has-error' : '' }}">
        <div class="col-5"> {!! Form::label('Author') !!} </div>
        <div class="col-5"> {!! Form::text('store/settings/general/default/author', old('store/settings/general/default/author', $settings_configuration->getConfigValue('store/settings/general/default/author')), ['class'=>'form-control', 'name'=>'store/settings/general/default/author', 'placeholder'=>'Unesite author']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'store/settings/general/default/author'])
    </div>

    <div class="form-group row {{ $errors->has('store/settings/general/default/robots') ? 'has-error' : '' }}">
        <div class="col-5">
            {!! Form::label('Robots') !!}
        </div>
        <div class="col-5">
            {!! Form::select('store/settings/general/default/robots',
            [
                'index, follow' => 'index, follow',
                'index, nofollow'  => 'index, nofollow',
                'noindex, nofollow' => 'noindex, nofollow',
                'noindex, follow' => 'noindex, follow',
                'follow, index' => 'follow, index'
            ],
            $configuration->getRobots(),
            ['class'=>'form-control alert-success', 'name'=>'store/settings/general/default/robots', 'id'=>'shop-status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('store/settings/general/default/robots') }}</span>
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('store/settings/general/default/site-keywords') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Site Keywords') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('store/settings/general/default/site-keywords', old('store/settings/general/default/site-keywords', $settings_configuration->getConfigValue('store/settings/general/default/site-keywords')), ['class'=>'form-control', 'name'=>'store/settings/general/default/site-keywords', 'id'=>'store/settings/general/default/site-keywords', 'placeholder'=>'Site keywordds', 'rows'=>'5']) !!}
            <small class="text-muted">Add mulityple keywords with "," separated</small>
        </div>
        <span class="text-danger">{{ $errors->first('store/settings/general/default/site-keywords') }}</span>
    </div>

    <hr class="delimiter">


    <h4>SEO configuration</h4>
    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('store/settings/general/default/seo-title') ? 'has-error' : '' }}">
        <div class="col-5"> {!! Form::label('SEO Title') !!} </div>
        <div class="col-5"> {!! Form::text('store/settings/general/default/seo-title', old('store/settings/general/default/seo-title', $settings_configuration->getConfigValue('store/settings/general/default/seo-title')), ['class'=>'form-control', 'name'=>'store/settings/general/default/seo-title', 'placeholder'=>'Unesite SEO title']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'store/settings/general/default/seo-title'])
    </div>

    <div class="form-group row {{ $errors->has('store/settings/general/default/seo-description') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('SEO description') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('store/settings/general/default/seo-description', old('store/settings/general/default/seo-description', $settings_configuration->getConfigValue('store/settings/general/default/seo-description')), ['class'=>'form-control', 'name'=>'store/settings/general/default/seo-description', 'id'=>'store/settings/general/default/seo-description', 'placeholder'=>'SEO description', 'rows'=>'5']) !!}
            <small class="text-muted"></small>
        </div>
        <span class="text-danger">{{ $errors->first('store/settings/general/default/seo-description') }}</span>
    </div>

    <hr class="delimiter">
