    <h4>Head configuration</h4>
    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('store/settings/general/head-script') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Head scripts') !!}
        </div>
        <div class="col-10">
            {!! Form::textarea('store/settings/general/head-script', old('store/settings/general/head-script', $settings_configuration->getConfigValue('store/settings/general/head-script')), ['class'=>'form-control', 'name'=>'store/settings/general/head-script', 'id'=>'store/settings/general/head-script', 'placeholder'=>'Head Scripts', 'rows'=>'5']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('store/settings/general/head-script') }}</span>
    </div>

    <hr class="delimiter">
