
    <div class="form-group row {{ $errors->has('store/settings/general/show-loader') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show Loader') !!}
        </div>
        <div class="col-8">
            {!! Form::select('store/settings/general/show-loader', ['1' => 'Yes', '0' => 'No'], old('store/settings/general/show-loader', $settings_configuration->getConfigValue('store/settings/general/show-loader')), ['class'=>'form-control alert-success', 'name'=>'store/settings/general/show-loader', 'id'=>'status']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'store/settings/general/show-loader'])
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('store/settings/general/base-url') ? 'has-error' : '' }}">
        <div class="col-4 text-right"> {!! Form::label('Base url') !!} </div>
        <div class="col-8"> {!! Form::text('store/settings/general/base-url', old('store/settings/general/base-url', $settings_configuration->getConfigValue('store/settings/general/base-url')), ['class'=>'form-control', 'name'=>'store/settings/general/base-url', 'placeholder'=>'Unesite base url']) !!} </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'store/settings/general/base-url'])
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('store/settings/general/maintenance') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Maintenance') !!}
        </div>
        <div class="col-8">
            {!! Form::select('store/settings/general/maintenance', ['1' => 'Enabled', '0' => 'Disabled'], old('store/settings/general/maintenance', $settings_configuration->getConfigValue('store/settings/general/maintenance')), ['class'=>'form-control alert-success', 'name'=>'store/settings/general/maintenance', 'id'=>'status']) !!}
        </div>
        @include('themes.default.layouts.admin.form.message', ['errName' => 'store/settings/general/maintenance'])
    </div>

