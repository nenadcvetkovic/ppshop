
    <div class="form-group row {{ $errors->has('product/rating/status') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show rating') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/rating/status',
            ['1' => 'Enabled', '0' => 'Disabled'],
            $settings_configuration->getConfigValue('product/rating/status'),
            ['class'=>'form-control alert-success', 'name'=>'product/rating/status', 'id'=>'product-rating-status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/rating/status') }}</span>
    </div>

    <div class="form-group row {{ $errors->has('product/wishlist/status') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show wishlist') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/wishlist/status',
            ['1' => 'Enabled', '0' => 'Disabled'],
            $settings_configuration->getConfigValue('product/wishlist/status'),
            ['class'=>'form-control alert-success', 'name'=>'product/wishlist/status', 'id'=>'product-wishlist-status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/wishlist/status') }}</span>
    </div>

    <div class="form-group row {{ $errors->has('product/sharing/status') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show sharing') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/sharing/status',
            ['1' => 'Enabled', '0' => 'Disabled'],
            $settings_configuration->getConfigValue('product/sharing/status'),
            ['class'=>'form-control alert-success', 'name'=>'product/sharing/status', 'id'=>'product-sharing-status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/sharing/status') }}</span>
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('product/additional-info/status') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show additional info') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/additional-info/status',
            ['1' => 'Enabled', '0' => 'Disabled'],
            $settings_configuration->getConfigValue('product/additional-info/status'),
            ['class'=>'form-control alert-success', 'name'=>'product/additional-info/status', 'id'=>'product-sharing-status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/additional-info/status') }}</span>
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('product/show_prod_stock_status') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show product in stock status') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/show_prod_stock_status',
            ['0' => 'Hide', '1' => 'Show'],
            $settings_configuration->getConfigValue('product/show_prod_stock_status'),
            ['class'=>'form-control alert-success', 'name'=>'product/show_prod_stock_status', 'id'=>'product-list-mode']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/show_prod_stock_status') }}</span>
    </div>


    <div class="form-group row {{ $errors->has('product/show_prod_sku') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show Product SKU') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/show_prod_sku',
            ['0' => 'Hide', '1' => 'Show'],
            $settings_configuration->getConfigValue('product/show_prod_sku'),
            ['class'=>'form-control alert-success', 'name'=>'product/show_prod_sku', 'id'=>'product-list-mode']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/show_prod_sku') }}</span>
    </div>

    <div class="form-group row {{ $errors->has('product/show_prod_price') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show product price') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/show_prod_price',
            ['0' => 'Hide', '1' => 'Show'],
            $settings_configuration->getConfigValue('product/show_prod_price'),
            ['class'=>'form-control alert-success', 'name'=>'product/show_prod_price', 'id'=>'product-list-mode']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/show_prod_price') }}</span>
    </div>

    <div class="form-group row {{ $errors->has('product/show_prod_cat') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show product categories') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/show_prod_cat',
            ['0' => 'Hide', '1' => 'Show'],
            $settings_configuration->getConfigValue('product/show_prod_cat'),
            ['class'=>'form-control alert-success', 'name'=>'product/show_prod_cat', 'id'=>'product-list-mode']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/show_prod_cat') }}</span>
    </div>

    <div class="form-group row {{ $errors->has('product/show_prod_desc') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show product desc') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/show_prod_desc',
            ['0' => 'Hide', '1' => 'Show'],
            $settings_configuration->getConfigValue('product/show_prod_desc'),
            ['class'=>'form-control alert-success', 'name'=>'product/show_prod_desc', 'id'=>'product-list-mode']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/show_prod_desc') }}</span>
    </div>

    <hr class="delimiter">

    <div class="form-group row {{ $errors->has('product/show/related-products/status') ? 'has-error' : '' }}">
        <div class="col-4 text-right">
            {!! Form::label('Show related prodcuts on product page') !!}
        </div>
        <div class="col-8">
            {!! Form::select('product/show/related-products/status',
            ['1' => 'Enabled', '0' => 'Disabled'],
            $settings_configuration->getConfigValue('product/show/related-products/status'),
            ['class'=>'form-control alert-success', 'name'=>'product/show/related-products/status', 'id'=>'product-show-related-status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('product/show/related-products/status') }}</span>
    </div>

    <hr class="delimiter">
