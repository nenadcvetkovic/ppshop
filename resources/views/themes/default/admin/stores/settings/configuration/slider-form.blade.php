
    <div class="form-group row {{ $errors->has('slider/status') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('slider Status') !!}
        </div>
        <div class="col-3">
            {!! Form::select('slider/status', ['1' => 'Enabled', '0' => 'Disabled'],
            $settings_configuration->getConfigValue('slider/status'),
            ['class'=>'form-control alert-success', 'name'=>'slider/status', 'id'=>'slider-status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('slider/status') }}</span>
    </div>

    <div class="form-group row {{ $errors->has('slider/active/id') ? 'has-error' : '' }}">
        <div class="col-4">
            {!! Form::label('slider/active/id') !!}
        </div>
        <div class="col-3">
            {!! Form::select('slider/active/id', (array)$allSliders,
            $settings_configuration->getConfigValue('slider/active/id'),
            ['class'=>'form-control alert-success', 'name'=>'slider/active/id', 'id'=>'slider/active/id']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('slider/active/id') }}</span>
    </div>

    <hr class="delimiter">
