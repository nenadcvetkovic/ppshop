
<div class="form-group row {{ $errors->has('account/auth/status') ? 'has-error' : '' }}">
    <div class="col-5">
        {!! Form::label('Show account login/register') !!}
    </div>
    <div class="col-5">
        {!! Form::select('account/auth/status', ['1' => 'Enabled', '0' => 'Disabled'], $configuration->showSiteAuth() ? 1 : 0, ['class'=>'form-control alert-success', 'name'=>'account/auth/status', 'id'=>'shop-status']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('account/auth/status') }}</span>
</div>

<hr class="delimiter">

<div class="form-group row {{ $errors->has('account/currency/status') ? 'has-error' : '' }}">
    <div class="col-5">
        {!! Form::label('Show ste currency') !!}
    </div>
    <div class="col-5">
        {!! Form::select('account/currency/status', ['1' => 'Enabled', '0' => 'Disabled'], $configuration->showSiteCurency() ? 1 : 0, ['class'=>'form-control alert-success', 'name'=>'account/currency/status', 'id'=>'shop-status']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('account/currency/status') }}</span>
</div>

<hr class="delimiter">

<div class="form-group row {{ $errors->has('account/language/status') ? 'has-error' : '' }}">
    <div class="col-5">
        {!! Form::label('Show site language') !!}
    </div>
    <div class="col-5">
        {!! Form::select('account/language/status', ['1' => 'Enabled', '0' => 'Disabled'], $configuration->showSiteLanguage() ? 1 : 0, ['class'=>'form-control alert-success', 'name'=>'account/language/status', 'id'=>'shop-status']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('account/language/status') }}</span>
</div>

<hr class="delimiter">
