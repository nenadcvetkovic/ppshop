
{{--  <div class="form-group row d-none {{ $errors->has('stores/settings/configuration/maintenance/ips') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Whitelist ip') !!}
    </div>
    <div class="col-8">
        {!! Form::textarea('stores/settings/configuration/maintenance/ips', old('stores/settings/configuration/maintenance/ips', $configuration->getWhitelistIps()), ['class'=>'form-control', 'name'=>'stores/settings/configuration/maintenance/ips', 'id'=>'stores/settings/configuration/maintenance/ips', 'placeholder'=>'Whitelist IP // ex. 127.0.0.1,127.0.0.2', 'rows'=>'3']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('stores/settings/configuration/maintenance/ips') }}</span>
</div>  --}}

<div class="form-group row {{ $errors->has('stores/settings/configuration/maintenance/status') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Mainteance status' ) !!}
    </div>
    <div class="col-8">
        @if (file_exists(storage_path('framework/down')))
        <div class="btn btn-danger btn-spinner btn-sm m-b-0 mr-10" disabled>Enabled</div>
        <a href="{{ route('system.site.maintenance', 'up') }}" role="button" class="btn btn-primary btn-spinner btn-sm m-b-0 mr-10">
            <i class="fa fa-minus"></i>&nbsp; Disable Maintenace
        </a>
        @else
        <div class="btn btn-success btn-spinner btn-sm m-b-0 mr-10" disabled>Disabled</div>
        <a href="{{ route('system.site.maintenance', 'down') }}" role="button" class="btn btn-danger btn-spinner btn-sm m-b-0 mr-10">
            <i class="fa fa-minus"></i>&nbsp; Enable Maintenance
        </a>
        @endif
    </div>
    <span class="text-danger">{{ $errors->first('stores/settings/configuration/maintenance/status') }}</span>
</div>


<hr class="delimiter">

