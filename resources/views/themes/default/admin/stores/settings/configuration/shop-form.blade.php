
    <div class="form-group row {{ $errors->has('shop/status') ? 'has-error' : '' }}">
        <div class="col-2">
            {!! Form::label('Shop Status') !!}
        </div>
        <div class="col-3">
            {!! Form::select('shop/status', ['1' => 'Enabled', '0' => 'Disabled'], 0, ['class'=>'form-control alert-success', 'name'=>'shop/status', 'id'=>'shop-status']) !!}
        </div>
        <span class="text-danger">{{ $errors->first('shop/status') }}</span>
    </div>

    <hr class="delimiter">
