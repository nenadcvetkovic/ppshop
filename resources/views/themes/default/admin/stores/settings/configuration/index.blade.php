@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-gears"></i>
                    Stores configuration
                    {{-- <a href="{{ route('content.menu.create') }}" role="button" class="btn btn-primary btn-spinner btn-sm pull-right m-b-0">
                        <i class="fa fa-plus"></i>&nbsp; New Menu
                    </a> --}}
                </div>
                <div class="card-body">
                    @include('themes.default.layouts.admin.message')

                    {!! Form::model($settings_configuration,array('route'=>array('store.settings.configuration'), 'files' => true, 'enctype' => "multipart/form-data")) !!}

                    <section class="m-2 col-12">

                        <div class="form-group pull-right">
                            <input class="btn btn-outline-primary btn-sm" type="submit" value="Submit">
                            <input class="btn btn-outline-danger btn-sm" type="reset" value="Reset">
                        </div>

                        <ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link active" id="default-tab-md" data-toggle="tab" href="#default-md" role="tab" aria-controls="default-md" aria-selected="true">Default</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="design-tab-md" data-toggle="tab" href="#design-md" role="tab" aria-controls="design-md" aria-selected="false">Design</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="shop-tab-md" data-toggle="tab" href="#shop-md" role="tab" aria-controls="shop-md" aria-selected="false">Shop</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="product-tab-md" data-toggle="tab" href="#product-md" role="tab" aria-controls="product-md" aria-selected="false">Product</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="catalog-tab-md" data-toggle="tab" href="#catalog-md" role="tab" aria-controls="catalog-md" aria-selected="false">Catalog</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="slider-tab-md" data-toggle="tab" href="#slider-md" role="tab" aria-controls="slider-md" aria-selected="false">Slider</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                            <a class="nav-link" id="account-tab-md" data-toggle="tab" href="#account-md" role="tab" aria-controls="account-md" aria-selected="false">Account</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="maintenance-tab-md" data-toggle="tab" href="#maintenance-md" role="tab" aria-controls="maintenance-md" aria-selected="false">Maintenance</a>
                            </li>
                        </ul>

                        <div class="tab-content p-5" id="myTabContentMD">
                            <div class="tab-pane fade show active" id="default-md" role="tabpanel" aria-labelledby="default-tab-md">
                                <div class="col-12 mt-5 mt-5 ml-auto mr-auto">
                                    {!! Form::token() !!}
                                    <div class="form-group row {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <div class="col-2"> {!! Form::label('Ime kategorije') !!} </div>
                                        <div class="col-10"> {!! Form::text('name', old('name'), ['class'=>'form-control', 'name'=>'name', 'placeholder'=>'Unesite ime kategorije']) !!} </div>
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>

                                <hr class="delimiter">

                                    <div class="form-group row {{ $errors->has('status') ? 'has-error' : '' }}">
                                        <div class="col-2">
                                            {!! Form::label('Status') !!}
                                        </div>
                                        <div class="col-3">
                                            {!! Form::select('status', ['1' => 'Enabled', '0' => 'Disabled'], 0, ['class'=>'form-control alert-success', 'name'=>'status', 'id'=>'status']) !!}
                                        </div>
                                        <span class="text-danger">{{ $errors->first('status') }}</span>
                                    </div>
                                    <div class="form-group row {{ $errors->has('desc') ? 'has-error' : '' }}">
                                        <div class="col-2">
                                            {!! Form::label('Opis') !!}
                                        </div>
                                        <div class="col-10">
                                            {!! Form::textarea('desc', old('desc'), ['class'=>'form-control', 'name'=>'desc', 'id'=>'desc', 'placeholder'=>'Opis kategorije', 'rows'=>'3']) !!}
                                        </div>
                                        <span class="text-danger">{{ $errors->first('desc') }}</span>
                                    </div>
                                </div>
                            </div>

                            {{-- Design Configuration --}}
                            <div class="tab-pane fade" id="design-md" role="tabpanel" aria-labelledby="design-tab-md">

                                <hr class="delimiter">

                                <div class="input-group hdtuto control-group lst increment" >
                                    <div class="col-5">
                                        {!! Form::label('Logo') !!}
                                    </div>
                                    <div class="col-6 first-image">
                                        @if($settings_configuration->getLogoSrc())
                                            <img src="{{ $settings_configuration->getLogoSrc() }}" width="250" />
                                            <hr class="delimiter">
                                        @endif
                                        {!! Form::file('images[]', ['type'=>'files', 'accept' => 'image/*', 'onchange' => 'preview_image(event)']) !!}
                                        <img id="output_image" class="output_image" height="50" />
                                    </div>
                                </div>

                                <script>
                                    function preview_image(event)
                                    {
                                        var reader = new FileReader();
                                        reader.onload = function()
                                        {
                                            //var output = document.getElementById('output_image');
                                            //console.log(event.target.parentNode.querySelector('.output_image'));
                                            var output = event.target.parentNode.querySelector('.output_image');
                                            output.src = reader.result;
                                        }
                                        reader.readAsDataURL(event.target.files[0]);
                                    }
                                </script>


                            <hr class="delimiter">

                            <div class="form-group row {{ $errors->has('stores/settings/configuration/design/logo/max-width') ? 'has-error' : '' }}">
                                <div class="col-2"> {!! Form::label('Max sirina logoa') !!} </div>
                                <div class="col-10">
                                    {!! Form::text('stores/settings/configuration/design/logo/max-width', old('stores/settings/configuration/design/logo/max-width', $configuration->getLogoWidth()), ['class'=>'form-control', 'name'=>'stores/settings/configuration/design/logo/max-width', 'placeholder'=>'Unesite maximalnu sirinu']) !!}
                                    <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "auto"</span>
                                </div>
                                <span class="text-danger">{{ $errors->first('stores/settings/configuration/design/logo/max-width') }}</span>
                            </div>

                            <div class="form-group row {{ $errors->has('stores/settings/configuration/design/logo/max-height') ? 'has-error' : '' }}">
                                <div class="col-2"> {!! Form::label('Max visina logoa') !!} </div>
                                <div class="col-10">
                                    {!! Form::text('stores/settings/configuration/design/logo/max-height', old('stores/settings/configuration/design/logo/max-height', $configuration->getLogoHeight()), ['class'=>'form-control', 'name'=>'stores/settings/configuration/design/logo/max-height', 'placeholder'=>'Unesite maximalnu visinu']) !!}
                                    <span class="text-muted">Ako se ne unese vrednost, vrednost je automatki podeseno na "auto"</span>
                                </div>
                                <span class="text-danger">{{ $errors->first('stores/settings/configuration/design/logo/max-height') }}</span>
                            </div>
                            </div>

                            {{-- Shop Configuration --}}
                            <div class="tab-pane fade" id="shop-md" role="tabpanel" aria-labelledby="shop-tab-md">
                            @include('themes.default.admin.stores.settings.configuration.shop-form')
                            </div>

                            {{-- Product Configuration --}}
                            <div class="tab-pane fade" id="product-md" role="tabpanel" aria-labelledby="product-tab-md">
                                @include('themes.default.admin.stores.settings.configuration.product-form')
                            </div>

                            {{-- Catalog Configuration --}}
                            <div class="tab-pane fade" id="catalog-md" role="tabpanel" aria-labelledby="catalog-tab-md">
                                @include('themes.default.admin.stores.settings.configuration.catalog-form')
                            </div>

                            {{-- Slider Configuration --}}
                            <div class="tab-pane fade" id="slider-md" role="tabpanel" aria-labelledby="slider-tab-md">
                                @include('themes.default.admin.stores.settings.configuration.slider-form')
                            </div>

                            {{-- Account Configuration --}}
                            <div class="tab-pane fade" id="account-md" role="tabpanel" aria-labelledby="account-tab-md">
                                @include('themes.default.admin.stores.settings.configuration.account-form')
                            </div>
                            {{-- Maintenance Configuration --}}
                            <div class="tab-pane fade" id="maintenance-md" role="tabpanel" aria-labelledby="maintenance-tab-md">
                                @include('themes.default.admin.stores.settings.configuration.maintenance-form')
                            </div>
                        </div>
                    </section>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
