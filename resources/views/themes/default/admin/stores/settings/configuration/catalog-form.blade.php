
<div class="form-group row {{ $errors->has('catalog/layout') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Catalog Layout') !!}
    </div>
    <div class="col-8">
        {!! Form::select('catalog/layout',
        [
            '1-column-full-width-page' => '1-column-full-width-page',
            '1-column-full-width' => '1-column-full-width',
            '1-column' => '1-column',
            '2-column-left' => '2-column-left',
            '2-column-right-blog' => '2-column-right-blog',
            '2-column-right' => '2-column-right',
            '2-column-right-full-width' => '2-column-right-full-width',
        ],
        $settings_configuration->getConfigValue('catalog/layout'),
        ['class'=>'form-control alert-success', 'name'=>'catalog/layout', 'id'=>'product-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('catalog/layout') }}</span>
</div>

<hr class="delimiter">

<div class="form-group row {{ $errors->has('product/list/mode') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Product list style') !!}
    </div>
    <div class="col-8">
        {!! Form::select('product/list/mode',
        ['list' => 'list', 'grid' => 'grid'],
        $settings_configuration->getConfigValue('product/list/mode'),
        ['class'=>'form-control alert-success', 'name'=>'product/list/mode', 'id'=>'product-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('product/list/mode') }}</span>
</div>

<div class="form-group row {{ $errors->has('product/prod_per_row') ? 'has-error' : '' }}">

    <div class="col-4 text-right">
        {!! Form::label('Products per row') !!}
    </div>
    <div class="col-8">
        {!! Form::select('product/prod_per_row',
        ['3' => '3', '4' => '4', '5' => '5', '6' => '6'],
        $settings_configuration->getConfigValue('product/prod_per_row'),
        ['class'=>'form-control alert-success', 'name'=>'product/prod_per_row', 'id'=>'product-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('product/prod_per_row') }}</span>
</div>

<div class="form-group row {{ $errors->has('product/prod_per_page') ? 'has-error' : '' }}">

    <div class="col-4 text-right">
        {!! Form::label('Products per page') !!}
    </div>
    <div class="col-8">
        {!! Form::select('product/prod_per_page',
        [
            '3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9',
            '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14',
            '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19',
            '20' => '20','21' => '21','22' => '22','23' => '23','24' => '24'
        ],
        $settings_configuration->getConfigValue('product/prod_per_page'),
        ['class'=>'form-control alert-success', 'name'=>'product/prod_per_page', 'id'=>'product-grid-per-page']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('product/prod_per_page') }}</span>
</div>

<hr class="delimiter">

<div class="form-group row {{ $errors->has('catalog/product/show_prod_stock_status') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Show product in stock status') !!}
    </div>
    <div class="col-8">
        {!! Form::select('catalog/product/show_prod_stock_status',
        ['0' => 'Hide', '1' => 'Show'],
        $settings_configuration->getConfigValue('catalog/product/show_prod_stock_status'),
        ['class'=>'form-control alert-success', 'name'=>'catalog/product/show_prod_stock_status', 'id'=>'product-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('catalog/product/show_prod_stock_status') }}</span>
</div>


<div class="form-group row {{ $errors->has('catalog/product/show_prod_sku') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Show Product SKU') !!}
    </div>
    <div class="col-8">
        {!! Form::select('catalog/product/show_prod_sku',
        ['0' => 'Hide', '1' => 'Show'],
        $settings_configuration->getConfigValue('catalog/product/show_prod_sku'),
        ['class'=>'form-control alert-success', 'name'=>'catalog/product/show_prod_sku', 'id'=>'product-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('catalog/product/show_prod_sku') }}</span>
</div>

<div class="form-group row {{ $errors->has('catalog/product/show_prod_price') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Show product price') !!}
    </div>
    <div class="col-8">
        {!! Form::select('catalog/product/show_prod_price',
        ['0' => 'Hide', '1' => 'Show'],
        $settings_configuration->getConfigValue('catalog/product/show_prod_price'),
        ['class'=>'form-control alert-success', 'name'=>'catalog/product/show_prod_price', 'id'=>'product-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('catalog/product/show_prod_price') }}</span>
</div>

<div class="form-group row {{ $errors->has('catalog/product/show_prod_cat') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Show product categories') !!}
    </div>
    <div class="col-8">
        {!! Form::select('catalog/product/show_prod_cat',
        ['0' => 'Hide', '1' => 'Show'],
        $settings_configuration->getConfigValue('catalog/product/show_prod_cat'),
        ['class'=>'form-control alert-success', 'name'=>'catalog/product/show_prod_cat', 'id'=>'product-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('catalog/product/show_prod_cat') }}</span>
</div>

<div class="form-group row {{ $errors->has('catalog/product/show_prod_desc') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Show product desc') !!}
    </div>
    <div class="col-8">
        {!! Form::select('catalog/product/show_prod_desc',
        ['0' => 'Hide', '1' => 'Show'],
        $settings_configuration->getConfigValue('catalog/product/show_prod_desc'),
        ['class'=>'form-control alert-success', 'name'=>'catalog/product/show_prod_desc', 'id'=>'product-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('catalog/product/show_prod_desc') }}</span>
</div>

<hr class="delimiter">

<h3>Mobile Configuration</h3>

<hr class="delimter">

<div class="form-group row {{ $errors->has('product/mobile/list/mode') ? 'has-error' : '' }}">
    <div class="col-4 text-right">
        {!! Form::label('Product list style') !!}
    </div>
    <div class="col-8">
        {!! Form::select('product/mobile/list/mode',
        ['list' => 'list', 'grid' => 'grid'],
        $settings_configuration->getConfigValue('product/mobile/list/mode'),
        ['class'=>'form-control alert-success', 'name'=>'product/mobile/list/mode', 'id'=>'product-mobile-list-mode']) !!}
    </div>
    <span class="text-danger">{{ $errors->first('product/mobile/list/mode') }}</span>
</div>
