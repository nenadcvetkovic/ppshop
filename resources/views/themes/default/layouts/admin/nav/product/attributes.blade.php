@if(isset(Route::getCurrentRoute()->parameters['method']) && Route::getCurrentRoute()->parameters['method'] == "create")
<li class="nav-item active">
    <a class="nav-link" href="{{ url('admin/settings/product/attributes') }}">Home</a>
</li>
@else
<li class="nav-item active">
    <a class="nav-link" href="{{ url('admin/settings/product/attributes/create') }}">Add new</a>
</li>
@endif
