{{-- {{ dd(Route::getCurrentRoute()->parameters) }} --}}
@if(isset(Route::getCurrentRoute()->parameters['route']) && Route::getCurrentRoute()->parameters['route'] == "create")
<li class="nav-item active">
    <a class="nav-link" href="{{ route('catalog', 'category') }}">Home</a>
</li>
@else
<li class="nav-item active">
    <a class="nav-link" href="{{ route('catalog', 'category/create') }}">Add new</a>
</li>
@endif
