<div class="form-message-box col-12 mt-1">
    @if($errors->has($errName))
    <div class="alert alert-danger p-1">
        {{ $errors->first($errName) }}
    </div>
    @endif
</div>
