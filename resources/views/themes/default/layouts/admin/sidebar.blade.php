<sidebar id="sidebar" class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            {{--  <a href="{{ url('/') }}" class="simple-text logo-mini">
                {{ config('app.name', 'PPShop Admin') }}
            </a>
            <a href="{{ url('/') }}" class="simple-text logo-normal">
                {{ config('app.name', 'PPShop Admin') }}
            </a>  --}}
            <a href="{{ url('/') }}" class="simple-text logo-mini text-center" style="display: block;">
                <img src="{{ $configuration->getLogoSrc() }}" alt="{{ config('app.name', 'PPShop Admin') }}" style="width: 60%; margin: 20px auto;">
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item level0 active">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            {{--  Catalog  --}}
            <li class="nav-item level0">
                <a class="nav-link" data-toggle="collapse" href="#catalogItems">
                  <p>Catalog</p>
                </a>
                <div class="collapse card m-1" id="catalogItems">
                    <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('catalog', 'category') }}">
                                <span class="sidebar-mini">CA</span>
                                <span class="sidebar-normal">Category</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('catalog', 'product') }}">
                                <span class="sidebar-mini">PR</span>
                                <span class="sidebar-normal">Products</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            {{--  Content  --}}
            <li class="nav-item level0">
                <a class="nav-link" data-toggle="collapse" href="#contentItems">
                  <p>Content</p>
                </a>
                <div class="collapse card m-1" id="contentItems">
                    <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('content.pages.index') }}">
                                <span class="sidebar-mini">CMS</span>
                                <span class="sidebar-normal">Pages</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('content.block.index') }}">
                                <span class="sidebar-mini">CMS</span>
                                <span class="sidebar-normal">Blocks</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('content', 'design') }}">
                                <span class="sidebar-mini">BL</span>
                                <span class="sidebar-normal">Design</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('content', 'menu') }}">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal">Menu</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('content', 'slider') }}">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal">Slider</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('content', 'brands') }}">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal">Brands</span>
                            </a>
                        </li>
                        <li class="nav-item level2">
                            <a class="nav-link" data-toggle="collapse" href="#settings">
                              <p>Blog</p>
                            </a>
                            <div class="collapse card m-1" id="settings">
                                <ul class="nav">
                                    <li class="nav-item ">
                                        <a class="nav-link" href="{{ route('themes.default.admin.content.blog.index') }}">
                                            <span class="sidebar-mini"></span>
                                            <span class="sidebar-normal">All blogs</span>
                                        </a>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link" href="{{ route('themes.default.admin.content.blog.create') }}">
                                            <span class="sidebar-mini"></span>
                                            <span class="sidebar-normal">New Blog Post</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item level0">
              <a class="nav-link" data-toggle="collapse" href="#formsSettings">
                <p>Stores</p>
              </a>
              <div class="collapse card m-1" id="formsSettings">
                  <ul class="nav">
                      {{-- <li class="nav-item ">
                          <a class="nav-link" href="{{ route('settings', '1') }}">
                              <span class="sidebar-mini">Options</span>
                              <span class="sidebar-normal">1</span>
                          </a>
                      </li>
                      <li class="nav-item ">
                          <a class="nav-link" href="{{ route('settings', '2') }}">
                              <span class="sidebar-mini">Ef</span>
                              <span class="sidebar-normal">Extended Forms</span>
                          </a>
                      </li>
                      <li class="nav-item ">
                          <a class="nav-link" href="{{ route('settings', '3') }}">
                              <span class="sidebar-mini">Vf</span>
                              <span class="sidebar-normal">Validation Forms</span>
                          </a>
                      </li>
                      <li class="nav-item ">
                          <a class="nav-link" href="{{ route('settings', '4') }}">
                              <span class="sidebar-mini">W</span>
                              <span class="sidebar-normal">Wizard</span>
                          </a>
                      </li> --}}
                      <li class="nav-item level1">
                        <a class="nav-link" data-toggle="collapse" href="#settings">
                          <p>Settings</p>
                        </a>
                        <div class="collapse card m-1" id="settings">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="{{ route('store.settings', 'general') }}">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">General</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="{{ route('store.settings', 'configuration') }}">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Configuration</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item level2">
                        <a class="nav-link" data-toggle="collapse" href="#attributes">
                          <p>Attributes</p>
                        </a>
                        <div class="collapse card m-1" id="attributes">
                            <ul class="nav">
                                <li class="nav-item ">
                                    <a class="nav-link" href="{{ route('store.attributes', 'product') }}">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Product</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="{{ route('store.attributes', 'attribute-set') }}">
                                        <span class="sidebar-mini"></span>
                                        <span class="sidebar-normal">Attribute Set</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                  </ul>
              </div>
          </li>
            {{--  Content  --}}
            <li class="nav-item level0">
                <a class="nav-link" data-toggle="collapse" href="#systemItems">
                    <p>System</p>
                </a>
                <div class="collapse card m-1" id="systemItems">
                    <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link" href="{{ route('system.tools.cache') }}">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal">Cache Management</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
      </div>
      <ul class="nav">
        <li class="nav-item level0 active">
            <a class="nav-link" href="{{ url('admin/laravel-filemanager') }}" target="_blank">
                <i class="nc-icon nc-chart-pie-35"></i>
                <p>File Manager</p>
            </a>
        </li>
    </ul>
</sidebar>

