@extends('themes.default.layouts.admin.index')

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 p-0">
                    <div class="col-12 alert-danger p-3">
                        <h1>404</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
