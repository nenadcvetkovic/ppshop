@include('themes.default.layouts.admin.header')

@include('themes.default.layouts.admin.sidebar')

<main class="main-content">
    @include('themes.default.layouts.admin.top-nav')

    @yield('main-content')
</main>

@include('themes.default.layouts.admin.footer')
