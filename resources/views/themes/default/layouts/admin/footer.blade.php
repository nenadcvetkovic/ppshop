
@yield('before-footer')

{{-- <script src="{{  url('plugins/dropzone/dropzone.min.js') }}"></script> --}}

<!-- Javascript: jQuery, Popper and Bootstrap -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
{{--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.js"></script>

<!-- Scripts -->
<script src="{{ asset('js/admin/app.js') }}" defer></script>

{{--  <script src="~/node_modules/trumbowyg/plugins/trumbowyg.upload.js" defer></script>  --}}
<script>

    $( document ).ready(function() {
        $('.colorpicker').colorpicker();
        $('.colorpicker').change(function () {
            var color = $(this).val();
            $(this).css( "background-color", color );
            $(this).css( "color", '#fff' );
        });
        $(function() {
            $('#cp2').colorpicker();
        });

        function preview_image(event)
        {
            var reader = new FileReader();
            reader.onload = function()
            {
                //var output = document.getElementById('output_image');
                //console.log(event.target.parentNode.querySelector('.output_image'));
                var output = event.target.parentNode.querySelector('.output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
        $('#output_image').change(function() {
            alert();
        });
    });
</script>

</body>
</html>
