@include('themes.default.layouts.frontend.header')

{{--  @include('themes.default.layouts.frontend.sidebar')  --}}

<main id="app" class="main-content">
        @yield('main-content')
    {{--  @include('layouts.site.top-nav')  --}}
</main>

@include('themes.default.layouts.frontend.footer')
