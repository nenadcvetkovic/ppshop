@include('themes.default.layouts.frontend.header')

@include('themes.default.front.site-header')

<div id="app" class="container-full">
    <div class="col-12">
        @include('themes.default.layouts.frontend.page-header')
    </div>

    <main class="main-content">
        @yield('content')
    </main>
    <div>
        {{--  <pp-slider/>  --}}
    </div>
</div>

{{--  @include('themes.default.front.sections.subscribe')  --}}

@include('themes.default.front.site-footer')

@include('themes.default.layouts.frontend.footer')
