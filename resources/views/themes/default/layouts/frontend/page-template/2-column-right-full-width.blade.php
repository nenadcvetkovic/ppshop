@include('themes.default.layouts.frontend.header')

@include('themes.default.front.site-header')

@include('themes.default.front.sections.services')

<div class="container-fluid pl-15 pr-15">
    @yield('top-page-content')
</div>

<div class="container-fluid">
    <div class="col-12">
    <div class="row">

            <div class="col-12">
                @include('themes.default.layouts.frontend.page-header')
            </div>

            <main class="main-content col-sm-9 col-md-9 col-lg-9 col-xs-12">
                @yield('content')
            </main>

            <div class="col-3 d-none d-sm-block">
                @include('themes.default.layouts.frontend.right-sidebar')
            </div>
        </div>
    </div>
</div>

{{-- @include('themes.default.front.sections.contact-us') --}}

{{--  @include('themes.default.front.sections.subscribe')  --}}

@include('themes.default.front.site-footer')

@include('themes.default.layouts.frontend.footer')
