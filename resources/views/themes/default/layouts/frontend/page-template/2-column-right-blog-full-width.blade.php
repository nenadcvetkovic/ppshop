@include('themes.default.layouts.frontend.header')

@include('themes.default.front.site-header')

@include('themes.default.front.sections.services')

<div class="container-fluid pl-15 pr-15"> {{--  old pl-15 pr-15 --}}
    @yield('top-page-content')
</div>

<div id="app">
    <div class="container-fluid">
        <div class="col-12">
            <div class="row pt-3 pb-3">

                <div class="col-12">
                    @include('themes.default.layouts.frontend.page-header')
                </div>

                <main class="main-content col-lg-9 col-md-12 col-sm-12 col-xs-12">
                    @yield('content')
                </main>

                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 d-none d-sm-block">
{{--                    @include('themes.default.front.blog.sidebar')--}}
                    @include('themes.default.layouts.frontend.right-sidebar-blog')
                </div>
            </div>
        </div>
    </div>

    @include('themes.default.front.sections.contact-us')

    {{--  @include('themes.default.front.sections.subscribe')  --}}
</div>


@include('themes.default.front.site-footer')

@include('themes.default.layouts.frontend.footer')
