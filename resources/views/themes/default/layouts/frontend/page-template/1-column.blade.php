@include('themes.default.layouts.frontend.header')

@include('themes.default.front.site-header')

@include('themes.default.front.sections.services')

<div class="container pl-15 pr-15">
    @yield('top-page-content')
</div>

<div class="container">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                @include('themes.default.layouts.frontend.page-header')
            </div>

            <main class="main-content col-12">
                @yield('content')
            </main>
        </div>
    </div>
</div>

{{--  @include('themes.default.front.sections.subscribe')  --}}

@include('themes.default.front.site-footer')

@include('themes.default.layouts.frontend.footer')
