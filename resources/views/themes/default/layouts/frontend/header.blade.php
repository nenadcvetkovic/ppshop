<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7"><![endif]-->
<!--[if IE 8]><html class="ie ie8"><![endif]-->
<!--[if IE 9]><html class="ie ie9"><![endif]-->
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;" />

    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="{{ url('storage/media/images/logo.png') }}" rel="icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">

    @php
        $head = $config->getHeadData(request());
    @endphp

    @if ($head)
        @foreach ($head['meta'] as $key=>$value)
            @if($value == '')
                <meta property="og:{{ $key }}" name="{{ $key }}" content="{{ env('APP_'.strtoupper($key)) }}">
            @else
                <meta property="og:{{ $key }}" name="{{ $key }}" content="{{ $value }}">
            @endif
        @endforeach
    @endif
    <meta property="og:url"           content="https://www.your-domain.com/your-page.html" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Your Website Title" />
    <meta property="og:description"   content="Your description" />
    <meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- {{ dd(request()->route()->parameters) }} --}}
    <title>{{ @$config->getTitle(request()) }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Archivo+Narrow:300,400,700%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/ps-icon/style.css') }}">
    <!-- Font Awesome icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    {{--  <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>  --}}

    <!-- CSS Library-->
    <link rel="stylesheet" href="{{ url('plugins/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/owl-carousel/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/slick/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/Magnific-Popup/dist/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/revolution/css/settings.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/revolution/css/layers.css') }}">
    {{--  <link rel="stylesheet" href="{{ url('plugins/revolution/css/navigation.css') }}">  --}}
    <link href="https://mreq.github.io/slick-lightbox/dist/slick-lightbox.css" rel="stylesheet" />

    <link rel="stylesheet" href="{{ url('plugins/lightslider/css/lightslider.css') }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--WARNING: Respond.js doesnt work if you view the page via file://-->
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->

    @if ($configuration->getHeadScripts())
        <script>
            {!! $configuration->getHeadScripts() !!}
        </script>
    @endif

    @yield('head')

    <link href="{{ url('css/admin_style.css') }}" rel="stylesheet">

</head>
<!--[if IE 7]><body class="ie7 lt-ie8 lt-ie9 lt-ie10"><![endif]-->
<!--[if IE 8]><body class="ie8 lt-ie9 lt-ie10"><![endif]-->
<!--[if IE 9]><body class="ie9 lt-ie10"><![endif]-->
<body class="{{ $configuration->showLoader() ? 'ps-loading' : 'ps-loading-off' }}" id="page-{{ $page ?? 'none' }}">
    @if ($configuration->getTopBodyScripts())
        <div>
            {!! $configuration->getTopBodyScripts() !!}
        </div>
    @endif
    <div class="site">
