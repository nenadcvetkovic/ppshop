<div class="ps-product-action">
    <div class="ps-product__filter d-none">
        <select class="ps-select selectpicker">
            <option value="1">Sort by</option>
            <option value="2">Name</option>
            <option value="3">Price (Low to High)</option>
            <option value="3">Price (High to Low)</option>
        </select>
    </div>
    @if ($allProducts)
    <div class="ps-pagination">
        {{ $allProducts->links('themes.default.layouts.frontend.parts.pagination') }}
    </div>
    @endif
</div>
