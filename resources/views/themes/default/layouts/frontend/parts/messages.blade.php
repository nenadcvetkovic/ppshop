<div class="message-box">
    @if(Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
    @endif

    @if(Session::has('error'))
    <div class="alert alert-danger">
        {{ Session::get('error') }}
    </div>
    @endif

    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->getMessages() as $this_error)
                <p style="color: red;">{{$this_error[0]}}</p>
            @endforeach
        </div>
    @endif
</div>
