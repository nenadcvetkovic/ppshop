@extends('themes.default.layouts.frontend.index')

@section('main-content')

    @include('themes.default.front.site-header')

    <main class="ps-main">

        <div class="container mb-5">
            <div class="row">
                <div class="col-12 text-center">
                    <h1>404 - Page not found</h1>
                    <h2>Uh oh...</h2>
                    <h4>The page You requested could not be found.</h4>
                    <hr class="delimiter">
                    <h4 class="mt-2">Back to <a href="{{ route('home') }}">Home</a> page</h4>
                </div>
            </div>
        </div>

        @include('themes.default.front.site-footer')

    </main>
@endsection
