</div> <!-- #app -->
@yield('before-footer')

<script type="text/javascript" src="{{ url('js/app.js') }}"></script>

<!-- JS Library-->
<script type="text/javascript" src="{{ url('plugins/jquery/dist/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>

<script type="text/javascript" src="{{ url('plugins/owl-carousel/owl.carousel.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>


<script type="text/javascript" src="{{ url('plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/elevatezoom/jquery.elevatezoom.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/Magnific-Popup/dist/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ url('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
{{--  <script type="text/javascript" src="{{ url('plugins/lightbox/js/lightbox.js') }}"></script>  --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>--}}


  <script type="text/javascript" src="{{ url('plugins/slick/slick/slick.min.js') }}"></script>
{{--  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>--}}
{{--  <script type="text/javascript" src="{{ url('plugins/lightbox/js/lightbox.js') }}"></script>  --}}

<script src="https://mreq.github.io/slick-lightbox/dist/slick-lightbox.js"></script>

<script src="{{ url('plugins/lightslider/js/lightslider.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbWidth:200,
            thumbItem:4,
            thumbMargin:4,
            slideMargin:0,
            enableDrag: false,
            currentPagerPosition:'left',
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#imageGallery .lslide'
                });
            }
        });
        $('.open-menu').on('click', function() {
            console.log('test');
            $(this).parent().find('.sub-menu').toggle();
        });
        $(document).scroll(function() {
            if ($(this).scrollTop() > 150) {
                $('.sticky-top').addClass('short-sticky');
            } else {
                $('.sticky-top').removeClass('short-sticky');
            }
        });
        // $('.small').click(function(){
        //     $('.small').toggleClass('full');
        //     $('body').toggleClass('dark')
        // });
    });
    if ($('#map').length) {
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 43.308872, lng: 21.904543},
                zoom: 15,
    //            scrollwheel: false,
                styles: [{
                    "featureType": "road", "stylers": [{"hue": "#5e00ff"}, {"saturation": -79}]
                }, {
                    "featureType": "poi",
                    "stylers": [{"saturation": -78}, {"hue": "#6600ff"}, {"lightness": -47}, {"visibility": "off"}]
                }, {
                    "featureType": "road.local", "stylers": [{"lightness": 22}]
                }, {
                    "featureType": "landscape",
                    "stylers": [{"hue": "#6600ff"}, {"saturation": -11}]
                }, {}, {}, {
                    "featureType": "water", "stylers": [{"saturation": -65}, {"hue": "#1900ff"}, {"lightness": 8}]
                }, {
                    "featureType": "road.local",
                    "stylers": [{"weight": 1.3}, {"lightness": 30}]
                }, {
                    "featureType": "transit", "stylers": [{
                        "visibility": "simplified"
                    }, {"hue": "#5e00ff"}, {"saturation": -16}]
                }, {
                    "featureType": "transit.line", "stylers": [{"saturation": -72}]
                }, {}
                ]
            });
            var marker = new google.maps.Marker({
                position: {lat: 43.308872, lng: 21.904543},
                map: map,
                animation: google.maps.Animation.DROP,
                icon: "public/storage/media/images/icons/placeholder.png"
            });
        }

    }

</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPZ-TPRAw4Did3-XOe-7mnBBm0hsNa-GA&callback=initMap">
</script>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- Custom scripts-->
<script type="text/javascript" src="{{ url('js/main.js') }}"></script>

@if ($configuration->getBottomBodyScripts())
<div>
    {!! $configuration->getBottomBodyScripts() !!}
</div>
@endif
</body>
</html>
