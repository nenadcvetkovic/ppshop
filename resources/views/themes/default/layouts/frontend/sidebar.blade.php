
<div>
    <pp-sidebar :route="'{{ isset(request()->route()->parameters['identifier']) ? request()->route()->parameters['identifier'] : 'none' }}'"/>
</div>

<div class="pt-10 pb-10">
    @block('left-sidebar')
</div>
