{{--  {{ dd($pageContent) }}  --}}
@php
    $layout = $pageContent->layout ? $pageContent->layout : 'index'
@endphp
@extends('themes.default.layouts.frontend.page-template.'.$layout)

@section('top-page-content')
    @include('themes.default.front.sections.page.header')
@endsection

@section('main-content')

    @include('themes.default.front.site-header')
<div id="app">
    <main class="ps-main">
        <div class="container-fluid pl-0 pr-0">
            @yield('top-page-content')
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @section('content')
                        @include('themes.default.layouts.frontend.parts.messages')
                        {{--  <pp-app page="'homepage'" content="{{ $content }}"/>  --}}
                        {!! $content !!}
                    @endsection
                </div>
            </div>
        </div>

        @include('themes.default.front.site-footer')

    </main>
</div>
@endsection
