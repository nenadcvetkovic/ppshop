@extends('themes.default.layouts.frontend.index')

@section('main-content')

    @include('themes.default.front.site-header')

    <main class="ps-main">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('themes.default.layouts.admin.message')

                    <p>{!! $content !!}</p>
                </div>
            </div>
        </div>

        @include('themes.default.front.site-footer')

    </main>
@endsection

{{--  {{ dd(\App\Product::all()) }}  --}}

