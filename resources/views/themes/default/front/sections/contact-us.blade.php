<br/>
<div class="container-test ps-home-contact d-none">
    <div id="contact-map" data-address="New York, NY" data-title="BAKERY LOCATION!" data-zoom="17"></div>
    <div class="ps-home-contact__form">
        <header>
        <h3>Contact Us</h3>
        <p>Learn about our company profile, communityimpact, sustainable motivation, and more.</p>
        </header>
        <footer>
        <form action="product-listing.html" method="post">
            <div class="form-group">
            <label>Name<span>*</span></label>
            <input class="form-control" type="text">
            </div>
            <div class="form-group">
            <label>Email<span>*</span></label>
            <input class="form-control" type="email">
            </div>
            <div class="form-group">
            <label>Your message<span>*</span></label>
            <textarea class="form-control" rows="4"></textarea>
            </div>
            <div class="form-group text-center">
            <button class="ps-btn">Send Message<i class="fa fa-angle-right"></i></button>
            </div>
        </form>
        </footer>
    </div>
</div>

{{--
    <div id="map"></div>
    <style>
        #map {
          width: 100%;
          height: 400px;
          background-color: grey;
        }
       </style>
       <script>
                function initMap() {
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat: 43.308872, lng: 21.904543},
                        zoom: 15,
            //            scrollwheel: false,
                        styles: [{
                            "featureType": "road", "stylers": [{"hue": "#5e00ff"}, {"saturation": -79}]
                        }, {
                            "featureType": "poi",
                            "stylers": [{"saturation": -78}, {"hue": "#6600ff"}, {"lightness": -47}, {"visibility": "off"}]
                        }, {
                            "featureType": "road.local", "stylers": [{"lightness": 22}]
                        }, {
                            "featureType": "landscape",
                            "stylers": [{"hue": "#6600ff"}, {"saturation": -11}]
                        }, {}, {}, {
                            "featureType": "water", "stylers": [{"saturation": -65}, {"hue": "#1900ff"}, {"lightness": 8}]
                        }, {
                            "featureType": "road.local",
                            "stylers": [{"weight": 1.3}, {"lightness": 30}]
                        }, {
                            "featureType": "transit", "stylers": [{
                                "visibility": "simplified"
                            }, {"hue": "#5e00ff"}, {"saturation": -16}]
                        }, {
                            "featureType": "transit.line", "stylers": [{"saturation": -72}]
                        }, {}
                        ]
                    });
                    var marker = new google.maps.Marker({
                        position: {lat: 43.308872, lng: 21.904543},
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: "{{ Storage::url('public/media/images/icons/placeholder.png') }}"
                    });
                }
       </script>
       <script async defer
       src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPZ-TPRAw4Did3-XOe-7mnBBm0hsNa-GA&callback=initMap">
       </script>  --}}

    {{--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5805.909944756019!2d21.89261907575574!3d43.315202016537164!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4755b0b02058201f%3A0xbece72d66f572694!2zTWFyZ2VyLCBOacWh!5e0!3m2!1sen!2srs!4v1585122661592!5m2!1sen!2srs" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>  --}}
