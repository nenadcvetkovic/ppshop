<div>
    <h4 class="mb-2 text-white"><i class="fa fa-envelope"></i>Prijavite se na email</h4>
    <form class="subscribe__form" action="{{ route('subscribe.comfirm') }}" method="post">
        <input type="hidden" value="{{ csrf_token() }}" name="_token"/>
        <input name="email" class="form-control" type="email" placeholder="Email" required>
        <button class="mt-2 btn btn-light">Prijavi se</button>
    </form>
</div>
