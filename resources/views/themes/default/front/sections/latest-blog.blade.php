
<div class="ps-section ps-home-blog pt-20 pb-20">
    <div class="container p-0">
        <div class="ps-section__header mb-50 text-center">
            <h3 class="ps-section__title" data-mask="Novosti i objave">
                <span>
                    Novosti i objave
                </span>
            </h3>
            <div class="ps-section__action"><a class="ps-morelink text-uppercase" href="{{ route('blog.index') }}">{{ $t->translate('Vidite sve') }}<i class="fa fa-long-arrow-right"></i></a></div>
        </div>
        <div class="ps-section__content">
            <div class="row">
                @if ($latestPosts)
                    @foreach ($latestPosts as $post)
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 pp-post__box">
                            @include('themes.default.front.blog.blog-grid-post')
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
