@if ($configuration->showSiteAuth())
<div class="btn-group ps-dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">USD<i class="fa fa-angle-down"></i></a>
    <ul class="dropdown-menu">
        <li><a href="#"><img src="images/flag/usa.svg" alt=""> USD</a></li>
        <li><a href="#"><img src="images/flag/singapore.svg" alt=""> SGD</a></li>
        <li><a href="#"><img src="images/flag/japan.svg" alt=""> JPN</a></li>
    </ul>
</div>
@endif
