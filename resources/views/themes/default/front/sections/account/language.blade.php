@if ($configuration->showSiteLanguage())
<div class="btn-group ps-dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Language<i class="fa fa-angle-down"></i></a>
    <ul class="dropdown-menu">
        <li><a href="#">English</a></li>
        <li><a href="#">Japanese</a></li>
        <li><a href="#">Chinese</a></li>
    </ul>
</div>
@endif
