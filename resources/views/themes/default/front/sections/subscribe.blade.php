<div class="ps-subscribe">
    <div class="ps-container">
        <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 ">
                <h3><i class="fa fa-envelope"></i>Prijavite se na email</h3>
            </div>
            <div class="col-lg-5 col-md-7 col-sm-12 col-xs-12 ">
                <form class="ps-subscribe__form" action="{{ route('subscribe.comfirm') }}" method="post">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token"/>
                    <input name="email" class="form-control" type="email" placeholder="" required>
                    <button>Prijavi se</button>
                </form>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 ">
                <p>...i dobijte najnovije <span> vesti</span>  o akcijama.</p>
            </div>
        </div>
    </div>
</div>

{{--  <pp-subscribe/>  --}}