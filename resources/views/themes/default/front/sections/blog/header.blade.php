
@if (isset($post))
    {{--  {{ dd($post) }}  --}}
    <div class="bg--parallax pt-80 pb-40 mb-20"
        data-background="{{ asset('storage/'. (!isset($post) || $post->getPostTopImage() == '' ? 'media/images/catalog/category/category-hover.jpg' : $post->getPostTopImage())) }}"
        style="background: url('{{ asset('storage/'. (!isset($post) || $post->getPostTopImage() == '' ? 'media/images/catalog/category/category-hover.jpg' : $post->getPostTopImage())) }}') 50% 10px;"
    >

    <div class="container-fluid">
        <div class="ps-section__header mb-50">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    @if ($post->showPostTitle())
                        <h3 class="ps-section__title category-page single-blog-page" data-mask="{{ $post->getPostTitle()}}">
                            {{--  - {{ request()->route()->identifier }}  --}}
                        </h3>
                    @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

@else

<div class="container">
    <div class="bg--parallax pt-80 pb-80 mb-20 d-none d-sm-block"
        data-background="{{ asset('storage/media/images/catalog/category/category-hover.jpg') }}"
        style="background: url('{{ asset('storage/media/images/catalog/category/category-hover.jpg') }}') 50% 10px;"
    >

    <div class="container">
        <div class="ps-section__header">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <h3 class="ps-section__title category-page blog-page" data-mask="Blog">
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endif

