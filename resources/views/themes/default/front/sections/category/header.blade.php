
<div class="container">
    <div class="bg--parallax pt-80 pb-80 mb-20 d-none d-sm-block"
        data-background="{{ asset('storage/'. (!isset($cat) ? 'media/images/catalog/category/category-hover.jpg' : $cat->getCatImage())) }}"
        style="background: url('{{ asset('storage/'. (!isset($cat) ? 'media/images/catalog/category/category-hover.jpg' : $cat->getCatImage())) }}') 50% 10px;"
    >

    <div class="container">
        <div class="ps-section__header">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    @if (!isset($cat) ? true : $cat->showPageTitle())
                        <h3 class="ps-section__title category-page" data-mask="{{ !isset($cat) ? 'Svi proizvodi' : $cat->getSeoTitle() }}">
                            {{--  <span>
                                {{ request()->route()->identifier }}
                            </span>  --}}
                        </h3>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
