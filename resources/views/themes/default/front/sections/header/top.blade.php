<div class="header__top container-fluid">
    <div class="row">
        <div class="col">
            @block('top-header-info')
        </div>
        <div class="col header__actions">
            @include('themes.default.front.sections.account.authentication')

            @include('themes.default.front.sections.account.currency')

            @include('themes.default.front.sections.account.language')
        </div>
    </div>
</div>
