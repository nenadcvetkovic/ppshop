<div class="container">
    <div class="feature">
        @php
            $index = 1;
        @endphp
        @foreach (App\Category::where(['status' => 1, 'parent_id' => '0'])->get() as $cat)
            <div class="item item--{{ $index }}">
                <a href="{{ route('site.category', $cat->identifier) }}">
                    <span>{{ $cat->name }}</span>
                    <img src="http://ppshop.test/storage/media/images/wysiwyg/offers/dust-mask.jpg" alt="">
                </a>
            </div>
           @php
               $index++;
           @endphp
        @endforeach
    </div>
</div>
