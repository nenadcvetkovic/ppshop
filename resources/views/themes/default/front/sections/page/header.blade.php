{{--  {{ dd($pageContent) }}  --}}
@if (isset($pageContent))

    @if ($pageContent->showImage())
    <div class="bg--parallax pt-80 pb-40 mb-20" data-background="{{ asset('storage/'.$pageContent->getImage()) }}" style="background: url(&quot;{{ asset('storage/'.$pageContent->getImage()) }}&quot;) 50% 10px;">
    @endif

    <div class="container">
        <div class="ps-section__header mb-50">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
                    @if ($pageContent->showPageTitle())
                    <h3 class="ps-section__title category-page" data-mask="{{ $pageContent->getSeoTitle() }}">
                        <span>
                            {{ request()->route()->identifier }}
                        </span>
                    </h3>
                    @endif
                    </div>
                </div>
            </div>
        </div>

    @if ($pageContent->showImage())
    </div>
    @endif

@endif

