  <div class="container pt-20 pb-20">
    <h3 data-mask="Brendovi" class="ps-section__title mb-50">
        <span>
            Brendovi
        </span>
    </h3>
    @php
        $index = 1;
    @endphp
     <section id="slick_slider" class="customer-logos slider">
         @foreach ($allBrands as $brand)
            <div class="slide{{ $index == 1 ? ' slick-active' : '' }}">
                {{-- <a href="{{ url('catalog/brands/'.$brand->name) }}"> --}}
                   <img src="{{ asset('storage/'.$brand->image_path) }}">
                {{-- </a> --}}
            </div>
            @php
                $index++;
            @endphp
         @endforeach
         {{--  <div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg"></div>
         <div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg"></div>
         <div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg"></div>
         <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
         <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
         <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
         <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
         <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div>  --}}
     </section>
  </div>
