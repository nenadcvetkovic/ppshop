{{--  {{ dd($featured_products) }}  --}}
@php
$featured_products = App\Product::where('created_at', '>=', Carbon\Carbon::now()->subDays(30)->toDateTimeString())->get();
@endphp

@if (count($featured_products) > 0)
<div class="ps-section--features-product ps-section masonry-root pt-10 pb-10">
    <div class="ps-container">
      <div class="ps-section__header mb-50">
        <h3 class="ps-section__title" data-mask="Proizvodi">- Najnoviji proizvodi</h3>
        <ul class="ps-masonry__filter">
          <li class="current"><a href="#" data-filter="*">Najnoviji proizvodi <sup>{{ count($featured_products) }}</sup></a></li>
          {{--  <li><a href="#" data-filter=".test">Test <sup>1</sup></a></li>
          <li><a href="#" data-filter=".nike">Nike <sup>1</sup></a></li>
          <li><a href="#" data-filter=".adidas">Adidas <sup>1</sup></a></li>
          <li><a href="#" data-filter=".men">Men <sup>1</sup></a></li>
          <li><a href="#" data-filter=".women">Women <sup>1</sup></a></li>
          <li><a href="#" data-filter=".kids">Kids <sup>4</sup></a></li>  --}}
        </ul>
      </div>
      <div class="ps-section__content pb-50">
        <div class="masonry-wrapper" data-col-md="4" data-col-sm="2" data-col-xs="1" data-gap="30" data-radio="100%">
          <div class="ps-masonry">
            <div class="grid-sizer"></div>

            @foreach ($featured_products as $fprod)
                <div class="grid-item test">
                    <div class="grid-item__content-wrapper">
                      @include('themes.default.front.catalog.product.product-grid', ['prod' => $fprod])
                    </div>
                  </div>
                @endforeach

            {{--  <div class="grid-item kids">
              <div class="grid-item__content-wrapper">
                <div class="ps-shoe mb-30">
                  <div class="ps-shoe__thumbnail"><a class="ps-shoe__favorite" href="#"><i class="ps-icon-heart"></i></a><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""><a class="ps-shoe__overlay" href="{{ url('catalog/products/Test')  }}"></a>
                  </div>
                  <div class="ps-shoe__content">
                    <div class="ps-shoe__variants">
                      <div class="ps-shoe__variant normal"><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""></div>
                      <select class="ps-rating ps-shoe__rating">
                        <option value="1">1</option>
                        <option value="1">2</option>
                        <option value="1">3</option>
                        <option value="1">4</option>
                        <option value="2">5</option>
                      </select>
                    </div>
                    <div class="ps-shoe__detail"><a class="ps-shoe__name" href="#">Air Jordan 7 Retro</a>
                      <p class="ps-shoe__categories"><a href="#">Men shoes</a>,<a href="#"> Nike</a>,<a href="#"> Jordan</a></p><span class="ps-shoe__price"> £ 120</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>  --}}
          </div>
        </div>
      </div>
    </div>
  </div>
@endif
