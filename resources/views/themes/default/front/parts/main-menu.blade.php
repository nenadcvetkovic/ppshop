{{--  {{ dd($menuFilteredItems) }}  --}}

<ul class="main-menu menu">
    @foreach ($menuFilteredItems as $item)
        @if($item['type'] == 'custom' || $item['type'] == 'page')
        <li class="menu-item  menu-item-{{ $item['root']->id }} {{ isset($item['children']) && $item['children']->count() ? 'menu-item-has-children dropdown' : '' }}">
            <a href="{{ $item['root']->url ? url($item['root']->url) : '' }}">{{ $item['root']->name }}</a>
            @if (isset($item['children']) && $item['children']->count())
            <ul class="sub-menu">
              <li class="menu-item"><a href="index.html">Homepage #1</a></li>
              <li class="menu-item"><a href="#">Homepage #2</a></li>
              <li class="menu-item"><a href="#">Homepage #3</a></li>
            </ul>
            @endif
        </li>
        @endif

        @if($item['type'] == 'category')
            @if ($item['root']->design_type == 'full-width')
            <li class="menu-item menu-item-{{ $item['root']->id }} menu-item-has-children has-mega-menu">
                <a href="{{ route('site.category') }}">{{ $item['root']->name }}xvbxfvx</a>
                <div class="mega-menu">
                    <div class="mega-wrap">
                        {{--  {{ dd($filteredCategries) }}  --}}
                        @foreach ($filteredCategries as $rootCat)
                            @php
                            if (isset($rootCat['data']) && $rootCat['data']->status == 0) continue;
                            @endphp
                            <div class="mega-column">
                                <h4 class="mega-heading">
                                    <a href="{{ route('site.category', $rootCat['data']->identifier) }}">{{ $rootCat['data']->name }}</a>
                                </h4>
                                @if(isset($rootCat['children']))
                                    <ul class="mega-item">
                                        @foreach ($rootCat['children'] as $subCat)
                                            @php
                                            if ($subCat->status == 0) continue;
                                            @endphp
                                            <li>
                                                <a href="{{ route('site.category', $subCat->name) }}">{{ $subCat->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </li>
            @elseif ($item['root']->design_type == 'dropdown')
                {{--  {{ dd(isset($filteredCategries) && count($filteredCategries)) }}  --}}
                <li class="menu-item  menu-item-{{ $item['root']->id }} {{ isset($filteredCategries) && count($filteredCategries) ? 'menu-item-has-children dropdown' : '' }}">
                    <a href="{{ route('site.category') }}">{{ $item['root']->name }}</a>
                    <span class="open-menu"></span>
                    @if (isset($filteredCategries) && count($filteredCategries))
                    {{--  {{ dd($filteredCategries) }}  --}}
                        <ul class="sub-menu">
                            @foreach ($filteredCategries as $rootCat)
                                @php
                                if (isset($rootCat['data']) && $rootCat['data']->status == 0) continue;
                                @endphp
                                <li class="menu-item">
                                    <a href="{{ route('site.category', $rootCat['data']->identifier) }}">
                                        {{ $rootCat['data']->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endif
        @endif
    @endforeach
</ul>

