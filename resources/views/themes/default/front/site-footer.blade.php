<div class="ps-footer bg--cover" data-background="{{ asset('storage/media/images/background/parallax.jpg') }}">
    <div class="ps-footer__content pt-20 pb-10">
        <div class="container">
              <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @block('footer')
                    </div>
              </div>
        </div>
    </div>
    <div class="ps-footer__copyright">
        <div class="container">
              <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                      <p>&copy;
                          <a href="https://patrol.co.rs">PATROLPM</a>. Sva prava zadrzana.
                          <span class="d-none">Design by <a href="https://primerprogetto.com"> Primer Progetto</a></span>
                      </p>
                    </div>
            </div>
        </div>
    </div>
  </div>
</div> <!-- #app -->
{{--  @yield('before-footer')  --}}
</body>
</html>
