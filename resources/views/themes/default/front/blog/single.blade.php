@extends('themes.default.layouts.frontend.page-template.2-column-right-blog')
{{--  @extends('layouts.site.2-column-right-full-width')  --}}

@section('head')
    <meta property="og:title" content="{{ $post->getPostTitle() }}" />
    <meta property="og:image" content="{{ url('/storage/'.$post->getPostImage()) }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:url" content="{{ url('/blog/'.$post->identifier) }}" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="{{ $post->short_content }}" />
    <meta property="fb:app_id" content="112233" />
@endsection

@section('top-page-content')
    @include('themes.default.front.sections.blog.header')
@endsection

@section('content')
    <div class="container">
        <div class="form-group">
            <a href="{{ route('blog.index') }}" class="ps-btn ps-btn--sm ps-contact__submit"><i class="ps-icon-back"></i> Nazad</a>
        </div>
        {{--  {{ dd($identifier) }}  --}}

            <div class="ps-post--detail">
                <div class="ps-post__thumbnail"><img src="images/blog/11.png" alt=""></div>
                <div class="ps-post__header">
                <h3 class="ps-post__title">{{ $post->getPostTitle() }}</h3>
                <p class="ps-post__meta">Posted by <a href="blog-grid.html">{{ $post->getAuthorName($post->author) }}</a> on {{ $post->getPostUpdated() }}
                    {{-- in <a href="blog-grid.html">Men Shoes</a> , <a href="blog-grid.html">Stylish</a></p> --}}
                </div>
                <div class="ps-post__content">
                    {!! $post->content !!}
                </div>

                <div class="ps-post__footer d-none">
                    <p class="ps-post__tags"><i class="fa fa-tags"></i><a href="blog-list.html">Man shoe</a>,<a href="blog-list.html"> Woman</a>,<a href="blog-list.html"> Nike</a></p>
                    <div class="ps-post__actions"><span><i class="fa fa-comments"></i> 23 Comments</span><span><i class="fa fa-heart"></i>  likes</span>
                        <div class="ps-post__social"><i class="fa fa-share-alt"></i><a href="#">Share</a>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ps-author d-none">
                <div class="ps-author__thumbnail"><img src="images/user/1.jpg" alt=""></div>
                <div class="ps-author__content">
                <header>
                    <h4>MARK GREY</h4>
                    <p>WEB DESIGNER</p>
                </header>
                <p>The development of the mass spectrometer allowed the mass of atoms to be measured with increased accuracy. The device uses the launch and continued operation of the Hubble space telescope probably.</p>
                </div>
            </div>
            <div class="ps-comments d-none">
                <h3>Comment(4)</h3>
                <div class="ps-comment">
                <div class="ps-comment__thumbnail"><img src="images/user/2.jpg" alt=""></div>
                <div class="ps-comment__content">
                    <header>
                    <h4>MARK GREY <span>(15 minutes ago)</span></h4><a href="#">Reply<i class="ps-icon-arrow-left"></i></a>
                    </header>
                    <p>The development of the mass spectrometer allowed the mass of atoms to be measured with increased accuracy. The device uses the launch and continued operation of the Hubble space telescope probably.</p>
                </div>
                </div>
                <div class="ps-comment ps-comment--reply">
                <div class="ps-comment__thumbnail"><img src="images/user/3.jpg" alt=""></div>
                <div class="ps-comment__content">
                    <header>
                    <h4>MARK GREY <span>(3 hours ago)</span></h4><a href="#">Reply<i class="ps-icon-arrow-left"></i></a>
                    </header>
                    <p>The development of the mass spectrometer allowed the mass of atoms to be measured with increased accuracy. The device uses  continue ace explore.</p>
                </div>
                </div>
                <div class="ps-comment">
                <div class="ps-comment__thumbnail"><img src="images/user/4.jpg" alt=""></div>
                <div class="ps-comment__content">
                    <header>
                    <h4>MARK GREY <span>(1 day ago)</span></h4><a href="#">Reply<i class="ps-icon-arrow-left"></i></a>
                    </header>
                    <p>The development of the mass spectrometer allowed the mass of atoms to be measured with increased accuracy. The device uses the launch and continued operation of the Hubble space telescope probably.</p>
                </div>
                </div>
            </div>
            <form class="ps-form--comment d-none" action="do_action" method="post">
                <h3>LEAVE A COMMENT</h3>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        <div class="form-group">
                        <input class="form-control" type="text" placeholder="Your Name">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        <div class="form-group">
                        <input class="form-control" type="email" placeholder="E-mail">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        <div class="form-group">
                        <input class="form-control" type="text" placeholder="Subject">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        <div class="form-group">
                        <input class="form-control" type="text" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div class="form-group">
                        <textarea class="form-control" rows="6" placeholder="Text your message here..."></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                <button class="ps-btn ps-btn--sm ps-contact__submit">Send Message<i class="ps-icon-next"></i></button>
                </div>
            </form>
    </div>

@endsection


