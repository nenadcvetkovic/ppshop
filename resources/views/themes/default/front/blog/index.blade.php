@extends('themes.default.layouts.frontend.page-template.2-column-right-blog')
{{--  @extends('layouts.site.2-column-right-full-width')  --}}

@section('top-page-content')
    @include('themes.default.front.sections.blog.header')
@endsection

@section('content')

@foreach ($allBlogPosts as $post)
    @include('themes.default.front.blog.blog-grid')
@endforeach

<div class="clearfix"></div>

{{ $allBlogPosts->links('themes.default.layouts.frontend.parts.pagination') }}
@endsection
