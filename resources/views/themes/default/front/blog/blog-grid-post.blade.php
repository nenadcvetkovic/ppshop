<div class="ps-post pp-post mb-30">
    <div class="ps-post__thumbnail">
        <a class="ps-post__overlay" href="{{ url('blog', $post->identifier) }}"></a>
        <img src="{{ $post->image_path ? asset('storage/'.$post->image_path) : asset('storage/media/images/no-image.png') }}" alt="">
      </div>
    <div class="ps-post__content pp-post-content">
        <a class="ps-post__title" href="{{ url('blog', $post->identifier) }}">
          {{ $post->name }}
          </a>
      <p class="ps-post__meta">
          <span>By:<a class="mr-5" href="{{ route('blog.view.author', $post->getAuthor($post->author)) }}">{{ $post->getAuthorName($post->author) }}</a></span>
          -<span class="ml-5">{{ $post->getPostUpdated() }}</span>
      </p>
      <p> {{ $post->short_content }}</p>
      <a class="ps-morelink" href="{{ url('blog', $post->identifier) }}">Nastavi citanje<i class="fa fa-long-arrow-right"></i></a>
    </div>
</div>
