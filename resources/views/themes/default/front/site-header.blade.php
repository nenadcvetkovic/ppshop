<div class="navbar navbar-expand-md navbar-light bg-light mb-0 sticky-top" role="navigation">
    <div class="container">
        <a class="navbar-brand-logo col-9 col-sm-2 col-md-2 col-lg-2 col-xl-2 p-0" href="{{ url('/') }}">
            <img src="{{ $configuration->getLogoSrc() }}" alt="" style="max-width: {{ $configuration->getLogoWidth() }}; max-height: {{ $configuration->getLogoHeight() }};">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse col-12 col-sm-9 col-md-9 col-lg-10 col-xl-10 p-0" id="navbarCollapse">
            <div class="col-12 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                @php
                    echo \App\Http\Controllers\MenuController::returnMenuView();
                @endphp
            </div>
            <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3 p-0 m-0 m-3 m-sm-0   ">
                <form class="ps-search--header" action="{{ route('proizvod.pretraga') }}" method="get">
                    <input class="form-control" type="text" name="q" placeholder="Pretrazi">
                    <button><i class="ps-icon-search"></i></button>
                </form>
{{--                <br/>--}}
                <p class="pull-right social-links">
                    <a href="https://www.facebook.com/patrolpm" class="pl-2 pr-2" target="_blank">
                    <span><i class="fa fa-facebook"></i></span>
                    </a>
                    <a href="https://www.linkedin.com/company/patrol-pm" class="pl-2 pr-2" target="_blank">
                    <span><i class="fa fa-linkedin"></i></span>
                    </a>
                    <a href="https://www.linkedin.com/company/patrol-pm" class="pl-2 pr-2" target="_blank">
                    <span><i class="fa fa-instagram"></i></span>
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>

