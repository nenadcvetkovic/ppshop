<header class="header">

    @include('themes.default.front.sections.header.top')

    <nav class="navigation fixed-top">
      <div class="container-fluid">
        <div class="navigation__column left">
            <div class="header__logo">
                <a class="ps-logo" href="{{ url('/') }}">
                    {{--  {{ dd($configuration->getLogoWidth()) }}  --}}
                    <img src="{{ url('storage/media/images/logo.png') }}" alt="" style="max-width: {{ $configuration->getLogoWidth() }}; max-height: {{ $configuration->getLogoHeight() }};">
                </a>
            </div>
        </div>
        <div class="navigation__column center">
            @php
                echo \App\Http\Controllers\MenuController::returnMenuView();
            @endphp

        </div>
        <div class="navigation__column right">
            <div class="menu-toggle"><span></span></div>
            <form class="ps-search--header" action="{{ route('proizvod.pretraga') }}" method="get">
                <input class="form-control" type="text" name="q" placeholder="Pretrazi proizvode">
                <button><i class="ps-icon-search"></i></button>
            </form>
            <p class="pr-80">
                <br/><br/>
                <a href="https://www.facebook.com/patrolpm" class="pl-2 pr-2" target="_blank">
                <span><i class="fa fa-facebook"></i></span>
                </a>
                <a href="https://www.linkedin.com/company/patrol-pm" class="pl-2 pr-2" target="_blank">
                <span><i class="fa fa-linkedin"></i></span>
                </a>
                <a href="https://www.linkedin.com/company/patrol-pm" class="pl-2 pr-2" target="_blank">
                <span><i class="fa fa-instagram"></i></span>
                </a>
            </p>
          @include('themes.default.front.sections.shopping-cart')

        </div>
      </div>
    </nav>
  </header>

  <div class="header--sidebar"></div>
