@extends('themes.default.layouts.frontend.index')

@section('main-content')
    <div class="app">
    @include('themes.default.front.site-header')

    <main class="ps-main">
        @include('themes.default.front.sections.slider')

        @include('themes.default.front.sections.services')

        @include('themes.default.front.sections.offer')

        {{--  @include('themes.default.front.sections.sale')  --}}

        @include('themes.default.front.sections.top-sale')

        @include('themes.default.front.sections.most-view')

        {{--  @include('themes.default.front.sections.testimonial')  --}}

        @include('themes.default.front.sections.contact-us')

        @include('themes.default.front.sections.subscribe')

        @include('themes.default.front.sections.slider.logo')

        @include('themes.default.front.site-footer')

    </main>
    </div>
@endsection
