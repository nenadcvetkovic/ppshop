@extends('themes.default.layouts.frontend.page-template.1-column')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="pt-60 pb-60">
                    @include('themes.default.layouts.frontend.parts.messages')

                    <a href="{{ url('/') }}" class="btn btn-success">Povratak na pocetnu stranu</a>
                </div>
            </div>
        </div>
    </div>

@endsection

