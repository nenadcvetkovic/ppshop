@if ($slider['status'])
<div class="full mx-auto">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" style="height: {{ $configuration->getSliderHeight($slider['slider_id']) }}; margin: 0 auto; overflow: hidden;">
            @php
            $index = 0;
            @endphp
            @foreach ($slider['items'] as $item)
                <div class="carousel-item img-fluid {{ $index == 0 ? 'active' : '' }}">
                    <img class="carousel-item-image d-block w-100" src="{{ $item->image_type == 'url' ? $item->image_path : asset('storage/'.$item->image_path) }}" alt="{{ $item->name }}">
                </div>
                @php
                $index++
                @endphp
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
</div>
@endif
