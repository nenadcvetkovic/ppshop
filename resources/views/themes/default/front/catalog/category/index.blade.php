{{--  {{ dd($layout) }}  --}}
@extends('themes.default.layouts.frontend.page-template.'.$layout)

@section('top-page-content')
    @include('themes.default.front.sections.category.header')
@endsection

@section('content')
@php
    $count = $allProducts->count();
@endphp
    @if ($count)
        <div class="ps-products-wrapt pt-0 pb-80">
            <div class="ps-productst" data-mh="product-listing">

                <div class="ps-product__columns {{ isset($_GET['list']) && $_GET['list'] == 'grid' ? 'grid' : $configuration->getCatalogListMod() }}">
                    @if (isset($cat))
                        @if ($cat->show_desc)
                            <div class="pp-category__desc">
                                <p>{!! $cat->getDesc() !!}</p>
                            </div>
                        @endif
                    @endif

                    @include('themes.default.layouts.frontend.parts.product-action')

                    @foreach($allProducts as $prod)
                        @include('themes.default.front.catalog.product.product-'. (isset($_GET['list']) && $_GET['list'] == 'grid' ? $_GET['list'] : $configuration->getCatalogListMod()), ['prod' => $prod])
                    @endforeach

                    {{--  <div class="ps-product__column">
                        <div class="ps-shoe mb-30">
                            <div class="ps-shoe__thumbnail">
                                <a class="ps-shoe__favorite" href="#">
                                    <i class="ps-icon-heart"></i>
                                </a>
                                <img src="{{ asset('storage/media/images/no-image.png') }}" alt="">
                                <a class="ps-shoe__overlay" href="{{ url('catalog/'.strtolower($cat->name).'/test-product') }}"></a>
                            </div>
                            <div class="ps-shoe__content">
                            <div class="ps-shoe__variants">
                                <div class="ps-shoe__variant normal"><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""><img src="{{ asset('storage/media/images/no-image.png') }}" alt=""></div>
                                <select class="ps-rating ps-shoe__rating">
                                <option value="1">1</option>
                                <option value="1">2</option>
                                <option value="1">3</option>
                                <option value="1">4</option>
                                <option value="2">5</option>
                                </select>
                            </div>
                            <div class="ps-shoe__detail"><a class="ps-shoe__name" href="#">Air Jordan 7 Retro</a>
                                <p class="ps-shoe__categories"><a href="#">Men shoes</a>,<a href="#"> Nike</a>,<a href="#"> Jordan</a></p><span class="ps-shoe__price"> £ 120</span>
                            </div>
                            </div>
                        </div>
                    </div>  --}}
                </div>

                @include('themes.default.layouts.frontend.parts.product-action')

            </div>
        </div>
    @else
        <div class="alert alert-warning text-center p-5 m-2">
            <p>Trenuto nema proizvoda za kategoriju <strong><i>{{ isset($cat) ? $cat->name : ' SVI PROIZVODI' }}</i></strong></p>
        </div>
    @endif


@endsection
