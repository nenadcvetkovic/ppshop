
{{--  <div class="container">  --}}
    <div class="pp-product__list">
        <div class="card col mb-3">
            <div class="card-body row">
                <a class="pp-product__link" href="{{ url('katalog/proizvodi/'.$prod->identifier) }}"></a>
                <div class="pp-product__thumbnail col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                    @if ($prod->isNew())
                        <div class="ps-badge"><span>Novo</span></div>
                    @endif
                    @if ($prod->special_price)
                        <div class="ps-badge ps-badge--sale ps-badge--2nd">
                        <span>{{ $prod->getSalePercent() }}%</span>
                        </div>
                    @endif
                    @if ($configuration->getWishlistStatus())
                        <a class="pp-product__favorite" href="#">
                            <i class="ps-icon-heart"></i>
                        </a>
                    @endif
                    {{--  <a class="pp-product__favorite" href="#">
                        <i class="ps-icon-heart"></i>
                    </a>  --}}
                    <img src="{{ $prod->getHoverImage() }}" alt="">
                </div>
                <div class="pp-product__detail col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                    <h4>
                        <a class="pp-product__name" href="{{ url('catalog/products/'.$prod->identifier) }}">{{ $prod->name }}</a>
                        @if ($prod->showCatalogProdStockStatus())
                        <span class="pp-product__stock-status btn btn-sm btn-{{ $prod->stock_status ? 'success' : 'danger' }}">
                            {{  $prod->stock_status ? $t->translate('Na stanju') : $t->translate('Nije na stanju')  }}
                        </span>
                        @endif
                    </h4>
                    @if ($prod->sku && $prod->showCatalogProdSku())
                        <span class="pp-product__sku">
                            {!! $t->translate('SKU') !!}: {{ $prod->sku }}
                        </span>
                        <hr class="delimiter">
                    @endif
                    @if ($prod->showCatalogProdCat())
                    <p class="pp-product__categories">
                        @if($prod->productCategory())
                            @foreach ($prod->productCategory as $cat)
                                <a href="{{ url('catalog/'.$cat->identifier) }}">{{ $cat->name }}</a>,
                            @endforeach
                        @endif
                    </p>
                    @endif
                    @if ($prod->showCatalogProdPrice())
                    <span class="pp-product__price">
                        @if ($prod->special_price)
                            {{  $price->getPriceBlock($prod->special_price) }} <del>{{ $price->getPriceBlock($prod->price) }}</del>
                        @else
                            {{ $price->getPriceBlock($prod->price) }}
                        @endif
                    </span>
                    @endif
                    @if ($prod->showCatalogProdDesc())
                    <p class="pp-product__desc mt-3">
                        @if ($prod->short_desc)
                                {{  $prod->short_desc }}
                        @endif
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
{{--  </div>  --}}

<div class="{{ isset($section) && $section == 'slider' ? 'ps-shoes--carousel' : 'ps-product__column_list' }} d-none">
    <a class="ps-shoe__name" href="{{ url('catalog/products/'.$prod->identifier) }}">
    <div class="ps-shoe {{ isset($section) && $section == 'slider' ? '' : 'mb-30' }}">
        <div class="ps-shoe__thumbnail">
            @if ($prod->isNew())
                <div class="ps-badge"><span>Novo</span></div>
            @endif
            @if ($prod->special_price)
                <div class="ps-badge ps-badge--sale ps-badge--2nd">
                <span>{{ $prod->getSalePercent() }}%</span>
                </div>
            @endif
            @if ($configuration->getWishlistStatus())
                <a class="ps-shoe__favorite" href="#">
                    <i class="ps-icon-heart"></i>
                </a>
            @endif
            {{--  <a class="ps-shoe__favorite" href="#">
                <i class="ps-icon-heart"></i>
            </a>  --}}
            <img src="{{ $prod->getHoverImage() }}" alt="">
            <a class="ps-shoe__overlay" href="{{ url('catalog/products/'.$prod->identifier) }}"></a>
        </div>
        <div class="ps-shoe__content">
            <div class="ps-shoe__variants">
                <div class="ps-shoe__variant_list normal">
                    @if (count((array)$prod->productImages))
                        @foreach ($prod->productImages as $img)
                            <img src="{{ $img->image_path }}" alt="">
                        @endforeach
                    @endif
                </div>

                @include('themes.default.front.catalog.product.ratings')

            </div>
            <div class="ps-shoe__detail"><a class="ps-shoe__name" href="{{ url('catalog/products/'.$prod->identifier) }}">{{ $prod->name }} - {{ $prod->id }}</a>
                <p class="ps-shoe__categories">
                    @if($prod->productCategory())
                        @foreach ($prod->productCategory as $cat)
                            <a href="{{ url('catalog/'.$cat->identifier) }}">{{ $cat->name }}</a>,
                        @endforeach
                    @endif
                </p>
                <span class="ps-shoe__price">
                    @if ($prod->special_price)
                        {{  $price->getPriceBlock($prod->special_price) }} <del>{{ $price->getPriceBlock($prod->price) }}</del>
                    @else
                        {{ $price->getPriceBlock($prod->price) }}
                    @endif
                </span>
                <p class="ps-shoe__desc">
                    @if ($prod->short_desc)
                            {{  $prod->short_desc }}
                    @endif
                </p>
            </div>
        </div>
    </div>
</div>
</a>
<div class="clearfix"></div>
