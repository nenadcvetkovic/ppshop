@extends('themes.default.layouts.frontend.page-template.1-column')

@section('content')
<div id="app">

    <div class="ps-product--detail pt-60">
        <div class="ps-container">
          <div class="row">
            <div class="col-lg-10 col-md-12 col-lg-offset-1 mb-20">
              <div class="ps-product__thumbnail">
                <div class="ps-product__preview">
                  <div class="ps-product__variants">
                      @foreach ($prod->productImages as $image)
                      <div class="item"><img src="{{ $image->image_path }}" alt="" data-lightbox="roadtrip"/></div>
                      @endforeach
                  </div>
                  @if ($prod->video_url)
                    <a class="popup-youtube ps-product__video" href="{{ $prod->video_url }}">
                        <img src="{{  asset('storage/media/images/no-image.png') }}" alt=""><i class="fa fa-play"></i>
                    </a>
                    @endif
                </div>
                <div class="ps-product__image">
                    @foreach ($prod->productImages as $image)
                    <div class="item lightbox-item">
                        <img class="zoomm prod-image" src="{{ $image->image_path }}" alt="" data-zoom-imagem="{{ $image->image_path }}" data-lightbox="roadtrip"/>
                    </div>
                    @endforeach
                </div>
              </div>

              <div class="ps-product__thumbnail--mobile d-none">
                <div class="ps-product__main-img"><img src="{{ $prod->getHoverImage() }}" alt="" data-lightbox="roadtrip"></div>
                <div class="ps-product__preview owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="20" data-owl-nav="true" data-owl-dots="false" data-owl-item="3" data-owl-item-xs="3" data-owl-item-sm="3" data-owl-item-md="3" data-owl-item-lg="3" data-owl-duration="1000" data-owl-mousedrag="on">
                    @foreach ($prod->productImages as $image)
                    <img class="prod-image" src="{{ $image->image_path }}" alt="{{ $image->name }}" data-lightbox="roadtrip"/>
                    @endforeach
                </div>
              </div>
            <div class="ps-product__thumbnail--mobile">
                <ul id="imageGallery">
                    @foreach ($prod->productImages as $image)
                        <li class="item lightbox-item" data-thumb="{{ $image->image_path }}" data-src="{{ $image->image_path }}">
                            <img class="small" src="{{ $image->image_path }}" width="100%"/>
                        </li>
                    @endforeach
                </ul>
            </div>

              <div class="ps-product__info">

                @include('themes.default.front.catalog.product.ratings')

                <h1>{{ $prod->name }}</h1>
                @if ($prod->sku)
                    <span class="pp-product__sku">
                        {!! $t->translate('SKU') !!}: {{ $prod->sku }}
                    </span>
                @endif

                <hr class="delimiter">
                <h3 class="ps-product__price">
                    @if ($prod->showProdStockStatus())
                    <span class="pp-product__stock-status btn btn-sm btn-{{ $prod->stock_status ? 'success' : 'danger' }}">
                        {{  $prod->stock_status ? $t->translate('Na stanju') : $t->translate('Nije na stanju')  }}
                    </span>
                    @endif
                    @if ($prod->showProdPrice())
                        <div class="pp-product__price-box">
                            @if ($prod->special_price)
                            {{  $price->getPriceBlock($prod->special_price) }} <del>{{ $price->getPriceBlock($prod->price) }}</del>
                            @else
                                {{ $price->getPriceBlock($prod->price) }}
                            @endif
                        </div>
                    @endif
                </h3>

                <hr class="delimiter">
                @if ($prod->showProdCat())
                <p class="ps-product__category">
                    Kategorije:
                    @foreach ($prod->productCategory as $cat)
                    <a href="{{ route('site.category', $cat->identifier) }}"> {{ $cat->name }} </a>
                    @endforeach
                </p>
                @endif

                @if ($prod->desc)
                <div class="ps-product__block ps-product__quickview">
                  <h4 class="d-none">{{ $t->translate('OPIS PROIZVODA') }}</h4>
                  @if ($prod->showProdDesc())
                  <p>{!! $prod->desc !!}</p>
                  @endif
                </div>
                @endif

                {{--  <div class="ps-product__block ps-product__style">
                  <h4>CHOOSE YOUR STYLE</h4>
                  <ul>
                    <li><a href="product-detail.html"><img src="{{  asset('storage/media/images/no-image.png') }}" alt=""></a></li>
                    <li><a href="product-detail.html"><img src="{{  asset('storage/media/images/no-image.png') }}" alt=""></a></li>
                    <li><a href="product-detail.html"><img src="{{  asset('storage/media/images/no-image.png') }}" alt=""></a></li>
                    <li><a href="product-detail.html"><img src="{{  asset('storage/media/images/no-image.png') }}" alt=""></a></li>
                  </ul>
                </div>  --}}

                {{--  <div class="ps-product__block ps-product__size">
                  <h4>CHOOSE SIZE<a href="#">Size chart</a></h4>
                  <select class="ps-select selectpicker">
                    <option value="1">Select Size</option>
                    <option value="2">4</option>
                    <option value="3">4.5</option>
                    <option value="3">5</option>
                    <option value="3">6</option>
                    <option value="3">6.5</option>
                    <option value="3">7</option>
                    <option value="3">7.5</option>
                    <option value="3">8</option>
                    <option value="3">8.5</option>
                    <option value="3">9</option>
                    <option value="3">9.5</option>
                    <option value="3">10</option>
                  </select>
                  <div class="form-group">
                    <input class="form-control" type="number" value="1">
                  </div>
                </div>  --}}

                <div class="ps-product__shopping">
                    @if ($configuration->getShopStatus())
                        <a class="ps-btn mb-10" href="cart.html">
                            Add to cart<i class="ps-icon-next"></i>
                        </a>
                    @endif
                    <div class="ps-product__actions">
                        @if ($configuration->getWishlistStatus())
                            <a class="mr-10" href="whishlist.html">
                                <i class="ps-icon-heart"></i>
                            </a>
                        @endif
                        @if ($configuration->getShareStatus())
                            <a href="compare.html">
                                <i class="ps-icon-share"></i>
                            </a>
                        @endif
                    </div>
                </div>
              </div>

              <div class="clearfix"></div>

              @if ($configuration->showProductAdditionalInfo())
              <div class="ps-product__content mt-50">
                <ul class="tab-list" role="tablist">
                  <li class="active"><a href="#tab_01" aria-controls="tab_01" role="tab" data-toggle="tab">Opis</a></li>
                  @if ($configuration->getProductRatingStatus())
                    <li><a href="#tab_02" aria-controls="tab_02" role="tab" data-toggle="tab">Review</a></li>
                  @endif
                  {{--  <li><a href="#tab_03" aria-controls="tab_03" role="tab" data-toggle="tab">PRODUCT TAG</a></li>
                  <li><a href="#tab_04" aria-controls="tab_04" role="tab" data-toggle="tab">ADDITIONAL</a></li>  --}}
                </ul>
              </div>
              <div class="tab-content mb-60">
                <div class="tab-pane active" role="tabpanel" id="tab_01">
                  <p>{{ $prod->desc }}</p>
                </div>

                @if ($configuration->getProductRatingStatus())
                    <div class="tab-pane" role="tabpanel" id="tab_02">
                        <p class="mb-20">1 review for <strong>Shoes Air Jordan</strong></p>
                        <div class="ps-review">
                            <div class="ps-review__thumbnail"><img src="{{  asset('storage/media/images/no-image.png') }}" alt=""></div>
                            <div class="ps-review__content">
                            <header>
                                <select class="ps-rating">
                                <option value="1">1</option>
                                <option value="1">2</option>
                                <option value="1">3</option>
                                <option value="1">4</option>
                                <option value="5">5</option>
                                </select>
                                <p>By<a href=""> Alena Studio</a> - November 25, 2017</p>
                            </header>
                            <p>Soufflé danish gummi bears tart. Pie wafer icing. Gummies jelly beans powder. Chocolate bar pudding macaroon candy canes chocolate apple pie chocolate cake. Sweet caramels sesame snaps halvah bear claw wafer. Sweet roll soufflé muffin topping muffin brownie. Tart bear claw cake tiramisu chocolate bar gummies dragée lemon drops brownie.</p>
                            </div>
                        </div>
                        <form class="ps-product__review" action="_action" method="post">
                            <h4>ADD YOUR REVIEW</h4>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                    <div class="form-group">
                                    <label>Name:<span>*</span></label>
                                    <input class="form-control" type="text" placeholder="">
                                    </div>
                                    <div class="form-group">
                                    <label>Email:<span>*</span></label>
                                    <input class="form-control" type="email" placeholder="">
                                    </div>
                                    <div class="form-group">
                                    <label>Your rating<span></span></label>
                                    <select class="ps-rating">
                                        <option value="1">1</option>
                                        <option value="1">2</option>
                                        <option value="1">3</option>
                                        <option value="1">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 ">
                                    <div class="form-group">
                                    <label>Your Review:</label>
                                    <textarea class="form-control" rows="6"></textarea>
                                    </div>
                                    <div class="form-group">
                                    <button class="ps-btn ps-btn--sm">Submit<i class="ps-icon-next"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif

                <div class="tab-pane" role="tabpanel" id="tab_03">
                  <p>Add your tag <span> *</span></p>
                  <form class="ps-product__tags" action="_action" method="post">
                    <div class="form-group">
                      <input class="form-control" type="text" placeholder="">
                      <button class="ps-btn ps-btn--sm">Add Tags</button>
                    </div>
                  </form>
                </div>
                <div class="tab-pane" role="tabpanel" id="tab_04">
                  <div class="form-group">
                    <textarea class="form-control" rows="6" placeholder="Enter your addition here..."></textarea>
                  </div>
                  <div class="form-group">
                    <button class="ps-btn" type="button">Submit</button>
                  </div>
                </div>
              </div>
              @endif
              <br/>
            </div>
          </div>
        </div>
    </div>


      @if ($configuration->showRelatedProducts())
      @include('themes.default.front.sections.related-products')
      @endif
</div>

@endsection
