@extends('themes.default.layouts.frontend.page-template.2-column-left')

@section('content')

    <div class="ps-products-wrapt pt-0 pb-80">
        <div class="ps-productst" data-mh="product-listing">

            @include('themes.default.layouts.frontend.parts.product-action')

            <div class="ps-product__columns {{ isset($_GET['list']) && $_GET['list'] == 'grid' ? 'grid' : $configuration->getCatalogListMod() }}">
                @foreach($allProducts as $prod)
                    @include('themes.default.front.catalog.product.product-'.$configuration->getCatalogListMod(), ['prod' => $prod])
                @endforeach
            </div>

            @include('themes.default.layouts.frontend.parts.product-action')
        </div>
    </div>

@endsection

{{--  {{ dd(\App\Product::all()) }}  --}}

