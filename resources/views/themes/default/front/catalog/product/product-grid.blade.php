
{{--  <div class="container">  --}}
    <div class="pp-product__grid per_row__{{ $prod->prodPerRow() }}">
        <div class="card col mb-3">
            <div class="card-body row">
                <a class="pp-product__link" href="{{ url('katalog/proizvodi/'.$prod->identifier) }}"></a>
                <div class="pp-product__thumbnail col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if ($prod->isNew())
                        <div class="ps-badge"><span>Novo</span></div>
                    @endif
                    @if ($prod->special_price && $prod->showCatalogProdPrice())
                        <div class="ps-badge ps-badge--sale ps-badge--2nd">
                        <span>{{ $prod->getSalePercent() }}%</span>
                        </div>
                    @endif
                    @if ($prod->showCatalogProdStockStatus())
                    <span class="pp-product__stock-status {{ $prod->special_price ? '' : 'with-price' }} btn btn-sm btn-{{ $prod->stock_status ? 'success' : 'danger' }}">
                        {{  $prod->stock_status ? $t->translate('Na stanju') : $t->translate('Nije na stanju')  }}
                    </span>
                    @endif
                    @if ($configuration->getWishlistStatus())
                        <a class="pp-product__favorite" href="#">
                            <i class="ps-icon-heart"></i>
                        </a>
                    @endif
                    {{--  <a class="pp-product__favorite" href="#">
                        <i class="ps-icon-heart"></i>
                    </a>  --}}
                    <img src="{{ $prod->getHoverImage() }}" alt="">
                </div>
                <div class="pp-product__detail col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <h1 class="pp-product__title">
                        <a class="pp-product__name" href="{{ url('catalog/products/'.$prod->identifier) }}">{{ $prod->name }}</a>
                    </h1>
                    @if ($prod->sku && $prod->showCatalogProdSku())
                        <span class="pp-product__sku">
                            {!! $t->translate('SKU') !!}: {{ $prod->sku }}
                        </span>
                        <hr class="delimiter">
                    @endif
                    @if ($prod->showCatalogProdCat())
                    <p class="pp-product__categories">
                        @if($prod->productCategory())
                            @foreach ($prod->productCategory as $cat)
                                <a href="{{ url('catalog/'.$cat->identifier) }}">{{ $cat->name }}</a>,
                            @endforeach
                        @endif
                    </p>
                    @endif
                    @if ($prod->showCatalogProdPrice())
                    <div class="pp-product__price">
                        <span>
                            @if ($prod->special_price)
                                    {{  $price->getPriceBlock($prod->special_price) }} <del>{{ $price->getPriceBlock($prod->price) }}</del>
                                @else
                                    {{ $price->getPriceBlock($prod->price) }}
                                @endif
                        </span>
                    </div>
                    @endif
                    @if ($prod->short_desc && $prod->showCatalogProdDesc())
                    <p class="pp-product__desc">
                        {{ $prod->short_desc  }}
                    </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
{{--  </div>  --}}
