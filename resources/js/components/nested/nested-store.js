import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

export const nested = new Vuex.Store({
  namespaced: true,
  state: {
    elements: [
      {
        id: 1,
        name: "Shrek",
        elements: []
      },
      {
        id: 2,
        name: "Fiona",
        elements: [
          {
            id: 4,
            name: "Lord Farquad",
            elements: []
          },
          {
            id: 5,
            name: "Prince Charming",
            elements: []
          }
        ]
      },
      {
        id: 3,
        name: "Donkey",
        elements: []
      }
    ]
  },
  methods: {

  },
  mutations: {
    updateElements: (state, payload) => {
      state.elements = payload;
    }
  },
  actions: {
    updateElementsAction: ({ commit }, payload) => {
      commit("updateElements", payload);
    }
  }
});
