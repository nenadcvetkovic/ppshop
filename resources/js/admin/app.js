// require('../bootstrap');

import bootstrap from 'bootstrap';

import Vue from 'vue';
import Vuex from 'vuex';
import { store } from '../store';
import axios from 'axios';

import MenuItemsAdd from '../components/menu/items/Add.vue';
import SliderItemsAdd from '../components/slider/items/Add.vue';
import FormImageBox from '../components/form/image/Box.vue';
import CatFormImageBox from '../components/form/image/CatBox.vue';
import ImportFormImageBox from '../components/form/image/ImportFormImageBox.vue';
import PPEditContent from '../components/form/PPEditContent2.vue';
import NestedCategory from '../components/Nested.vue';
import Nested2 from '../components/Nested2.vue';
import NestedProducts from '../components/NestedProducts.vue';
import NestedImags from '../components/NestedImages.vue';
import PPVueDropzone from '../components/PPVueDropzone.vue';
import PPProductList from '../components/products/ProductListComponent';

import bootstrapColorpicker from 'bootstrap-colorpicker';

Vue.use(Vuex, bootstrapColorpicker);

Vue.prototype.$store = store;
Vue.prototype.$http = axios;

// register modal component
Vue.component("modal", {
    template: "#modal-template"
});

window.$ = window.jQuery = jQuery = $ = require('jquery');
$.noConflict();

$.trumbowyg.svgPath = '../../../../../public/assets/icons/icons.svg';
import './../../../public/plugins/trumbowyg/trumbowyg.upload.min.js';
import './../../../public/plugins/trumbowyg/insertSection/trumbowyg.insertSecton.min.js';
import './../../../public/plugins/trumbowyg/colors/trumbowyg.colors.js';
import './../../../public/plugins/trumbowyg/cleanpaste/trumbowyg.cleanpaste.js';
// import './../../../public/plugins/trumbowyg/lfm.upload.min.js';

Vue.prototype.$previewImage = function preview_image(event){
    var reader = new FileReader();
    reader.onload = function(){
        //var output = document.getElementById('output_image');
        //console.log(event.target.parentNode.querySelector('.output_image'));
        var output = event.target.parentNode.querySelector('.output_image');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}

function preview_image(event)
{
    var reader = new FileReader();
    reader.onload = function()
    {
        //var output = document.getElementById('output_image');
        //console.log(event.target.parentNode.querySelector('.output_image'));
        var output = event.target.parentNode.querySelector('.output_image');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}

const adminApp = new Vue({
    el: '#adminApp',
    components: {
        'pp-menu-items-add': MenuItemsAdd,
        'pp-form-image-box': FormImageBox,
        'pp-cat-form-image-box': CatFormImageBox,
        'pp-import-form-image-box': ImportFormImageBox,
        'pp-items-slider-add': SliderItemsAdd,
        'pp-nested-category': NestedCategory,
        'pp-edit-content': PPEditContent,
        'pp-nested-2': Nested2,
        'pp-nested-products': NestedProducts,
        'pp-nested-images': NestedImags,
        'pp-dropzone': PPVueDropzone,
        'pp-product-list': PPProductList
    },
    store,
    data: {
    },
    computed: {
    },
    mounted () {
    },
    methods: {
        updateBrandsStatus (brand_id) {
            console.log('srydhdfgdfg', brand_id);
            this.$http({
                url: this.$store.state.base_url + 'updateBrandsStatus', method: 'post', dataType: 'json',
                data: {
                    type: 'brands',
                    id: brand_id
                }
            }).then(response => {
                console.log('Data ' , response.data );
                console.log('Status', response.status);
            }).catch(error => {
                console.log(error.response.data);
                this.messagesClass = 'alert alert-danger';
                this.messages = error.response.data.errors;
            });
        },
        updateCmsPagesStatus (page_id) {
            console.log('srydhdfgdfg', page_id);
            this.$http({
                url: this.$store.state.base_url + 'updateCmsPagesStatus', method: 'post', dataType: 'json',
                data: {
                    type: 'page',
                    id: page_id
                }
            }).then(response => {
                console.log('Data ' , response.data );
                console.log('Status', response.status);
            }).catch(error => {
                console.log(error.response.data);
                this.messagesClass = 'alert alert-danger';
                this.messages = error.response.data.errors;
            });
        },
        updateCmsBlockStatus (block_id) {
            console.log('srydhdfgdfg', block_id);
            this.$http({
                url: this.$store.state.base_url + 'updateCmsBlockStatus', method: 'post', dataType: 'json',
                data: {
                    type: 'page',
                    id: block_id
                }
            }).then(response => {
                console.log('Data ' , response.data );
                console.log('Status', response.status);
            }).catch(error => {
                console.log(error.response.data);
                this.messagesClass = 'alert alert-danger';
                this.messages = error.response.data.errors;
            });
        },
        updateMenuStatus (menu_id) {
            console.log('srydhdfgdfg', menu_id);
            this.$http({
                url: this.$store.state.base_url + 'updateMenuStatus', method: 'post', dataType: 'json',
                data: {
                    type: 'page',
                    id: menu_id
                }
            }).then(response => {
                console.log('Data ' , response.data );
                console.log('Status', response.status);
            }).catch(error => {
                console.log(error.response.data);
                this.messagesClass = 'alert alert-danger';
                this.messages = error.response.data.errors;
            });
        }
    }
});
