require('./bootstrap');

import bootstrap from 'bootstrap';

import Vue from 'vue';
import Vuex from 'vuex';
import { store } from './store';
import axios from 'axios';

import PPApp from './components/PPApp.vue';

import PPSubscribe from './components/form/PPSubscribe.vue';
import PPSidebar from './components/plugins/PPSidebar.vue';
import PPMenuAccordion from "./components/plugins/PPMenuAccordion.vue";
import PPVueSlickCarousel from "./components/plugins/PPVueSlickCarousel.vue";
import PPVueConciseSlider from "./components/plugins/PPVueConciseSlider.vue";
import PPSlider from "./components/plugins/PPSlider.vue";
import PPProductSlider from './components/plugins/PPProductSlider';

import Sortable from 'vue-sortable';
import SortableJs from 'sortablejs';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faSortDown, faSortUp, faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

window.$ = window.jQuery = jQuery = $ = require('jquery');
$.noConflict();

import Slick from 'slick-carousel';

library.add(faSortDown, faSortUp, faAngleDown, faAngleUp);

Vue.component('font-awesome-icon', FontAwesomeIcon);

window.Vue = require('vue');
Vue.use(Vuex, Slick, PPProductSlider, PPApp, Sortable, SortableJs);
Vue.prototype.$store = store;
Vue.prototype.$http = axios;
// Vue.config.devtools = true;

Vue.component("modal", {
    template: "#modal-template"
});

// Vue.component('pp-menu-accordion', PPMenuAccordion);

const app = new Vue({
    el: '#app',
    name: 'app',
    components: {
        'pp-app': PPApp,
        'pp-subscribe': PPSubscribe,
        'pp-sidebar': PPSidebar,
        'pp-menu-accordion': PPMenuAccordion,
        'pp-vue-slick-carousel': PPVueSlickCarousel,
        'pp-vue-concise-slider': PPVueConciseSlider,
        'pp-slider': PPSlider,
        'pp-product-slider': PPProductSlider
    },
    store,
    data: {
    },
    computed: {
    },
    created () {
    },
    mounted () {
    },
    methods: {
    }
});

jQuery(document).ready(function(){
    jQuery(".slider").not('.slick-initialized').slick({
        slidesToShow: 6,
        slidesToScroll: 6,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });

});

