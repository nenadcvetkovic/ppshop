import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';


window.Vue = require('vue');
Vue.use(Vuex);
Vue.prototype.$http = axios;
let base = '/';

axios.get("/getBaseUrl").then(function(response) {
    console.log('testomg', response.data);
    base = response.data;
  }.bind(this));

export const store = new Vuex.Store({
  state: {
    namespaced: true,
    base_url: base,
    crsf_field: document.querySelector('meta[name="csrf-token"]').content,
    nested: {
        elements: [
            {
                id: 1,
                name: "Shrek",
                newId: '',
                elements: []
            },
            {
                id: 2,
                name: "Fiona",
                newId: '',
                elements: []
            },
            {
                id: 4,
                name: "Lord Farquad",
                newId: '',
                elements: []
            },
            {
                id: 5,
                name: "Prince Charming",
                newId: '',
                elements: []
            },
            {
                id: 3,
                name: "Donkey",
                newId: '',
                elements: []
            }
        ]
    },
    category: {
        elements: [
            {
                id: 1,
                name: "Shrek",
                newId: '',
                elements: []
            },
            {
                id: 2,
                name: "Fiona",
                newId: '',
                elements: []
            },
            {
                id: 4,
                name: "Lord Farquad",
                newId: '',
                elements: []
            },
            {
                id: 5,
                name: "Prince Charming",
                newId: '',
                elements: []
            },
            {
                id: 3,
                name: "Donkey",
                newId: '',
                elements: []
            }
        ]
    }
  },
  mutations: {
    updateElements: (state, payload) => {
      state.elements = payload;
      state.nested.elements = payload;
      console.log(state, payload);
      Object.keys(payload).forEach(function(key) {
        console.log('Old : ', payload[key].name, ' => ', payload[key].id, ' new ', parseInt(key)+1, ' level ', 0);
        payload[key].newId = parseInt(key)+1;
      });
    }
  },
  actions: {
    initX: () => {
        console.log('sdgdsfgdfg');
        axios({
            url: 'https://httpbin.org/get',
            method: 'get',
            dataType: 'json'
          }).then(response => {
            console.log('dsfdsgfsdgfds', response);
          });
    },
    updateElementsAction: ({ commit }, payload) => {
      commit("updateElements", payload);
    }
  },
  computed: {
    base_url () {
        let base = '';
        axios.get("/getBaseUrl").then(function(response) {
            console.log('testomg', response.data);
            base = response.data;
        }.bind(this));
        return 'tegsyhdfhyf';
    }
  }
})
